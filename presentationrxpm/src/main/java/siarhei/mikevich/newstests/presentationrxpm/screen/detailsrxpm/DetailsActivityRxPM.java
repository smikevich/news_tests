package siarhei.mikevich.newstests.presentationrxpm.screen.detailsrxpm;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.widget.Toast;

import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxrelay2.BehaviorRelay;

import java.io.Serializable;

import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.presentationrxpm.R;
import siarhei.mikevich.newstests.presentationrxpm.screen.base.activity.BaseActivityRxPM;
import siarhei.mikevich.newstests.presentationrxpm.utils.ExtraKeys;
import siarhei.mikevich.newstests.presentationrxpm.utils.browser.BrowserUtils;
import siarhei.mikevich.newstests.presentationrxpm.views.icon.ImageViewFresco;
import siarhei.mikevich.newstests.presentationrxpm.views.items.DetailsView;

/**
 * Created by Siarhei Mikevich on 12/18/17.
 */

public class DetailsActivityRxPM extends BaseActivityRxPM<DetailsPresentationModel> {

    private Article article;

    private AppBarLayout appBarLayout;
    private ImageViewFresco detailsImageView;
    private DetailsView detailsView;
    private FloatingActionButton aboutFloatingActionButton;

    private BehaviorRelay<DetailsPresentationModel.OffsetParams> appBarLayoutSubject = BehaviorRelay.create();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindViews();
        setupViews();
        setupStates();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_details;
    }

    @Override
    protected int getActivityTitleId() {
        return R.string.title_details;
    }

    @Override
    protected DetailsPresentationModel getPresentationModel() {
        Serializable serializable = loadArticle();
        return new DetailsPresentationModel(serializable);
    }

    private Serializable loadArticle() {
        Intent intent = getIntent();
        return intent.getSerializableExtra(ExtraKeys.ITEM_KEY);
    }

    private void bindViews() {
        appBarLayout = findViewById(R.id.app_bar_layout);
        detailsImageView = findViewById(R.id.details_image_view);
        detailsView = findViewById(R.id.details_view);
        aboutFloatingActionButton = findViewById(R.id.about_floating_action_button);
    }

    private void setupViews() {
        appBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            DetailsPresentationModel.OffsetParams offsetParams = new DetailsPresentationModel.OffsetParams(appBarLayout.getTotalScrollRange(), verticalOffset);
            appBarLayoutSubject.accept(offsetParams);
        });
        appBarLayoutSubject.subscribe(presentationModel.offsetChangedConsumer);
        RxView.clicks(aboutFloatingActionButton)
                .filter(object -> article != null)
                .map(object -> article.getUrl())
                .subscribe(presentationModel.aboutButtonClickConsumer);
    }

    private void setupStates() {
        presentationModel.articleState.subscribe(this::showArticle);
        presentationModel.navigateBrowserByUrlState.subscribe(this::navigateBrowserByUrl);
        presentationModel.floatingActionButtonAnimationState.subscribe(this::startFloatingActionButtonAnimation);
        presentationModel.errorState.subscribe(this::showToastMessage);
    }

    private void showArticle(Article article) {
        this.article = article;
        detailsView.setup(article);
        detailsImageView.setImageURI(article.getUrlToImage());
    }

    private void navigateBrowserByUrl(String url) {
        BrowserUtils.navigateBrowser(this, url, e -> Toast.makeText(this, R.string.error, Toast.LENGTH_SHORT).show());
    }

    private void startFloatingActionButtonAnimation(DetailsPresentationModel.State state) {
        if (DetailsPresentationModel.State.DO_NOTHING != state) {
            int visibility = DetailsPresentationModel.State.SHOW == state ? 1 : 0;
            ViewCompat.animate(aboutFloatingActionButton).scaleY(visibility).scaleX(visibility).start();
        }
    }

}
