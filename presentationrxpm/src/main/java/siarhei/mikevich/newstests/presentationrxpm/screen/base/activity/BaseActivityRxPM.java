package siarhei.mikevich.newstests.presentationrxpm.screen.base.activity;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import siarhei.mikevich.newstests.presentationrxpm.R;
import siarhei.mikevich.newstests.presentationrxpm.views.loadingdialog.LoadingDialog;
import siarhei.mikevich.newstests.presentationrxpm.views.loadingdialog.LoadingView;

/**
 * Created by Siarhei Mikevich on 12/18/17.
 */

public abstract class BaseActivityRxPM<PM extends BasePMContract> extends AppCompatActivity {

    private Toolbar toolbar;
    private LoadingView loadingView;

    protected PM presentationModel;

    @LayoutRes
    protected abstract int getLayoutId();

    @StringRes
    protected abstract int getActivityTitleId();

    protected abstract PM getPresentationModel();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        bindViews();
        setupViews();
        setTitle(getActivityTitleId());
        setupPresentationModel();
    }

    // loading dialog

    protected void showLoadingView() {
        loadingView.showLoadingView();
    }

    protected void hideLoadingView() {
        loadingView.hideLoadingView();
    }

    // toast message information

    protected void showToastMessage(@StringRes int toastMessageId) {
        Toast.makeText(this, getString(toastMessageId, Toast.LENGTH_SHORT), Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // private methods

    private void bindViews() {
        toolbar = findViewById(R.id.toolbar);
        loadingView = LoadingDialog.init(getSupportFragmentManager());
    }

    private void setupViews() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    private void setupPresentationModel() {
        presentationModel = getPresentationModel();
        presentationModel.init();
    }

}
