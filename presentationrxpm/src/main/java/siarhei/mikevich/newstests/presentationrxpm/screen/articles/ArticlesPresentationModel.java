package siarhei.mikevich.newstests.presentationrxpm.screen.articles;

import android.support.annotation.NonNull;

import com.jakewharton.rxrelay2.PublishRelay;

import java.io.Serializable;

import io.reactivex.functions.Consumer;
import rx.Observable;
import siarhei.mikevich.newstests.data.api.utils.Filter;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.domain.model.content.Source;
import siarhei.mikevich.newstests.domain.usecase.ArticlesUseCase;
import siarhei.mikevich.newstests.presentationrxpm.R;
import siarhei.mikevich.newstests.presentationrxpm.screen.base.activity.list.BaseListPresentationModel;

/**
 * Created by mikes on 19.12.2017.
 */

public class ArticlesPresentationModel extends BaseListPresentationModel<Article> {

    private ArticlesUseCase articlesUseCase;

    private Source source;
    private Serializable serializable;

    private @Filter String filter = Filter.TOP;

    private PublishRelay<String> filterArticlesAction = PublishRelay.create();
    public Consumer<String> filterArticlesActionConsumer = filterArticlesAction;

    ArticlesPresentationModel(Serializable serializable, @NonNull ArticlesUseCase articlesUseCase) {
        this.serializable = serializable;
        this.articlesUseCase = articlesUseCase;
    }

    @Override
    protected Observable<BaseItemsContent<Article>> getObservable() {
        return articlesUseCase.execUseCase(new ArticlesUseCase.Params(source.getId(), filter));
    }

    @Override
    public void init() {
        loadSource()
                .subscribe(source -> this.source = source, throwable -> {
                    throwable.printStackTrace();
                    errorMessageId.accept(R.string.error);
                });
        filterArticlesAction.subscribe(filter -> {
            this.filter = filter;
            super.init();
        });
        super.init();
    }

    private Observable<Source> loadSource() {
        if (serializable != null && serializable instanceof Source) {
            return Observable.just((Source) serializable);
        } else {
            return Observable.error(new Exception());
        }
    }

}
