package siarhei.mikevich.newstests.presentationrxpm;

import android.app.Application;
import android.content.Context;

import com.facebook.drawee.backends.pipeline.Fresco;

import siarhei.mikevich.newstests.data.DataModule;

/**
 * Created by mikes on 04.10.17.
 */

public class AppDelegate extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    public static Context getContext() {
        return context;
    }

    private void init() {
        context = this;
        Fresco.initialize(this);
        DataModule.get.init(new PresentationConfig());
    }

}
