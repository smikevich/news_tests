package siarhei.mikevich.newstests.presentationrxpm.views.items;

import android.content.Context;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.domain.utils.unit.DateUtils;
import siarhei.mikevich.newstests.domain.utils.unit.TextUtils;
import siarhei.mikevich.newstests.presentationrxpm.R;
import siarhei.mikevich.newstests.presentationrxpm.views.icon.IconView;

/**
 * Created by Siarhei Mikevich on 10/18/17.
 */

public class DetailsView extends LinearLayoutCompat {

    private LinearLayout detailsHeaderContent;
    private IconView detailsAuthorIconView;
    private TextView detailsAuthorTextView;
    private TextView detailsDateTextView;
    private TextView detailsTitleTextView;
    private TextView detailsDescriptionTextView;
    private TextView detailsErrorTextView;

    public DetailsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public DetailsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DetailsView(Context context) {
        super(context);
        init();
    }

    public void setup(Article article) {
        detailsAuthorIconView.setup(article.getAuthor());
        detailsAuthorTextView.setText(article.getAuthor());
        detailsDateTextView.setText(DateUtils.parseDateTime(article.getPublishedAt()));
        detailsTitleTextView.setText(article.getTitle());
        detailsDescriptionTextView.setText(article.getDescription());
        setupViewsVisibility(article);
    }

    private void init() {
        inflate(getContext(), R.layout.view_details_content, this);
        bindViews();
    }

    private void bindViews() {
        detailsHeaderContent = findViewById(R.id.details_header_content);
        detailsAuthorIconView = findViewById(R.id.details_author_icon_view);
        detailsAuthorTextView = findViewById(R.id.details_author_text_view);
        detailsDateTextView = findViewById(R.id.details_date_text_view);
        detailsTitleTextView = findViewById(R.id.details_title_text_view);
        detailsDescriptionTextView = findViewById(R.id.details_description_text_view);
        detailsErrorTextView = findViewById(R.id.details_error_text_view);
    }

    private void setupViewsVisibility(Article article) {
        String author = article.getAuthor();
        String date = DateUtils.parseDateTime(article.getPublishedAt());
        String title = article.getTitle();
        String description = article.getDescription();
        detailsAuthorTextView.setVisibility(TextUtils.isEmpty(author) ? View.GONE : View.VISIBLE);
        detailsDateTextView.setVisibility(TextUtils.isEmpty(date) ? View.GONE : View.VISIBLE);
        detailsTitleTextView.setVisibility(TextUtils.isEmpty(title) ? View.GONE : View.VISIBLE);
        detailsDescriptionTextView.setVisibility(TextUtils.isEmpty(description) ? View.GONE : View.VISIBLE);
        if (TextUtils.isEmpty(author) && TextUtils.isEmpty(date)) {
            detailsHeaderContent.setVisibility(View.GONE);
        }
        if (TextUtils.isEmpty(title) && TextUtils.isEmpty(description)) {
            detailsErrorTextView.setVisibility(View.VISIBLE);
        }
    }

}
