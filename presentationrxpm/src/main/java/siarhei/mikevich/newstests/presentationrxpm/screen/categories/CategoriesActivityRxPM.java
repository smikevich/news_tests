package siarhei.mikevich.newstests.presentationrxpm.screen.categories;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import siarhei.mikevich.newstests.data.repository.RepositoryProvider;
import siarhei.mikevich.newstests.domain.model.content.Category;
import siarhei.mikevich.newstests.domain.usecase.CategoriesUseCase;
import siarhei.mikevich.newstests.presentationrxpm.R;
import siarhei.mikevich.newstests.presentationrxpm.screen.base.activity.list.BaseListActivityRxPM;
import siarhei.mikevich.newstests.presentationrxpm.screen.base.adapter.OnItemClickListener;
import siarhei.mikevich.newstests.presentationrxpm.screen.sources.SourcesActivityRxPM;
import siarhei.mikevich.newstests.presentationrxpm.utils.transformer.AsyncTransformer;

/**
 * Created by mikes on 19.12.2017.
 */

public class CategoriesActivityRxPM extends BaseListActivityRxPM<Category, CategoriesPresentationModel, CategoriesAdapter>
    implements OnItemClickListener<Category> {

    @Override
    protected int getActivityTitleId() {
        return R.string.title_categories;
    }

    @Override
    protected CategoriesPresentationModel getPresentationModel() {
        CategoriesUseCase categoriesUseCase = new CategoriesUseCase(RepositoryProvider.provideCategoriesRepository(), new AsyncTransformer<>());
        return new CategoriesPresentationModel(categoriesUseCase);
    }

    @Override
    protected CategoriesAdapter getAdapter() {
        return new CategoriesAdapter(this);
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return new GridLayoutManager(this, getResources().getInteger(R.integer.categories_columns_count));
    }

    @Override
    protected Class<? extends AppCompatActivity> getNextActivity() {
        return SourcesActivityRxPM.class;
    }

}
