package siarhei.mikevich.newstests.presentationrxpm.screen.sources;

import android.support.annotation.NonNull;

import com.jakewharton.rxrelay2.BehaviorRelay;
import com.jakewharton.rxrelay2.PublishRelay;

import java.io.Serializable;

import io.reactivex.functions.Consumer;
import rx.Observable;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Category;
import siarhei.mikevich.newstests.domain.model.content.Source;
import siarhei.mikevich.newstests.domain.usecase.SourcesUseCase;
import siarhei.mikevich.newstests.presentationrxpm.R;
import siarhei.mikevich.newstests.presentationrxpm.screen.base.activity.list.BaseListPresentationModel;

/**
 * Created by mikes on 19.12.2017.
 */

public class SourcesPresentationModel extends BaseListPresentationModel<Source> {

    private SourcesUseCase sourcesUseCase;
    private Category category;
    private Serializable serializable;

    private BehaviorRelay<String> navigateBrowserByUrl = BehaviorRelay.create();
    public io.reactivex.Observable<String> navigateBrowserByUrlState = navigateBrowserByUrl.hide();

    private PublishRelay<Source> aboutButtonClick = PublishRelay.create();
    public Consumer<Source> aboutButtonClickConsumer = aboutButtonClick;

    SourcesPresentationModel(Serializable serializable, @NonNull SourcesUseCase sourcesUseCase) {
        this.serializable = serializable;
        this.sourcesUseCase = sourcesUseCase;
    }

    @Override
    protected Observable<BaseItemsContent<Source>> getObservable() {
        return sourcesUseCase.execUseCase(new SourcesUseCase.Params(category.getId()));
    }

    @Override
    public void init() {
        loadCategory()
                .subscribe(category -> this.category = category, throwable -> {
                    throwable.printStackTrace();
                    errorMessageId.accept(R.string.error);
                });
        aboutButtonClick
                .map(Source::getUrl)
                .subscribe(navigateBrowserByUrl);
        super.init();
    }

    private Observable<Category> loadCategory() {
        if (serializable != null && serializable instanceof Category) {
            return Observable.just((Category) serializable);
        } else {
            return Observable.error(new Exception());
        }
    }

}
