package siarhei.mikevich.newstests.presentationrxpm.views.items;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import siarhei.mikevich.newstests.domain.model.content.Category;
import siarhei.mikevich.newstests.presentationrxpm.R;
import siarhei.mikevich.newstests.presentationrxpm.views.icon.IconView;

/**
 * Created by mikes on 05.10.17.
 */

public class CategoryView extends FrameLayout {

    private IconView categoryIconView;
    private TextView categoryNameTextView;

    public CategoryView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public CategoryView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CategoryView(Context context) {
        super(context);
        init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int side = getMeasuredWidth();
        super.onMeasure(MeasureSpec.makeMeasureSpec(side, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(side, MeasureSpec.EXACTLY));
    }

    public void setup(Category category) {
        categoryIconView.setup(category.getName());
        categoryNameTextView.setText(category.getName());
    }

    private void init() {
        inflate(getContext(), R.layout.view_category, this);
        bindViews();
    }

    private void bindViews() {
        categoryIconView = findViewById(R.id.category_icon_view);
        categoryNameTextView = findViewById(R.id.category_name_text_view);
    }

}
