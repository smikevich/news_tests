package siarhei.mikevich.newstests.presentationrxpm.views.loadingdialog;

/**
 * Created by mikes on 05.10.17.
 */

public interface LoadingView {

    void showLoadingView();

    void hideLoadingView();

}
