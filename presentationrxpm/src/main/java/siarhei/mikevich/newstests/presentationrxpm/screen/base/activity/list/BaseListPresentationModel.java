package siarhei.mikevich.newstests.presentationrxpm.screen.base.activity.list;

import android.support.annotation.StringRes;

import com.jakewharton.rxrelay2.BehaviorRelay;
import com.jakewharton.rxrelay2.PublishRelay;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.utils.unit.TextUtils;
import siarhei.mikevich.newstests.presentationrxpm.R;
import siarhei.mikevich.newstests.presentationrxpm.screen.base.activity.BasePMContract;

/**
 * Created by mikes on 18.12.2017.
 */

public abstract class BaseListPresentationModel<T> implements BasePMContract {

    private BehaviorRelay<List<T>> list = BehaviorRelay.create();
    public Observable<List<T>> listState = list.hide();

    private BehaviorRelay<Boolean> visibilityContent = BehaviorRelay.create();
    public Observable<Boolean> visibilityContentState = visibilityContent.hide();

    private BehaviorRelay<Boolean> loading = BehaviorRelay.create();
    public Observable<Boolean> loadingState = loading.hide();

    private BehaviorRelay<T> navigateNext = BehaviorRelay.create();
    public Observable<T> navigateNextState = navigateNext.hide();

    private BehaviorRelay<String> errorMessage = BehaviorRelay.create();
    public Observable<String> errorMessageState = errorMessage.hide();

    protected BehaviorRelay<Integer> errorMessageId = BehaviorRelay.create();
    public Observable<Integer> errorMessageIdState = errorMessageId.hide();

    private PublishRelay<Boolean> loadItems = PublishRelay.create();
    public Consumer<Boolean> loadItemsConsumer = loadItems;

    private PublishRelay<T> clickItem = PublishRelay.create();
    public Consumer<T> clickItemConsumer = clickItem;

    protected abstract rx.Observable<BaseItemsContent<T>> getObservable();

    @Override
    public void init() {
        loadItems();
        loadItems.subscribe(b -> loadItems()); // TODO: fix after global switching to rx2
        clickItem.subscribe(navigateNext);
    }

    private void loadItems() {
        getObservable()
                .doOnSubscribe(() -> loading.accept(true))
                .doOnTerminate(() -> loading.accept(false))
                .subscribe(this::handleResponse, throwable -> {
                    throwable.printStackTrace();
                    setupFailureScreen(R.string.error_list_empty, true);
                });
    }

    private void handleResponse(BaseItemsContent<T> itemsContent) {
        List<T> items = itemsContent.getItems();
        if (items.size() == 0) {
            String errorMessage = itemsContent.getErrorMessage();
            if (TextUtils.isEmpty(errorMessage)) {
                setupFailureScreen(R.string.error_list_empty, false);
            } else {
                setupFailureScreen(errorMessage);
            }
        } else {
            setupSuccessScreen(items);
        }
    }

    private void setupSuccessScreen(List<T> list) {
        this.list.accept(list);
        this.visibilityContent.accept(true);
    }

    private void setupFailureScreen(String errorMessage) {
        setupFailureScreen(true);
        this.errorMessage.accept(errorMessage);
    }

    private void setupFailureScreen(@StringRes int errorMessageId, boolean isShouldToastShow) {
        setupFailureScreen(isShouldToastShow);
        this.errorMessageId.accept(errorMessageId);
    }

    private void setupFailureScreen(boolean isShouldToastShow) {
        this.list.accept(new ArrayList<>());
        this.visibilityContent.accept(false);
        if (isShouldToastShow) {
            this.errorMessageId.accept(R.string.error);
        }
    }

}
