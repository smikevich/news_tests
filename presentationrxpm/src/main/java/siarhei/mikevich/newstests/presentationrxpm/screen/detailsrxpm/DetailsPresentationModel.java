package siarhei.mikevich.newstests.presentationrxpm.screen.detailsrxpm;

import com.jakewharton.rxrelay2.BehaviorRelay;
import com.jakewharton.rxrelay2.PublishRelay;

import java.io.Serializable;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.presentationrxpm.R;
import siarhei.mikevich.newstests.presentationrxpm.screen.base.activity.BasePMContract;

/**
 * Created by mikes on 17.12.2017.
 */

public class DetailsPresentationModel implements BasePMContract {

    private Serializable serializable;
    private boolean isVisibleFloatingActionButton = true;

    private BehaviorRelay<Article> article = BehaviorRelay.create();
    public Observable<Article> articleState = article.hide();

    private BehaviorRelay<String> navigateBrowserByUrl = BehaviorRelay.create();
    public Observable<String> navigateBrowserByUrlState = navigateBrowserByUrl.hide();

    private BehaviorRelay<State> floatingActionButtonAnimation = BehaviorRelay.create();
    public Observable<State> floatingActionButtonAnimationState = floatingActionButtonAnimation.hide();

    private BehaviorRelay<Integer> error = BehaviorRelay.create();
    public Observable<Integer> errorState = error.hide();

    private PublishRelay<String> aboutButtonClick = PublishRelay.create();
    public Consumer<String> aboutButtonClickConsumer = aboutButtonClick;

    private PublishRelay<OffsetParams> offsetChanged = PublishRelay.create();
    public Consumer<OffsetParams> offsetChangedConsumer = offsetChanged;

    public DetailsPresentationModel(Serializable serializable) {
        this.serializable = serializable;
    }

    @Override
    public void init() {
        loadArticle()
                .subscribe(article, throwable -> {
                    throwable.printStackTrace();
                    error.accept(R.string.error);
                });
        aboutButtonClick.subscribe(navigateBrowserByUrl);
        offsetChanged.map(offsetChangingMapping()).subscribe(floatingActionButtonAnimation);
    }

    private Observable<Article> loadArticle() {
        if (serializable != null && serializable instanceof Article) {
            return Observable.just((Article) serializable);
        } else {
            return Observable.error(new Exception());
        }
    }

    private Function<OffsetParams, State> offsetChangingMapping() {
        return offsetParams -> {
            int percentage = (Math.abs(offsetParams.verticalOffset)) * 100 / offsetParams.totalScrollRange;
            if (percentage >= 20 && !isVisibleFloatingActionButton) {
                isVisibleFloatingActionButton = true;
                return State.HIDE;
            }
            else if(percentage < 20 && isVisibleFloatingActionButton){
                isVisibleFloatingActionButton = false;
                return State.SHOW;
            }
            return State.DO_NOTHING;
        };
    }

    static final class OffsetParams {

        private int totalScrollRange;
        private int verticalOffset;

        public OffsetParams(int totalScrollRange, int verticalOffset) {
            this.totalScrollRange = totalScrollRange;
            this.verticalOffset = verticalOffset;
        }

    }

    public enum State {
        SHOW, HIDE, DO_NOTHING
    }

}
