package siarhei.mikevich.newstests.presentationrxpm.screen.sources;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.jakewharton.rxrelay2.BehaviorRelay;

import java.io.Serializable;

import siarhei.mikevich.newstests.data.repository.RepositoryProvider;
import siarhei.mikevich.newstests.domain.model.content.Source;
import siarhei.mikevich.newstests.domain.usecase.SourcesUseCase;
import siarhei.mikevich.newstests.presentationrxpm.R;
import siarhei.mikevich.newstests.presentationrxpm.screen.articles.ArticlesActivityRxPM;
import siarhei.mikevich.newstests.presentationrxpm.screen.base.activity.list.BaseListActivityRxPM;
import siarhei.mikevich.newstests.presentationrxpm.utils.ExtraKeys;
import siarhei.mikevich.newstests.presentationrxpm.utils.browser.BrowserUtils;
import siarhei.mikevich.newstests.presentationrxpm.utils.transformer.AsyncTransformer;

/**
 * Created by mikes on 19.12.2017.
 */

public class SourcesActivityRxPM extends BaseListActivityRxPM<Source, SourcesPresentationModel, SourcesAdapter>
        implements OnSourceClickListener {

    private BehaviorRelay<Source> aboutClickedSubject = BehaviorRelay.create();

    @Override
    protected int getActivityTitleId() {
        return R.string.title_sources;
    }

    @Override
    protected SourcesPresentationModel getPresentationModel() {
        SourcesUseCase sourcesUseCase = new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>());
        return new SourcesPresentationModel(getSerializableData(), sourcesUseCase);
    }

    @Override
    protected SourcesAdapter getAdapter() {
        return new SourcesAdapter(this);
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(this);
    }

    @Override
    protected Class<? extends AppCompatActivity> getNextActivity() {
        return ArticlesActivityRxPM.class;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupViews();
        setupStates();
    }

    @Override
    public void onClickSourceAbout(Source source) {
        aboutClickedSubject.accept(source);
    }

    private Serializable getSerializableData() {
        return getIntent().getSerializableExtra(ExtraKeys.ITEM_KEY);
    }

    private void setupViews() {
        aboutClickedSubject.subscribe(presentationModel.aboutButtonClickConsumer);
    }

    private void setupStates() {
        presentationModel.navigateBrowserByUrlState.subscribe(this::navigateBrowserByUrl);
    }

    private void navigateBrowserByUrl(String url) {
        BrowserUtils.navigateBrowser(this, url, e -> showToastMessage(R.string.error));
    }

}
