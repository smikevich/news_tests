package siarhei.mikevich.newstests.presentationrxpm.views.button;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.widget.ImageView;

import siarhei.mikevich.newstests.presentationrxpm.R;

/**
 * Created by Siarhei Mikevich on 10/17/17.
 */

public class ButtonRippleCircle extends LinearLayoutCompat {

    private ImageView imageView;
    private int iconId;

    public ButtonRippleCircle(Context context) {
        super(context);
        init();
    }

    public ButtonRippleCircle(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttrs(attrs);
        init();
    }

    public ButtonRippleCircle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(attrs);
        init();
    }

    private void initAttrs(AttributeSet attrs) {
        TypedArray a = getContext().getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.ButtonRippleCircle,
                0, 0);

        try {
            iconId = a.getResourceId(R.styleable.ButtonRippleCircle_button_ripple_circle_icon, 0);
        } finally {
            a.recycle();
        }
    }

    private void init() {
        inflate(getContext(), R.layout.view_button_ripple_circle, this);
        bindViews();
        setupIcon();
    }

    private void bindViews() {
        imageView = findViewById(R.id.icon_image_view);
    }

    private void setupIcon() {
        if (iconId != 0) {
            imageView.setImageResource(iconId);
        }
    }

}
