package siarhei.mikevich.newstests.presentationrxpm.utils.transformer;

import android.support.annotation.NonNull;

import rx.Observable;
import siarhei.mikevich.newstests.presentationrxpm.views.loadingdialog.LoadingView;

/**
 * Created by mikes on 25.10.17.
 */

public class LoadingTransformer<T> implements Observable.Transformer<T, T> {

    private final LoadingView loadingView;

    public LoadingTransformer(@NonNull final LoadingView loadingView) {
        this.loadingView = loadingView;
    }

    @Override
    public Observable<T> call(Observable<T> observable) {
        return observable
                .doOnSubscribe(loadingView::showLoadingView)
                .doOnTerminate(loadingView::hideLoadingView);
    }

}
