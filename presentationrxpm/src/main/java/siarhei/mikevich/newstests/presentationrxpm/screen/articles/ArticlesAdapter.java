package siarhei.mikevich.newstests.presentationrxpm.screen.articles;

import android.view.View;

import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.presentationrxpm.R;
import siarhei.mikevich.newstests.presentationrxpm.screen.base.adapter.BaseAdapter;
import siarhei.mikevich.newstests.presentationrxpm.screen.base.adapter.BaseViewHolder;
import siarhei.mikevich.newstests.presentationrxpm.screen.base.adapter.OnItemClickListener;
import siarhei.mikevich.newstests.presentationrxpm.views.items.ArticleView;

/**
 * Created by Siarhei Mikevich on 10/11/17.
 */

class ArticlesAdapter extends BaseAdapter<ArticlesAdapter.ViewHolder, Article, OnItemClickListener<Article>> {

    ArticlesAdapter(OnItemClickListener<Article> listener) {
        super(listener);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item_article;
    }

    @Override
    protected ViewHolder getViewHolder(View itemView) {
        return new ViewHolder(itemView);
    }

    @Override
    protected void setupView(ViewHolder viewHolder, Article item) {
        viewHolder.setupView(item);
    }

    class ViewHolder extends BaseViewHolder<ArticleView, Article> {

        ViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected int getViewId() {
            return R.id.article_view;
        }

        @Override
        protected void setupView(ArticleView view, Article item) {
            view.setup(item);
        }

    }

}
