package siarhei.mikevich.newstests.presentationrxpm.screen.base.activity.list;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.jakewharton.rxrelay2.BehaviorRelay;

import java.io.Serializable;
import java.util.List;

import siarhei.mikevich.newstests.presentationrxpm.R;
import siarhei.mikevich.newstests.presentationrxpm.screen.base.activity.BaseActivityRxPM;
import siarhei.mikevich.newstests.presentationrxpm.screen.base.adapter.BaseAdapter;
import siarhei.mikevich.newstests.presentationrxpm.screen.base.adapter.OnItemClickListener;
import siarhei.mikevich.newstests.presentationrxpm.utils.ExtraKeys;

/**
 * Created by mikes on 19.12.2017.
 */

public abstract class BaseListActivityRxPM
        <
        T,
        PM extends BaseListPresentationModel<T>,
        A extends BaseAdapter<? extends RecyclerView.ViewHolder, T, ? extends OnItemClickListener<T>>
        >
        extends BaseActivityRxPM<PM>
        implements OnItemClickListener<T> {

    private RecyclerView recyclerView;
    private SwipeRefreshLayout refreshLayout;
    private TextView errorTextView;
    private A adapter;

    private BehaviorRelay<Boolean> refreshSubject = BehaviorRelay.create();
    private BehaviorRelay<T> itemClickedSubject = BehaviorRelay.create();

    protected abstract A getAdapter();

    protected abstract RecyclerView.LayoutManager getLayoutManager();

    protected abstract Class<? extends AppCompatActivity> getNextActivity();

    @Override
    protected int getLayoutId() {
        return R.layout.activity_list;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindViews();
        setupViews();
        setupStates();
    }

    @Override
    public void onItemClicked(T item) {
        itemClickedSubject.accept(item);
    }

    private void bindViews() {
        recyclerView = findViewById(R.id.recycler_view);
        refreshLayout = findViewById(R.id.refresh_layout);
        errorTextView = findViewById(R.id.error_text_view);
    }

    private void setupViews() {
        recyclerView.setLayoutManager(getLayoutManager());
        adapter = getAdapter();
        recyclerView.setAdapter(adapter);
        refreshLayout.setOnRefreshListener(() -> refreshSubject.accept(true));
        refreshLayout.setColorSchemeResources(R.color.colorAccent);
        refreshSubject.subscribe(presentationModel.loadItemsConsumer);
        itemClickedSubject.subscribe(presentationModel.clickItemConsumer);
    }

    private void setupStates() {
        presentationModel.listState.subscribe(this::showData);
        presentationModel.visibilityContentState.subscribe(this::setVisibilityContent);
        presentationModel.loadingState.subscribe(this::loadingProgress);
        presentationModel.navigateNextState.subscribe(this::navigateNext);
        presentationModel.errorMessageState.subscribe(this::showErrorMessage);
        presentationModel.errorMessageIdState.subscribe(this::showErrorMessage);
    }

    private void showData(List<T> list) {
        adapter.changeDataSet(list);
    }

    private void showErrorMessage(String errorMessage) {
        showErrorMessageText(errorMessage);
    }

    private void showErrorMessage(@StringRes int errorMessageId) {
        showErrorMessageText(getString(errorMessageId));
    }

    private void showErrorMessageText(String errorMessageText) {
        errorTextView.setText(errorMessageText);
    }

    private void setVisibilityContent(boolean isVisible) {
        recyclerView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        errorTextView.setVisibility(isVisible ? View.GONE : View.VISIBLE);
    }

    private void loadingProgress(boolean isLoading) {
        if (isLoading) {
            super.showLoadingView();
        } else {
            super.hideLoadingView();
            refreshLayout.setRefreshing(false);
        }
    }

    private void navigateNext(T item) {
        Intent intent = new Intent(this, getNextActivity());
        intent.putExtra(ExtraKeys.ITEM_KEY, (Serializable) item);
        startActivity(intent);
    }

}
