package siarhei.mikevich.newstests.presentationrxpm;

import android.support.annotation.StringRes;

import java.io.IOException;
import java.io.InputStream;

import siarhei.mikevich.newstests.data.DataConfig;
import siarhei.mikevich.newstests.data.utils.ErrorCode;

/**
 * Created by Siarhei Mikevich on 11/22/17.
 */

public class PresentationConfig implements DataConfig {

    @Override
    public String getStringResourceByErrorCode(String errorCode) {
        @StringRes int errorMessageResourceId;
        switch(errorCode) {
            case ErrorCode.ERROR_GETTING_RESPONSE_FROM_ASSETS:
                errorMessageResourceId = R.string.error_getting_response_from_assets;
                break;
            default:
                errorMessageResourceId = R.string.error;
                break;
        }
        return AppDelegate.getContext().getString(errorMessageResourceId);
    }

    @Override
    public InputStream getJsonInputStreamFromAssets(String assetPath) throws IOException {
        return AppDelegate.getContext().getAssets().open(assetPath);
    }

}
