package siarhei.mikevich.newstests.presentationrxpm.screen.categories;

import android.support.annotation.NonNull;

import rx.Observable;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Category;
import siarhei.mikevich.newstests.domain.usecase.CategoriesUseCase;
import siarhei.mikevich.newstests.presentationrxpm.screen.base.activity.list.BaseListPresentationModel;

/**
 * Created by mikes on 19.12.2017.
 */

public class CategoriesPresentationModel extends BaseListPresentationModel<Category> {

    private final CategoriesUseCase categoriesUseCase;

    CategoriesPresentationModel(@NonNull CategoriesUseCase categoriesUseCase) {
        this.categoriesUseCase = categoriesUseCase;
    }

    @Override
    protected Observable<BaseItemsContent<Category>> getObservable() {
        return categoriesUseCase.execUseCase(new CategoriesUseCase.Params("categories.json"));
    }

}
