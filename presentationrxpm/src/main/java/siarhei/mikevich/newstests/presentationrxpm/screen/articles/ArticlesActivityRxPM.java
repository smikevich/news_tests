package siarhei.mikevich.newstests.presentationrxpm.screen.articles;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.jakewharton.rxrelay2.BehaviorRelay;

import java.io.Serializable;

import siarhei.mikevich.newstests.data.api.utils.Filter;
import siarhei.mikevich.newstests.data.repository.RepositoryProvider;
import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.domain.usecase.ArticlesUseCase;
import siarhei.mikevich.newstests.presentationrxpm.R;
import siarhei.mikevich.newstests.presentationrxpm.screen.base.activity.list.BaseListActivityRxPM;
import siarhei.mikevich.newstests.presentationrxpm.screen.base.adapter.OnItemClickListener;
import siarhei.mikevich.newstests.presentationrxpm.screen.detailsrxpm.DetailsActivityRxPM;
import siarhei.mikevich.newstests.presentationrxpm.utils.ExtraKeys;
import siarhei.mikevich.newstests.presentationrxpm.utils.transformer.AsyncTransformer;

/**
 * Created by mikes on 19.12.2017.
 */

public class ArticlesActivityRxPM extends BaseListActivityRxPM<Article, ArticlesPresentationModel, ArticlesAdapter>
        implements OnItemClickListener<Article> {

    private BehaviorRelay<String> filterClickedSubject = BehaviorRelay.create();

    @Override
    protected int getActivityTitleId() {
        return R.string.title_articles;
    }

    @Override
    protected ArticlesPresentationModel getPresentationModel() {
        ArticlesUseCase articlesUseCase = new ArticlesUseCase(RepositoryProvider.provideArticlesRepository(), new AsyncTransformer<>());
        return new ArticlesPresentationModel(getSerializableData(), articlesUseCase);
    }

    @Override
    protected ArticlesAdapter getAdapter() {
        return new ArticlesAdapter(this);
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(this);
    }

    @Override
    protected Class<? extends AppCompatActivity> getNextActivity() {
        return DetailsActivityRxPM.class;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupStates();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.filter_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.filter_menu_top:
                filterClickedSubject.accept(Filter.TOP);
                return true;
            case R.id.filter_menu_latest:
                filterClickedSubject.accept(Filter.LATEST);
                return true;
            case R.id.filter_menu_popular:
                filterClickedSubject.accept(Filter.POPULAR);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private Serializable getSerializableData() {
        return getIntent().getSerializableExtra(ExtraKeys.ITEM_KEY);
    }

    private void setupStates() {
        filterClickedSubject.subscribe(presentationModel.filterArticlesActionConsumer);
    }

}
