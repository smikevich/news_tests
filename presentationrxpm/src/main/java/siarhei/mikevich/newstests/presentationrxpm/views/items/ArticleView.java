package siarhei.mikevich.newstests.presentationrxpm.views.items;

import android.content.Context;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.widget.TextView;

import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.domain.utils.unit.DateUtils;
import siarhei.mikevich.newstests.presentationrxpm.R;
import siarhei.mikevich.newstests.presentationrxpm.views.icon.ImageViewFresco;

/**
 * Created by Siarhei Mikevich on 10/7/17.
 */

public class ArticleView extends LinearLayoutCompat {

    private ImageViewFresco articleImageView;
    private TextView articleTitleTextView;
    private TextView articleDateTextView;

    public ArticleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public ArticleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ArticleView(Context context) {
        super(context);
        init();
    }

    public void setup(Article article) {
        articleImageView.setImageURI(article.getUrlToImage());
        articleTitleTextView.setText(article.getTitle());
        articleDateTextView.setText(DateUtils.parseDateTime(article.getPublishedAt()));
    }

    private void init() {
        inflate(getContext(), R.layout.view_article, this);
        bindViews();
    }

    private void bindViews() {
        articleImageView = findViewById(R.id.article_image_view);
        articleTitleTextView = findViewById(R.id.article_title_text_view);
        articleDateTextView = findViewById(R.id.article_date_text_view);
    }

}
