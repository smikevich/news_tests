package siarhei.mikevich.newstests.presentationmvvm.screen.base.activity;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import siarhei.mikevich.newstests.presentationmvvm.R;
import siarhei.mikevich.newstests.presentationmvvm.views.loadingdialog.LoadingDialog;
import siarhei.mikevich.newstests.presentationmvvm.views.loadingdialog.LoadingView;

/**
 * Created by mikes on 30.11.2017.
 */

public abstract class BaseActivityDataBinding<P extends BaseContract.Presenter, VDB extends ViewDataBinding> extends AppCompatActivity {

    private Toolbar toolbar;
    private LoadingView loadingView;

    protected P presenter;
    protected VDB viewDataBinding;

    @StringRes
    protected abstract int getActivityTitleId();

    protected abstract P getPresenter();

    protected abstract VDB getViewDataBinding();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupViewDataBinding();
        setupPresenter();
        bindViews();
        setupViews();
        setTitle(getActivityTitleId());
    }

    // loading dialog

    protected void showLoadingView() {
        loadingView.showLoadingView();
    }

    protected void hideLoadingView() {
        loadingView.hideLoadingView();
    }

    // toast message information

    protected void showToastMessage(@StringRes int toastMessageId) {
        Toast.makeText(this, getString(toastMessageId, Toast.LENGTH_SHORT), Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // private methods

    private void bindViews() {
        toolbar = findViewById(R.id.toolbar);
        loadingView = LoadingDialog.init(getSupportFragmentManager());
    }

    private void setupViews() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    private void setupViewDataBinding() {
        viewDataBinding = getViewDataBinding();
    }

    private void setupPresenter() {
        presenter = getPresenter();
        presenter.onCreate();
    }

}
