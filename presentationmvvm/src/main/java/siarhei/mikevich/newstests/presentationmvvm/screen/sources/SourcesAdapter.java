package siarhei.mikevich.newstests.presentationmvvm.screen.sources;

import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.widget.FrameLayout;

import siarhei.mikevich.newstests.domain.model.content.Source;
import siarhei.mikevich.newstests.presentationmvvm.BR;
import siarhei.mikevich.newstests.presentationmvvm.R;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.adapter.BaseAdapter;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.adapter.BaseViewHolder;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.adapter.OnItemClickListener;

/**
 * Created by Siarhei Mikevich on 10/11/17.
 */

public class SourcesAdapter extends BaseAdapter<SourcesAdapter.ViewHolder, Source, OnItemClickListener<Source>> {

    private OnSourceClickListener aboutListener;

    @Override
    protected int getLayoutId() {
        return R.layout.view_source_mvvm;
    }

    @Override
    protected ViewHolder getViewHolder(ViewDataBinding viewDataBinding) {
        return new ViewHolder(viewDataBinding);
    }

    @Override
    protected void setupView(ViewHolder viewHolder, Source item) {
        viewHolder.getAboutButton().setOnClickListener(v -> onClickSourceAbout(item));
        viewHolder.setupView(item);
    }

    private void onClickSourceAbout(Source item) {
        if (aboutListener != null) {
            aboutListener.onClickSourceAbout(item);
        }
    }

    public void setAboutListener(OnSourceClickListener aboutListener) {
        this.aboutListener = aboutListener;
    }

    class ViewHolder extends BaseViewHolder<Source> {

        ViewHolder(ViewDataBinding viewDataBinding) {
            super(viewDataBinding);
        }

        @Override
        protected void setupView(ViewDataBinding viewDataBinding, Source item) {
            viewDataBinding.setVariable(BR.source, item);
        }

        @NonNull
        FrameLayout getAboutButton() {
            return viewDataBinding.getRoot().findViewById(R.id.source_about_button_ripple_circle).findViewById(R.id.button);
        }

    }

}
