package siarhei.mikevich.newstests.presentationmvvm.utils.browser;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by Siarhei Mikevich on 11/17/17.
 */

public class BrowserUtils {

    public static void navigateBrowser(Context activity, String url, OnBrowserListener listener) {
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            listener.onError(e);
        } catch (NullPointerException e) {
            e.printStackTrace();
            listener.onError(new Exception());
        }
    }

}
