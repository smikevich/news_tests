package siarhei.mikevich.newstests.presentationmvvm.screen.base.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.ColorRes;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.utils.unit.TextUtils;
import siarhei.mikevich.newstests.presentationmvvm.BR;
import siarhei.mikevich.newstests.presentationmvvm.R;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.adapter.BaseAdapter;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.adapter.OnItemClickListener;
import siarhei.mikevich.newstests.presentationmvvm.utils.ExtraKeys;
import siarhei.mikevich.newstests.presentationmvvm.views.loadingdialog.LoadingView;

/**
 * Created by mikes on 02.12.2017.
 */

public abstract class BaseListViewModel<T, A extends BaseAdapter<? extends RecyclerView.ViewHolder, T, ? extends OnItemClickListener<T>>> extends BaseObservable implements BaseViewModelContract {

    protected final Context context;
    protected final Intent intent;
    private final LoadingView loadingView;

    private boolean showProgress = false;
    private boolean visibilityContent = false;
    private String errorMessage = "";
    private List<T> list;

    public BaseListViewModel(Context context, Intent intent, LoadingView loadingView) {
        this.context = context;
        this.intent = intent;
        this.loadingView = loadingView;
        list = new ArrayList<>();
    }

    protected abstract Observable<BaseItemsContent<T>> getObservable();

    protected abstract A getAdapter();

    protected abstract RecyclerView.LayoutManager getLayoutManager();

    protected abstract Class<? extends AppCompatActivity> getNextActivity();

    @Override
    public void init() {
        loadItems();
    }

    private void loadItems() {
        getObservable()
                .compose(loadingDialog())
                .subscribe(this::handleResponse, throwable -> {
                    throwable.printStackTrace();
                    setupFailureScreen(R.string.error_list_empty, true);
                });
    }

    private void handleResponse(BaseItemsContent<T> itemsContent) {
        List<T> items = itemsContent.getItems();
        if (items.size() == 0) {
            String errorMessage = itemsContent.getErrorMessage();
            if (TextUtils.isEmpty(errorMessage)) {
                setupFailureScreen(R.string.error_list_empty, false);
            } else {
                setupFailureScreen(errorMessage);
            }
        } else {
            setupSuccessScreen(items);
        }
    }

    private void setupSuccessScreen(List<T> list) {
        setList(list);
        setVisibilityContent(true);
    }

    private void setupFailureScreen(String errorMessage) {
        setupFailureScreen(true);
        setErrorMessage(errorMessage);
    }

    private void setupFailureScreen(@StringRes int errorMessageId, boolean isShouldToastShow) {
        setupFailureScreen(isShouldToastShow);
        setErrorMessage(context.getString(errorMessageId));
    }

    private void setupFailureScreen(boolean isShouldToastShow) {
        setList(new ArrayList<>());
        setVisibilityContent(false);
        if (isShouldToastShow) {
            showToastMessage();
        }
    }

    public void onItemClicked(Object object) {
        Intent intent = new Intent(context, getNextActivity());
        intent.putExtra(ExtraKeys.ITEM_KEY, (Serializable) object);
        context.startActivity(intent);
    }

    public void onRefresh() {
        loadItems();
    }

    @Bindable
    public boolean isShowProgress() {
        return showProgress;
    }

    public void setShowProgress(boolean showProgress) {
        if (showProgress) {
            loadingView.showLoadingView();
        } else {
            loadingView.hideLoadingView();
        }
        this.showProgress = showProgress;
        notifyPropertyChanged(BR.showProgress);
    }

    @Bindable
    public boolean isVisibilityContent() {
        return visibilityContent;
    }

    public void setVisibilityContent(boolean visibilityContent) {
        this.visibilityContent = visibilityContent;
        notifyPropertyChanged(BR.visibilityContent);
    }

    @Bindable
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        notifyPropertyChanged(BR.errorMessage);
    }

    @Bindable
    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyPropertyChanged(BR.list);
    }

    @Bindable
    public A getRecyclerAdapter() {
        return getAdapter();
    }

    @Bindable
    public RecyclerView.LayoutManager getRecyclerLayoutManager() {
        return getLayoutManager();
    }

    @Bindable
    public @ColorRes int getRefreshColorScheme() {
        return R.color.colorAccent;
    }


    private Observable.Transformer<BaseItemsContent<T>, BaseItemsContent<T>> loadingDialog() {
        return observable -> observable.doOnSubscribe(() -> setShowProgress(true)).doOnTerminate(() -> setShowProgress(false));
    }

    protected void showToastMessage() {
        Toast.makeText(context, R.string.error, Toast.LENGTH_SHORT).show();
    }

}
