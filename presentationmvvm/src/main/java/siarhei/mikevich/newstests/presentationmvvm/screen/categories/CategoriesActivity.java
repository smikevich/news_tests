package siarhei.mikevich.newstests.presentationmvvm.screen.categories;

import android.databinding.DataBindingUtil;

import siarhei.mikevich.newstests.data.repository.RepositoryProvider;
import siarhei.mikevich.newstests.domain.usecase.CategoriesUseCase;
import siarhei.mikevich.newstests.presentationmvvm.R;
import siarhei.mikevich.newstests.presentationmvvm.databinding.ActivityListMvvmBinding;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.activity.BaseActivity;
import siarhei.mikevich.newstests.presentationmvvm.utils.transformer.AsyncTransformer;
import siarhei.mikevich.newstests.presentationmvvm.views.loadingdialog.LoadingView;

/**
 * Created by mikes on 02.12.2017.
 */

public class CategoriesActivity extends BaseActivity<CategoriesViewModel, ActivityListMvvmBinding> {

    @Override
    protected CategoriesViewModel getViewModel(LoadingView loadingView) {
        return new CategoriesViewModel(this, getIntent(), new CategoriesUseCase(RepositoryProvider.provideCategoriesRepository(), new AsyncTransformer<>()), loadingView);
    }

    @Override
    protected ActivityListMvvmBinding getViewDataBinding() {
        return DataBindingUtil.setContentView(this, R.layout.activity_list_mvvm);
    }

    @Override
    protected void setupViewDataBinding() {
        viewDataBinding.setModel(viewModel);
    }

    @Override
    protected int getActivityTitleId() {
        return R.string.title_categories;
    }

}
