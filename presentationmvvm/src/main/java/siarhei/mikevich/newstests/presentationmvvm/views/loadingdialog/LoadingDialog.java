package siarhei.mikevich.newstests.presentationmvvm.views.loadingdialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.View;

import siarhei.mikevich.newstests.presentationmvvm.R;

/**
 * Created by mikes on 05.10.17.
 */

public class LoadingDialog extends DialogFragment {

    public static LoadingView init(FragmentManager fragmentManager) {
        return new LoadingDialogManager(fragmentManager);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureDialog();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setView(View.inflate(getActivity(), R.layout.dialog_loading, null))
                .create();
    }

    private void configureDialog() {
        setStyle(STYLE_NO_TITLE, getTheme());
        setCancelable(false);
    }

    private static class LoadingDialogManager implements LoadingView {

        private FragmentManager fragmentManager;

        LoadingDialogManager(FragmentManager fragmentManager) {
            this.fragmentManager = fragmentManager;
        }

        @Override
        public void showLoadingView() {
            if (fragmentManager.findFragmentByTag(LoadingDialog.class.getName()) == null) {
                DialogFragment dialog = new LoadingDialog();
                dialog.show(fragmentManager, LoadingDialog.class.getName());
            }
        }

        @Override
        public void hideLoadingView() {
            LoadingDialog dialog = (LoadingDialog) fragmentManager.findFragmentByTag(LoadingDialog.class.getName());
            if (dialog != null) {
                dialog.dismissAllowingStateLoss();
            }
        }

    }

}
