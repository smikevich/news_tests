package siarhei.mikevich.newstests.presentationmvvm.screen.details.mvvm;

import android.content.Context;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.view.View;
import android.widget.Toast;

import java.io.Serializable;

import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.presentationmvvm.BR;
import siarhei.mikevich.newstests.presentationmvvm.R;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.activity.BaseViewModelContract;
import siarhei.mikevich.newstests.presentationmvvm.utils.ExtraKeys;
import siarhei.mikevich.newstests.presentationmvvm.utils.browser.BrowserUtils;

/**
 * Created by Siarhei Mikevich on 12/1/17.
 */

public class DetailsViewModel extends BaseObservable implements BaseViewModelContract {

    private Context context;
    private Intent intent;
    private Article article;

    private boolean hideFloatingActionButton = true;

    public DetailsViewModel(Context context, Intent intent) {
        this.context = context;
        this.intent = intent;
    }

    @Override
    public void init() {
        Article article = loadArticle();
        setArticle(article);
    }

    private Article loadArticle() {
        Serializable serializable = intent.getSerializableExtra(ExtraKeys.ITEM_KEY);
        if (serializable != null && serializable instanceof Article) {
            return (Article) serializable;
        } else {
            showErrorToastMessage();
            return new Article();
        }
    }

    public void onClickReadMore(@NonNull View view) {
        if (article != null) {
            BrowserUtils.navigateBrowser(context, article.getUrl(), e -> showErrorToastMessage());
        } else {
            showErrorToastMessage();
        }
    }

    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        int totalScrollRange = appBarLayout.getTotalScrollRange();
        int percentage = (Math.abs(verticalOffset)) * 100 / totalScrollRange;
        if (percentage >= 20 && !hideFloatingActionButton) {
            setHideFloatingActionButton(true);
        }
        else if(percentage < 20 && hideFloatingActionButton){
            setHideFloatingActionButton(false);
        }
    }

    private void showErrorToastMessage() {
        Toast.makeText(context, R.string.error, Toast.LENGTH_SHORT).show();
    }

    @Bindable
    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
        notifyPropertyChanged(BR.article);
    }

    @Bindable
    public boolean isHideFloatingActionButton() {
        return hideFloatingActionButton;
    }

    public void setHideFloatingActionButton(boolean hideFAB) {
        this.hideFloatingActionButton = hideFAB;
        notifyPropertyChanged(BR.hideFloatingActionButton);
    }

}
