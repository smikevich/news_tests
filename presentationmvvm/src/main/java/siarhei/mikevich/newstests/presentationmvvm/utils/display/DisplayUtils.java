package siarhei.mikevich.newstests.presentationmvvm.utils.display;

import android.content.Context;

/**
 * Created by Siarhei Mikevich on 10/18/17.
 */

public class DisplayUtils {

    public static int getPixelsByDp(int dp, Context context) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

}
