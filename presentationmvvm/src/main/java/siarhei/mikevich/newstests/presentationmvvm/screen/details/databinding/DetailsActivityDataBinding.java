package siarhei.mikevich.newstests.presentationmvvm.screen.details.databinding;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;

import java.io.Serializable;

import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.presentationmvvm.R;
import siarhei.mikevich.newstests.presentationmvvm.databinding.ActivityDetailsDataBindingBinding;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.activity.BaseActivityDataBinding;
import siarhei.mikevich.newstests.presentationmvvm.utils.ExtraKeys;
import siarhei.mikevich.newstests.presentationmvvm.utils.browser.BrowserUtils;

/**
 * Created by Siarhei Mikevich on 10/7/17.
 */

public class DetailsActivityDataBinding
        extends BaseActivityDataBinding<DetailsContract.Presenter, ActivityDetailsDataBindingBinding>
        implements DetailsContract.View, AppBarLayout.OnOffsetChangedListener {

    private AppBarLayout appBarLayout;
    private FloatingActionButton aboutFloatingActionButton;

    @Override
    protected int getActivityTitleId() {
        return R.string.title_details;
    }

    @Override
    protected DetailsContract.Presenter getPresenter() {
        return new DetailsPresenter(this);
    }

    @Override
    protected ActivityDetailsDataBindingBinding getViewDataBinding() {
        return DataBindingUtil.setContentView(this, R.layout.activity_details_data_binding);
    }

    @Override
    public void init() {
        bindViews();
        setupViews();
        loadArticle();
    }

    @Override
    public void showToastMessage(int messageId) {
        super.showToastMessage(messageId);
    }

    @Override
    public void showArticle(Article article) {
        viewDataBinding.setArticle(article);
        viewDataBinding.setPresenter(presenter);
    }

    @Override
    public void navigateBrowserByUrl(String url) {
        BrowserUtils.navigateBrowser(this, url, e -> showToastMessage(R.string.error));
    }

    @Override
    public void showAboutButton() {
        ViewCompat.animate(aboutFloatingActionButton).scaleY(1).scaleX(1).start();
    }

    @Override
    public void hideAboutButton() {
        ViewCompat.animate(aboutFloatingActionButton).scaleY(0).scaleX(0).start();
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        presenter.onOffsetChanged(appBarLayout.getTotalScrollRange(), verticalOffset);
    }

    private void bindViews() {
        appBarLayout = findViewById(R.id.app_bar_layout);
        aboutFloatingActionButton = findViewById(R.id.about_floating_action_button);
    }

    private void setupViews() {
        appBarLayout.addOnOffsetChangedListener(this);
    }

    private void loadArticle() {
        Intent intent = getIntent();
        Serializable serializable = intent.getSerializableExtra(ExtraKeys.ITEM_KEY);
        presenter.loadArticleFromSerializable(serializable);
    }

}
