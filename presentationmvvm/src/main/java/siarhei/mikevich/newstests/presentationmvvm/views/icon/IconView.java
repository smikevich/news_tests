package siarhei.mikevich.newstests.presentationmvvm.views.icon;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import siarhei.mikevich.newstests.domain.utils.unit.StringUtils;
import siarhei.mikevich.newstests.domain.utils.unit.TextUtils;
import siarhei.mikevich.newstests.presentationmvvm.R;

/**
 * Created by Siarhei Mikevich on 10/9/17.
 */

public class IconView extends FrameLayout {

    private TextView symbolsTextView;

    public IconView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public IconView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public IconView(Context context) {
        super(context);
        init();
    }

    public void setup(String text) {
        String abbreviation = new StringUtils().getAbbreviation(text);
        if (TextUtils.isEmpty(abbreviation)) {
            this.setVisibility(View.GONE);
        } else {
            symbolsTextView.setText(abbreviation);
        }
    }

    private void init() {
        inflate(getContext(), R.layout.view_icon, this);
        bindViews();
    }

    private void bindViews() {
        symbolsTextView = findViewById(R.id.symbols_text_view);
    }

}
