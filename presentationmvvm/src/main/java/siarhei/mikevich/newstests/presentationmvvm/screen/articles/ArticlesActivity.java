package siarhei.mikevich.newstests.presentationmvvm.screen.articles;

import android.databinding.DataBindingUtil;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import siarhei.mikevich.newstests.data.api.utils.Filter;
import siarhei.mikevich.newstests.data.repository.RepositoryProvider;
import siarhei.mikevich.newstests.domain.usecase.ArticlesUseCase;
import siarhei.mikevich.newstests.presentationmvvm.R;
import siarhei.mikevich.newstests.presentationmvvm.databinding.ActivityListMvvmBinding;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.activity.BaseActivity;
import siarhei.mikevich.newstests.presentationmvvm.utils.transformer.AsyncTransformer;
import siarhei.mikevich.newstests.presentationmvvm.views.loadingdialog.LoadingView;

/**
 * Created by Siarhei Mikevich on 12/5/17.
 */

public class ArticlesActivity extends BaseActivity<ArticlesViewModel, ActivityListMvvmBinding> {

    @Override
    protected ArticlesViewModel getViewModel(LoadingView loadingView) {
        return new ArticlesViewModel(this, getIntent(), new ArticlesUseCase(RepositoryProvider.provideArticlesRepository(), new AsyncTransformer<>()), loadingView);
    }

    @Override
    protected ActivityListMvvmBinding getViewDataBinding() {
        return DataBindingUtil.setContentView(this, R.layout.activity_list_mvvm);
    }

    @Override
    protected void setupViewDataBinding() {
        viewDataBinding.setModel(viewModel);
    }

    @Override
    protected int getActivityTitleId() {
        return R.string.title_articles;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.filter_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.filter_menu_top:
                filterArticles(Filter.TOP);
                return true;
            case R.id.filter_menu_latest:
                filterArticles(Filter.LATEST);
                return true;
            case R.id.filter_menu_popular:
                filterArticles(Filter.POPULAR);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void filterArticles(@Filter String filter) {
        viewModel.filterArticles(filter);
    }

}
