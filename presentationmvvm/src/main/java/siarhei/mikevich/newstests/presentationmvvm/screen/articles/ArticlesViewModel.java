package siarhei.mikevich.newstests.presentationmvvm.screen.articles;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.io.Serializable;

import rx.Observable;
import siarhei.mikevich.newstests.data.api.utils.Filter;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.domain.model.content.Source;
import siarhei.mikevich.newstests.domain.usecase.ArticlesUseCase;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.activity.BaseListViewModel;
import siarhei.mikevich.newstests.presentationmvvm.screen.details.mvvm.DetailsActivity;
import siarhei.mikevich.newstests.presentationmvvm.utils.ExtraKeys;
import siarhei.mikevich.newstests.presentationmvvm.views.loadingdialog.LoadingView;

/**
 * Created by Siarhei Mikevich on 12/5/17.
 */

public class ArticlesViewModel extends BaseListViewModel<Article, ArticlesAdapter> {

    private final ArticlesUseCase useCase;

    private Source source;
    private @Filter String filter = Filter.TOP;

    public ArticlesViewModel(Context context, Intent intent, ArticlesUseCase useCase, LoadingView loadingView) {
        super(context, intent, loadingView);
        this.useCase = useCase;
    }

    @Override
    protected Observable<BaseItemsContent<Article>> getObservable() {
        return useCase.execUseCase(new ArticlesUseCase.Params(source.getId(), filter));
    }

    @Override
    protected ArticlesAdapter getAdapter() {
        return new ArticlesAdapter();
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(context);
    }

    @Override
    protected Class<? extends AppCompatActivity> getNextActivity() {
        return DetailsActivity.class;
    }

    @Override
    public void init() {
        loadSource();
        super.init();
    }

    public void filterArticles(@Filter String filter) {
        this.filter = filter;
        super.init();
    }

    private void loadSource() {
        Serializable serializable = intent.getSerializableExtra(ExtraKeys.ITEM_KEY);
        if (serializable != null && serializable instanceof Source) {
            source = (Source) serializable;
        } else {
            showToastMessage();
            source = new Source();
        }
    }

}
