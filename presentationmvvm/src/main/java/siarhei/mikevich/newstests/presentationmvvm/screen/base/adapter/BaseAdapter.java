package siarhei.mikevich.newstests.presentationmvvm.screen.base.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Siarhei Mikevich on 10/11/17.
 */

public abstract class BaseAdapter<VH extends RecyclerView.ViewHolder, T, L extends OnItemClickListener<T>> extends RecyclerView.Adapter<VH> {

    protected L listener;
    private final List<T> list = new ArrayList<>();

    @LayoutRes
    protected abstract int getLayoutId();

    protected abstract VH getViewHolder(ViewDataBinding itemView);

    protected abstract void setupView(VH viewHolder, T item);

    public void changeDataSet(List<T> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void setListener(L listener) {
        this.listener = listener;
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding itemView = DataBindingUtil.inflate(inflater, getLayoutId(), parent, false);
        return getViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        T item = list.get(position);
        setupView(holder, item);
        holder.itemView.setOnClickListener(v -> onClickType(item));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void onClickType(T item) {
        if (listener != null) {
            listener.onItemClicked(item);
        }
    }

}
