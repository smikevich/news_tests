package siarhei.mikevich.newstests.presentationmvvm.screen.sources;

import android.databinding.DataBindingUtil;

import siarhei.mikevich.newstests.data.repository.RepositoryProvider;
import siarhei.mikevich.newstests.domain.usecase.SourcesUseCase;
import siarhei.mikevich.newstests.presentationmvvm.R;
import siarhei.mikevich.newstests.presentationmvvm.databinding.ActivityListMvvmBinding;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.activity.BaseActivity;
import siarhei.mikevich.newstests.presentationmvvm.utils.transformer.AsyncTransformer;
import siarhei.mikevich.newstests.presentationmvvm.views.loadingdialog.LoadingView;

/**
 * Created by Siarhei Mikevich on 12/5/17.
 */

public class SourcesActivity extends BaseActivity<SourcesViewModel, ActivityListMvvmBinding> {

    @Override
    protected SourcesViewModel getViewModel(LoadingView loadingView) {
        return new SourcesViewModel(this, getIntent(), new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()), loadingView);
    }

    @Override
    protected ActivityListMvvmBinding getViewDataBinding() {
        return DataBindingUtil.setContentView(this, R.layout.activity_list_mvvm);
    }

    @Override
    protected void setupViewDataBinding() {
        viewDataBinding.setModel(viewModel);
    }

    @Override
    protected int getActivityTitleId() {
        return R.string.title_sources;
    }

}
