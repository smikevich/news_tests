package siarhei.mikevich.newstests.presentationmvvm.screen.sources;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.io.Serializable;

import rx.Observable;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Category;
import siarhei.mikevich.newstests.domain.model.content.Source;
import siarhei.mikevich.newstests.domain.usecase.SourcesUseCase;
import siarhei.mikevich.newstests.presentationmvvm.screen.articles.ArticlesActivity;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.activity.BaseListViewModel;
import siarhei.mikevich.newstests.presentationmvvm.utils.ExtraKeys;
import siarhei.mikevich.newstests.presentationmvvm.utils.browser.BrowserUtils;
import siarhei.mikevich.newstests.presentationmvvm.views.loadingdialog.LoadingView;

/**
 * Created by Siarhei Mikevich on 12/5/17.
 */

public class SourcesViewModel extends BaseListViewModel<Source, SourcesAdapter> {

    private final SourcesUseCase useCase;
    private Category category;

    public SourcesViewModel(Context context, Intent intent, SourcesUseCase useCase, LoadingView loadingView) {
        super(context, intent, loadingView);
        this.useCase = useCase;
    }

    @Override
    protected Observable<BaseItemsContent<Source>> getObservable() {
        return useCase.execUseCase(new SourcesUseCase.Params(category.getId()));
    }

    @Override
    protected SourcesAdapter getAdapter() {
        return new SourcesAdapter();
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(context);
    }

    @Override
    protected Class<? extends AppCompatActivity> getNextActivity() {
        return ArticlesActivity.class;
    }

    @Override
    public void init() {
        loadCategory();
        super.init();
    }

    private void loadCategory() {
        Serializable serializable = intent.getSerializableExtra(ExtraKeys.ITEM_KEY);
        if (serializable != null && serializable instanceof Category) {
            category = (Category) serializable;
        } else {
            showToastMessage();
            category = new Category();
        }
    }

    public void onAboutClicked(Source source) {
        BrowserUtils.navigateBrowser(context, source.getUrl(), e -> showToastMessage());
    }

}
