package siarhei.mikevich.newstests.presentationmvvm.utils.browser;

/**
 * Created by Siarhei Mikevich on 11/17/17.
 */

public interface OnBrowserListener {

    void onError(Exception e);

}
