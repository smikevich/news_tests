package siarhei.mikevich.newstests.presentationmvvm.screen.details.mvvm;

import android.databinding.DataBindingUtil;

import siarhei.mikevich.newstests.presentationmvvm.R;
import siarhei.mikevich.newstests.presentationmvvm.databinding.ActivityDetailsMvvmBinding;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.activity.BaseActivity;
import siarhei.mikevich.newstests.presentationmvvm.views.loadingdialog.LoadingView;

/**
 * Created by Siarhei Mikevich on 12/1/17.
 */

public class DetailsActivity extends BaseActivity<DetailsViewModel, ActivityDetailsMvvmBinding> {

    @Override
    protected DetailsViewModel getViewModel(LoadingView loadingView) {
        return new DetailsViewModel(this, getIntent());
    }

    @Override
    protected ActivityDetailsMvvmBinding getViewDataBinding() {
        return DataBindingUtil.setContentView(this, R.layout.activity_details_mvvm);
    }

    @Override
    protected void setupViewDataBinding() {
        viewDataBinding.setModel(viewModel);
    }

    @Override
    protected int getActivityTitleId() {
        return R.string.title_details;
    }

}
