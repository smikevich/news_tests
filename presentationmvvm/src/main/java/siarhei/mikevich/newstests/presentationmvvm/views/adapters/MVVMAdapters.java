package siarhei.mikevich.newstests.presentationmvvm.views.adapters;

import android.databinding.BindingAdapter;
import android.support.annotation.ColorRes;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import siarhei.mikevich.newstests.presentationmvvm.screen.base.adapter.BaseAdapter;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.adapter.OnItemClickListener;
import siarhei.mikevich.newstests.presentationmvvm.screen.sources.OnSourceClickListener;
import siarhei.mikevich.newstests.presentationmvvm.screen.sources.SourcesAdapter;
import siarhei.mikevich.newstests.presentationmvvm.views.icon.IconView;
import siarhei.mikevich.newstests.presentationmvvm.views.icon.ImageViewFresco;

/**
 * Created by mikes on 30.11.2017.
 */

public class MVVMAdapters {

    @BindingAdapter("android:src_by_url")
    public static void loadImageByUrl(ImageViewFresco imageView, String url) {
        imageView.setImageURI(url);
    }

    @BindingAdapter("android:offset_changed_listener")
    public static void setOffsetChangedListener(AppBarLayout appBarLayout, AppBarLayout.OnOffsetChangedListener offsetChangedListener) {
        appBarLayout.addOnOffsetChangedListener(offsetChangedListener);
    }

    @BindingAdapter("android:animation")
    public static void startAnimationFloatingActionButton(FloatingActionButton floatingActionButton, boolean isHideFloatingActionButton) {
        int value = isHideFloatingActionButton ? 0 : 1;
        ViewCompat.animate(floatingActionButton).scaleY(value).scaleX(value).start();
    }

    @BindingAdapter(value = {"android:recycler_layout_manager", "android:recycler_adapter", "android:recycler_list", "android:recycler_listener", "android:recycler_about_listener"}, requireAll = false)
    public static void initRecycler(RecyclerView recyclerView, RecyclerView.LayoutManager recyclerLayoutManager, BaseAdapter recyclerAdapter, List<?> recyclerList, OnItemClickListener<?> listener, OnSourceClickListener aboutListener) {
        recyclerView.setLayoutManager(recyclerLayoutManager);
        recyclerAdapter.changeDataSet(recyclerList);
        if (recyclerAdapter instanceof SourcesAdapter && aboutListener != null) {
            ((SourcesAdapter) recyclerAdapter).setAboutListener(aboutListener);
        }
        recyclerAdapter.setListener(listener);
        recyclerView.setAdapter(recyclerAdapter);
    }

    @BindingAdapter({"android:refresh_color_scheme", "android:refresh_listener", "android:refresh_indicator"})
    public static void setupRefreshColorScheme(SwipeRefreshLayout swipeRefreshLayout, @ColorRes int colorRes, SwipeRefreshLayout.OnRefreshListener listener, boolean refreshing) {
        swipeRefreshLayout.setColorSchemeResources(colorRes);
        swipeRefreshLayout.setOnRefreshListener(listener);
        swipeRefreshLayout.setRefreshing(refreshing);
    }

    @BindingAdapter({"android:setup_icon_view"})
    public static void setupIconView(IconView iconView, String text) {
        iconView.setup(text);
    }

}
