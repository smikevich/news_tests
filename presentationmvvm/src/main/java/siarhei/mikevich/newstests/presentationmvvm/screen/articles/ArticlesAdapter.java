package siarhei.mikevich.newstests.presentationmvvm.screen.articles;

import android.databinding.ViewDataBinding;

import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.presentationmvvm.BR;
import siarhei.mikevich.newstests.presentationmvvm.R;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.adapter.BaseAdapter;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.adapter.BaseViewHolder;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.adapter.OnItemClickListener;

/**
 * Created by Siarhei Mikevich on 10/11/17.
 */

class ArticlesAdapter extends BaseAdapter<ArticlesAdapter.ViewHolder, Article, OnItemClickListener<Article>> {

    @Override
    protected int getLayoutId() {
        return R.layout.view_article_mvvm;
    }

    @Override
    protected ViewHolder getViewHolder(ViewDataBinding viewDataBinding) {
        return new ViewHolder(viewDataBinding);
    }

    @Override
    protected void setupView(ViewHolder viewHolder, Article item) {
        viewHolder.setupView(item);
    }

    class ViewHolder extends BaseViewHolder<Article> {

        ViewHolder(ViewDataBinding viewDataBinding) {
            super(viewDataBinding);
        }

        @Override
        protected void setupView(ViewDataBinding viewDataBinding, Article item) {
            viewDataBinding.setVariable(BR.article, item);
        }

    }

}
