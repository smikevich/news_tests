package siarhei.mikevich.newstests.presentationmvvm.screen.categories;

import android.databinding.ViewDataBinding;

import siarhei.mikevich.newstests.domain.model.content.Category;
import siarhei.mikevich.newstests.presentationmvvm.BR;
import siarhei.mikevich.newstests.presentationmvvm.R;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.adapter.BaseAdapter;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.adapter.BaseViewHolder;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.adapter.OnItemClickListener;

/**
 * Created by Siarhei Mikevich on 10/11/17.
 */

class CategoriesAdapter extends BaseAdapter<CategoriesAdapter.ViewHolder, Category, OnItemClickListener<Category>> {

    @Override
    protected int getLayoutId() {
        return R.layout.view_category_mvvm;
    }

    @Override
    protected ViewHolder getViewHolder(ViewDataBinding itemView) {
        return new ViewHolder(itemView);
    }

    @Override
    protected void setupView(ViewHolder viewHolder, Category item) {
        viewHolder.setupView(item);
    }

    class ViewHolder extends BaseViewHolder<Category> {

        ViewHolder(ViewDataBinding itemView) {
            super(itemView);
        }

        @Override
        protected void setupView(ViewDataBinding viewDataBinding, Category item) {
            viewDataBinding.setVariable(BR.category, item);
        }

    }

}
