package siarhei.mikevich.newstests.presentationmvvm.screen.details.databinding;

import java.io.Serializable;

import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.activity.BaseContract;

/**
 * Created by Siarhei Mikevich on 10/7/17.
 */

public interface DetailsContract {

    interface View extends BaseContract.View {

        void showArticle(Article article);

        void navigateBrowserByUrl(String url);

        void showAboutButton();

        void hideAboutButton();

    }

    interface Presenter extends BaseContract.Presenter {

        void loadArticleFromSerializable(Serializable serializable);

        void onClickReadMore();

        void onOffsetChanged(int totalScrollRange, int verticalOffset);

    }

}
