package siarhei.mikevich.newstests.presentationmvvm.screen.categories;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import rx.Observable;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Category;
import siarhei.mikevich.newstests.domain.usecase.CategoriesUseCase;
import siarhei.mikevich.newstests.presentationmvvm.R;
import siarhei.mikevich.newstests.presentationmvvm.screen.base.activity.BaseListViewModel;
import siarhei.mikevich.newstests.presentationmvvm.screen.sources.SourcesActivity;
import siarhei.mikevich.newstests.presentationmvvm.views.loadingdialog.LoadingView;

/**
 * Created by mikes on 02.12.2017.
 */

public class CategoriesViewModel extends BaseListViewModel<Category, CategoriesAdapter> {

    private final CategoriesUseCase categoriesUseCase;

    public CategoriesViewModel(Context context, Intent intent, CategoriesUseCase categoriesUseCase, LoadingView loadingView) {
        super(context, intent, loadingView);
        this.categoriesUseCase = categoriesUseCase;
    }

    @Override
    protected Observable<BaseItemsContent<Category>> getObservable() {
        return categoriesUseCase.execUseCase(new CategoriesUseCase.Params("categories.json"));
    }

    @Override
    protected CategoriesAdapter getAdapter() {
        return new CategoriesAdapter();
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return new GridLayoutManager(context, context.getResources().getInteger(R.integer.categories_columns_count));
    }

    @Override
    protected Class<? extends AppCompatActivity> getNextActivity() {
        return SourcesActivity.class;
    }

}
