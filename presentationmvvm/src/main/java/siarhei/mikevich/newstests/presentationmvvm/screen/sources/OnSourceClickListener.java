package siarhei.mikevich.newstests.presentationmvvm.screen.sources;

import siarhei.mikevich.newstests.domain.model.content.Source;

/**
 * Created by Siarhei Mikevich on 10/11/17.
 */

public interface OnSourceClickListener {

    void onClickSourceAbout(Source source);

}
