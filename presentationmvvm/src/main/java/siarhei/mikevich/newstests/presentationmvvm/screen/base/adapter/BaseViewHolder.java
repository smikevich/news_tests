package siarhei.mikevich.newstests.presentationmvvm.screen.base.adapter;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;

/**
 * Created by Siarhei Mikevich on 10/11/17.
 */

public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {

    protected ViewDataBinding viewDataBinding;

    protected abstract void setupView(ViewDataBinding viewDataBinding, T item);

    public BaseViewHolder(ViewDataBinding viewDataBinding) {
        super(viewDataBinding.getRoot());
        bindView(viewDataBinding);
    }

    public void setupView(T item) {
        setupView(viewDataBinding, item);
    }

    private void bindView(ViewDataBinding itemView) {
        viewDataBinding = itemView;
    }

}
