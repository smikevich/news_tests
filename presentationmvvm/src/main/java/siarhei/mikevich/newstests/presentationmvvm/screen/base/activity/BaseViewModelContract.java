package siarhei.mikevich.newstests.presentationmvvm.screen.base.activity;

/**
 * Created by Siarhei Mikevich on 12/1/17.
 */

public interface BaseViewModelContract {

    void init();

}
