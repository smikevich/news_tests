package siarhei.mikevich.newstests.presentationmvvm.screen.base.activity;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import siarhei.mikevich.newstests.presentationmvvm.R;
import siarhei.mikevich.newstests.presentationmvvm.views.loadingdialog.LoadingDialog;
import siarhei.mikevich.newstests.presentationmvvm.views.loadingdialog.LoadingView;

/**
 * Created by Siarhei Mikevich on 12/1/17.
 */

public abstract class BaseActivity<VM extends BaseViewModelContract, VDB extends ViewDataBinding> extends AppCompatActivity {

    private Toolbar toolbar;
    protected LoadingView loadingView;

    protected VM viewModel;
    protected VDB viewDataBinding;

    protected abstract VM getViewModel(LoadingView loadingView);

    protected abstract VDB getViewDataBinding();

    protected abstract void setupViewDataBinding();

    @StringRes
    protected abstract int getActivityTitleId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupViewModel();
        bindViews();
        setupViews();
        setTitle(getActivityTitleId());
    }

    // loading dialog

    protected void showLoadingView() {
        loadingView.showLoadingView();
    }

    protected void hideLoadingView() {
        loadingView.hideLoadingView();
    }

    // toast message information

    protected void showToastMessage(@StringRes int toastMessageId) {
        Toast.makeText(this, getString(toastMessageId, Toast.LENGTH_SHORT), Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // private methods

    private void setupViewModel() {
        viewModel = getViewModel(LoadingDialog.init(getSupportFragmentManager()));
        viewDataBinding = getViewDataBinding();
        setupViewDataBinding();
        viewModel.init();
    }

    private void bindViews() {
        toolbar = findViewById(R.id.toolbar);
        loadingView = LoadingDialog.init(getSupportFragmentManager());
    }

    private void setupViews() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

}
