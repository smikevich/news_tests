package siarhei.mikevich.newstests.data.repository;

import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import rx.observers.TestSubscriber;
import siarhei.mikevich.newstests.data.DataModule;
import siarhei.mikevich.newstests.data.api.ApiFactory;
import siarhei.mikevich.newstests.data.utils.api.TestNewsSourcesService;
import siarhei.mikevich.newstests.data.utils.config.DataConfigTest;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Source;
import siarhei.mikevich.newstests.domain.repository.SourcesRepository;

/**
 * Created by mikes on 28.11.2017.
 */

@RunWith(AndroidJUnit4.class)
public class SourcesNewsRepositoryTest {

    private SourcesRepository repository;

    @Before
    public void init() throws Exception {
        repository = RepositoryProvider.provideSourcesRepository();
        DataModule.get.init(new DataConfigTest());
    }

    @Test
    public void testGetSourcesSomethingWrong() throws Exception {
        ApiFactory.setNewsService(new TestNewsSourcesService(true));
        TestSubscriber<BaseItemsContent<Source>> testSubscriber = new TestSubscriber<>();
        repository.getSources(TestNewsSourcesService.validCategory).subscribe(testSubscriber);
        testSubscriber.assertError(Exception.class);
    }

    @Test
    public void testGetSourcesWrongCategory() throws Exception {
        ApiFactory.setNewsService(new TestNewsSourcesService(false));
        repository.getSources("any_wrong_category")
                .subscribe(itemsContent -> {
                    Assert.assertNotNull(itemsContent);
                    Assert.assertNotNull(itemsContent.getItems());
                    Assert.assertNotNull(itemsContent.getErrorMessage());
                    Assert.assertEquals(0, itemsContent.getItems().size());
                    Assert.assertEquals("", itemsContent.getErrorMessage());
                });
    }

    @Test
    public void testGetSourcesSuccess() throws Exception {
        ApiFactory.setNewsService(new TestNewsSourcesService(false));
        BaseItemsContent<Source> itemsContent = repository.getSources(TestNewsSourcesService.validCategory).toBlocking().first();
        Assert.assertNotNull(itemsContent);
        Assert.assertNotNull(itemsContent.getItems());
        Assert.assertNotNull(itemsContent.getErrorMessage());
        Assert.assertEquals(11, itemsContent.getItems().size());
        Assert.assertEquals("", itemsContent.getErrorMessage());
    }

    @Test
    public void testAnySourceItem() throws Exception {
        ApiFactory.setNewsService(new TestNewsSourcesService(false));
        BaseItemsContent<Source> itemsContent = repository.getSources(TestNewsSourcesService.validCategory).toBlocking().first();
        Assert.assertNotNull(itemsContent);
        Assert.assertNotNull(itemsContent.getItems());
        Assert.assertNotNull(itemsContent.getErrorMessage());
        Assert.assertEquals(11, itemsContent.getItems().size());
        Assert.assertEquals("", itemsContent.getErrorMessage());
        Source source = itemsContent.getItems().get(5);

        Assert.assertEquals("financial-times", source.getId());
        Assert.assertEquals("Financial Times", source.getName());
        Assert.assertEquals("The latest UK and international business, finance, economic and political news, comment and analysis from the Financial Times on FT.com.", source.getDescription());
        Assert.assertEquals("http://www.ft.com/home/uk", source.getUrl());
        Assert.assertEquals("business", source.getCategory());
    }

    @After
    public void finish() throws Exception {
        DataModule.get.init(null);
        repository = null;
    }

}
