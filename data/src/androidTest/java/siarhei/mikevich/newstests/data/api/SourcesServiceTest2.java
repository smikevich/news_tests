package siarhei.mikevich.newstests.data.api;

import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import retrofit2.Response;
import rx.observers.TestSubscriber;
import siarhei.mikevich.newstests.data.DataModule;
import siarhei.mikevich.newstests.data.api.interceptor.MockingInterceptor;
import siarhei.mikevich.newstests.data.api.utils.StatusCode;
import siarhei.mikevich.newstests.data.model.content.SourceEntity;
import siarhei.mikevich.newstests.data.model.response.SourcesResponse;
import siarhei.mikevich.newstests.data.utils.config.DataConfigTest;

/**
 * Created by mikes on 28.11.2017.
 */

@RunWith(AndroidJUnit4.class)
public class SourcesServiceTest2 {

    @Before
    public void init() throws Exception {
        DataModule.get.init(new DataConfigTest());
    }

    @Test
    public void testGetSourcesSomethingWrong() throws Exception {
        MockingInterceptor.isError = true;
        TestSubscriber<Response<SourcesResponse>> testSubscriber = new TestSubscriber<>();
        ApiFactory.getNewsService().getSources("any_valid_category").subscribe(testSubscriber);
        testSubscriber.assertNoErrors();
    }

    @Test
    public void testGetSourcesSomethingWrong2() throws Exception {
        MockingInterceptor.isError = true;
        SourcesResponse sourcesResponse = ApiFactory.getNewsService().getSources("any_valid_category").toBlocking().first().body();
        Assert.assertNull(sourcesResponse);
    }

    @Test
    public void testGetSourcesWrongCategory() throws Exception {
        ApiFactory.getNewsService()
                .getSources(MockingInterceptor.WRONG_CATEGORY)
                .map(Response::body)
                .subscribe(sourcesResponse -> {
                    Assert.assertEquals(StatusCode.OK, sourcesResponse.getStatus());
                    Assert.assertNotNull(sourcesResponse.getSourceEntities());
                    Assert.assertEquals(0, sourcesResponse.getSourceEntities().size());
                    Assert.assertEquals(null, sourcesResponse.getCode());
                    Assert.assertEquals(null, sourcesResponse.getMessage());
                });
    }

    @Test
    public void testGetSourcesSuccess() throws Exception {
        SourcesResponse sourcesResponse = ApiFactory.getNewsService().getSources("any_valid_category").toBlocking().first().body();
        Assert.assertNotNull(sourcesResponse);
        Assert.assertEquals(StatusCode.OK, sourcesResponse.getStatus());
        Assert.assertNotNull(sourcesResponse);
        Assert.assertEquals(11, sourcesResponse.getSourceEntities().size());
        Assert.assertEquals(null, sourcesResponse.getCode());
        Assert.assertEquals(null, sourcesResponse.getMessage());
    }

    @Test
    public void testAnySourceItem() throws Exception {
        SourcesResponse sourcesResponse = ApiFactory.getNewsService().getSources("any_valid_category").toBlocking().first().body();
        Assert.assertNotNull(sourcesResponse);
        SourceEntity sourceEntity = sourcesResponse.getSourceEntities().get(5);
        Assert.assertEquals("financial-times", sourceEntity.getId());
        Assert.assertEquals("Financial Times", sourceEntity.getName());
        Assert.assertEquals("The latest UK and international business, finance, economic and political news, comment and analysis from the Financial Times on FT.com.", sourceEntity.getDescription());
        Assert.assertEquals("http://www.ft.com/home/uk", sourceEntity.getUrl());
        Assert.assertEquals("business", sourceEntity.getCategory());
    }

    @After
    public void finish() throws Exception {
        DataModule.get.init(null);
        MockingInterceptor.isError = false;
    }

}
