package siarhei.mikevich.newstests.data.api;

import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import retrofit2.Response;
import rx.observers.TestSubscriber;
import siarhei.mikevich.newstests.data.DataModule;
import siarhei.mikevich.newstests.data.api.interceptor.MockingInterceptor;
import siarhei.mikevich.newstests.data.api.utils.Filter;
import siarhei.mikevich.newstests.data.api.utils.StatusCode;
import siarhei.mikevich.newstests.data.model.content.ArticleEntity;
import siarhei.mikevich.newstests.data.model.response.ArticlesResponse;
import siarhei.mikevich.newstests.data.utils.config.DataConfigTest;

/**
 * Created by mikes on 28.11.2017.
 */

@RunWith(AndroidJUnit4.class)
public class ArticlesServiceTest {

    @Before
    public void init() throws Exception {
        DataModule.get.init(new DataConfigTest());
    }

    @Test
    public void testGetArticlesSomethingWrong() throws Exception {
        MockingInterceptor.isError = true;
        TestSubscriber<Response<ArticlesResponse>> testSubscriber = new TestSubscriber<>();
        ApiFactory.getNewsService().getArticles("any_valid_source", Filter.TOP).subscribe(testSubscriber);
        testSubscriber.assertNoErrors();
    }

    @Test
    public void testGetArticlesSomethingWrong2() throws Exception {
        MockingInterceptor.isError = true;
        ArticlesResponse articlesResponse = ApiFactory.getNewsService().getArticles("any_valid_source", Filter.TOP).toBlocking().first().body();
        Assert.assertNull(articlesResponse);
    }

    @Test
    public void testGetArticlesWrongSource() throws Exception {
        ApiFactory.getNewsService()
                .getArticles(MockingInterceptor.WRONG_SOURCE, Filter.TOP)
                .map(Response::body)
                .subscribe(articlesResponse -> {
                    Assert.assertEquals(StatusCode.ERROR, articlesResponse.getStatus());
                    Assert.assertNull(articlesResponse.getArticleEntities());
                    Assert.assertEquals("sourceDoesntExist", articlesResponse.getCode());
                    Assert.assertEquals("The news source you've entered doesn't exist. Check your spelling, or see /v1/sources for a list of valid sources.", articlesResponse.getMessage());
                });
    }

    @Test
    public void testGetArticlesUnavailableSortedBy() throws Exception {
        ArticlesResponse articlesResponse = ApiFactory.getNewsService().getArticles("any_valid_source", Filter.POPULAR).toBlocking().first().body();
        Assert.assertNotNull(articlesResponse);
        Assert.assertEquals(StatusCode.ERROR, articlesResponse.getStatus());
        Assert.assertNull(articlesResponse.getArticleEntities());
        Assert.assertEquals("sourceUnavailableSortedBy", articlesResponse.getCode());
        Assert.assertEquals("The news source you've selected (bloomberg) isn't available sorted by popular", articlesResponse.getMessage());
    }

    @Test
    public void testGetArticlesSuccess() throws Exception {
        ArticlesResponse articlesResponse = ApiFactory.getNewsService().getArticles("any_valid_source", Filter.TOP).toBlocking().first().body();
        Assert.assertNotNull(articlesResponse);
        Assert.assertEquals(StatusCode.OK, articlesResponse.getStatus());
        Assert.assertNotNull(articlesResponse);
        Assert.assertEquals(10, articlesResponse.getArticleEntities().size());
        Assert.assertEquals(null, articlesResponse.getCode());
        Assert.assertEquals(null, articlesResponse.getMessage());
    }

    @Test
    public void testAnyArticleItem() throws Exception {
        ArticlesResponse articlesResponse = ApiFactory.getNewsService().getArticles("any_valid_source", Filter.TOP).toBlocking().first().body();
        Assert.assertNotNull(articlesResponse);
        Assert.assertNotNull(articlesResponse.getArticleEntities());
        ArticleEntity articleEntity = articlesResponse.getArticleEntities().get(5);
        Assert.assertNotNull(articleEntity);
        Assert.assertEquals("Here come Tesla earnings ...", articleEntity.getTitle());
        Assert.assertEquals("Akin Oyedele", articleEntity.getAuthor());
        Assert.assertEquals("Tesla is set to report third-quarter earnings...", articleEntity.getDescription());
        Assert.assertEquals("2017-11-01T19:52:39Z", articleEntity.getPublishedAt());
        Assert.assertEquals("http://www.businessinsider.com/tesla-earnings-q3-2017-11", articleEntity.getUrl());
        Assert.assertEquals("http://static2.businessinsider.com/image/59baca9d9803c578288b58d9-1190-625/here-come-tesla-earnings-.jpg", articleEntity.getUrlToImage());
    }

    @After
    public void finish() throws Exception {
        DataModule.get.init(null);
        MockingInterceptor.isError = false;
    }

}
