package siarhei.mikevich.newstests.data.utils.api;

import retrofit2.Response;
import rx.Observable;
import siarhei.mikevich.newstests.data.api.NewsService;
import siarhei.mikevich.newstests.data.model.response.ArticlesResponse;
import siarhei.mikevich.newstests.data.model.response.SourcesResponse;

/**
 * Created by mikes on 28.11.2017.
 */

public class TestNewsService implements NewsService {

    @Override
    public Observable<Response<ArticlesResponse>> getArticles(String source, String sortBy) {
        return Observable.empty();
    }

    @Override
    public Observable<Response<SourcesResponse>> getSources(String category) {
        return Observable.empty();
    }

}
