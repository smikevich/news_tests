package siarhei.mikevich.newstests.data.utils.api;

import retrofit2.Response;
import retrofit2.http.Query;
import rx.Observable;
import siarhei.mikevich.newstests.data.api.utils.Filter;
import siarhei.mikevich.newstests.data.model.response.ArticlesResponse;
import siarhei.mikevich.newstests.data.utils.transformer.responsetransformer.ResponseAssetsTransformer;

/**
 * Created by mikes on 28.11.2017.
 */

public class TestNewsArticlesService extends TestNewsService {

    private boolean isError;
    public final String wrongSource = "wrong_source";

    public TestNewsArticlesService(boolean isError) {
        this.isError = isError;
    }

    @Override
    public Observable<Response<ArticlesResponse>> getArticles(@Query("source") String source, @Query("sortBy") @Filter String sortBy) {
        if (isError) {
            return Observable.just("").compose(getResponse());
        } else if (wrongSource.equals(source)) {
            return Observable.just("articles_not_exists.json").compose(getResponse());
        } else if (Filter.POPULAR.equals(sortBy)) {
            return Observable.just("articles_unavailable_sorted_by.json").compose(getResponse());
        } else {
            return Observable.just("articles.json").compose(getResponse());
        }
    }

    private Observable.Transformer<String, Response<ArticlesResponse>> getResponse() {
        return observable -> observable
                .compose(new ResponseAssetsTransformer<>(ArticlesResponse.class))
                .map(Response::success);
    }

}
