package siarhei.mikevich.newstests.data.repository;

import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import rx.observers.TestSubscriber;
import siarhei.mikevich.newstests.data.DataModule;
import siarhei.mikevich.newstests.data.api.interceptor.MockingInterceptor;
import siarhei.mikevich.newstests.data.utils.config.DataConfigTest;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Source;
import siarhei.mikevich.newstests.domain.repository.SourcesRepository;

/**
 * Created by mikes on 28.11.2017.
 */

@RunWith(AndroidJUnit4.class)
public class SourcesNewsRepositoryTest2 {

    private SourcesRepository repository;

    @Before
    public void init() throws Exception {
        DataModule.get.init(new DataConfigTest());
        repository = RepositoryProvider.provideSourcesRepository();
    }

    @Test
    public void testGetSourcesSomethingWrong() throws Exception {
        MockingInterceptor.isError = true;
        TestSubscriber<BaseItemsContent<Source>> testSubscriber = new TestSubscriber<>();
        repository.getSources("any_valid_category").subscribe(testSubscriber);
        testSubscriber.assertError(Exception.class);
    }

    @Test
    public void testGetSourcesWrongCategory() throws Exception {
        repository.getSources(MockingInterceptor.WRONG_CATEGORY)
                .subscribe(itemsContent -> {
                    Assert.assertNotNull(itemsContent);
                    Assert.assertNotNull(itemsContent.getItems());
                    Assert.assertNotNull(itemsContent.getErrorMessage());
                    Assert.assertEquals(0, itemsContent.getItems().size());
                    Assert.assertEquals("", itemsContent.getErrorMessage());
                });
    }

    @Test
    public void testGetSourcesSuccess() throws Exception {
        BaseItemsContent<Source> itemsContent = repository.getSources("any_valid_category").toBlocking().first();
        Assert.assertNotNull(itemsContent);
        Assert.assertNotNull(itemsContent.getItems());
        Assert.assertNotNull(itemsContent.getErrorMessage());
        Assert.assertEquals(11, itemsContent.getItems().size());
        Assert.assertEquals("", itemsContent.getErrorMessage());
    }

    @Test
    public void testAnySourceItem() throws Exception {
        BaseItemsContent<Source> itemsContent = repository.getSources("any_valid_category").toBlocking().first();
        Assert.assertNotNull(itemsContent);
        Assert.assertNotNull(itemsContent.getItems());
        Assert.assertNotNull(itemsContent.getErrorMessage());
        Assert.assertEquals(11, itemsContent.getItems().size());
        Assert.assertEquals("", itemsContent.getErrorMessage());
        Source source = itemsContent.getItems().get(5);

        Assert.assertEquals("financial-times", source.getId());
        Assert.assertEquals("Financial Times", source.getName());
        Assert.assertEquals("The latest UK and international business, finance, economic and political news, comment and analysis from the Financial Times on FT.com.", source.getDescription());
        Assert.assertEquals("http://www.ft.com/home/uk", source.getUrl());
        Assert.assertEquals("business", source.getCategory());
    }

    @After
    public void finish() throws Exception {
        DataModule.get.init(null);
        MockingInterceptor.isError = false;
        repository = null;
    }

}
