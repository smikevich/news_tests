package siarhei.mikevich.newstests.data.repository;

import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import siarhei.mikevich.newstests.data.DataModule;
import siarhei.mikevich.newstests.data.utils.config.DataConfigTest;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Category;
import siarhei.mikevich.newstests.domain.repository.CategoriesRepository;

/**
 * Created by mikes on 28.11.2017.
 */

@RunWith(AndroidJUnit4.class)
public class CategoriesNewsRepositoryTest {

    private CategoriesRepository repository;

    @Before
    public void init() throws Exception {
        DataModule.get.init(new DataConfigTest());
        repository = RepositoryProvider.provideCategoriesRepository();
    }

    @Test
    public void testGetCategoriesWithWrongAssetPath() throws Exception {
        BaseItemsContent<Category> itemsContent = repository.getCategories("wrong_asset_path.json").toBlocking().first();
        Assert.assertNotNull(itemsContent);
        Assert.assertNotNull(itemsContent.getItems());
        Assert.assertNotNull(itemsContent.getErrorMessage());
        Assert.assertEquals(0, itemsContent.getItems().size());
        Assert.assertEquals("mocked error message", itemsContent.getErrorMessage());
    }

    @Test
    public void testGetCategoriesSuccess() throws Exception {
        BaseItemsContent<Category> itemsContent = repository.getCategories("categories.json").toBlocking().first();
        Assert.assertNotNull(itemsContent);
        Assert.assertNotNull(itemsContent.getItems());
        Assert.assertNotNull(itemsContent.getErrorMessage());
        Assert.assertEquals(10, itemsContent.getItems().size());
        Assert.assertEquals("", itemsContent.getErrorMessage());
    }

    @Test
    public void testAnyCategoryItem() throws Exception {
        BaseItemsContent<Category> itemsContent = repository.getCategories("categories.json").toBlocking().first();
        Assert.assertNotNull(itemsContent);
        Assert.assertNotNull(itemsContent.getItems());
        Assert.assertNotNull(itemsContent.getErrorMessage());
        Assert.assertEquals(10, itemsContent.getItems().size());
        Assert.assertEquals("", itemsContent.getErrorMessage());
        Category category = itemsContent.getItems().get(5);
        Assert.assertEquals("politics", category.getId());
        Assert.assertEquals("Politics", category.getName());
    }

    @After
    public void finish() throws Exception {
        DataModule.get.init(null);
        repository = null;
    }

}
