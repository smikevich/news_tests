package siarhei.mikevich.newstests.data.utils.api;

import retrofit2.Response;
import retrofit2.http.Query;
import rx.Observable;
import siarhei.mikevich.newstests.data.model.response.SourcesResponse;
import siarhei.mikevich.newstests.data.utils.transformer.responsetransformer.ResponseAssetsTransformer;

/**
 * Created by mikes on 28.11.2017.
 */

public class TestNewsSourcesService extends TestNewsService {

    private boolean isError;
    public static final String validCategory = "valid_category";

    public TestNewsSourcesService(boolean isError) {
        this.isError = isError;
    }

    @Override
    public Observable<Response<SourcesResponse>> getSources(@Query("category") String category) {
        if (isError) {
            return Observable.error(new Exception());
        } else if (validCategory.equals(category)) {
            return Observable.just("sources.json").compose(getResponse());
        } else {
            return Observable.just("sources_empty.json").compose(getResponse());
        }
    }

    private Observable.Transformer<String, Response<SourcesResponse>> getResponse() {
        return observable -> observable
                .compose(new ResponseAssetsTransformer<>(SourcesResponse.class))
                .map(Response::success);
    }

}
