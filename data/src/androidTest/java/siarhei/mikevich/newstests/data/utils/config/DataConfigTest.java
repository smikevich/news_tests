package siarhei.mikevich.newstests.data.utils.config;

import android.support.test.InstrumentationRegistry;

import java.io.IOException;
import java.io.InputStream;

import siarhei.mikevich.newstests.data.DataConfig;

/**
 * Created by Siarhei Mikevich on 11/28/17.
 */

public class DataConfigTest implements DataConfig {

    @Override
    public String getStringResourceByErrorCode(String errorCode) {
        return "mocked error message";
    }

    @Override
    public InputStream getJsonInputStreamFromAssets(String assetPath) throws IOException {
        return InstrumentationRegistry.getContext().getAssets().open(assetPath);
    }

}
