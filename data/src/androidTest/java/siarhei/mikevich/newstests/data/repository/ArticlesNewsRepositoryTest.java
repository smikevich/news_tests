package siarhei.mikevich.newstests.data.repository;

import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import rx.observers.TestSubscriber;
import siarhei.mikevich.newstests.data.DataModule;
import siarhei.mikevich.newstests.data.api.interceptor.MockingInterceptor;
import siarhei.mikevich.newstests.data.api.utils.Filter;
import siarhei.mikevich.newstests.data.utils.config.DataConfigTest;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.domain.repository.ArticlesRepository;

/**
 * Created by mikes on 28.11.2017.
 */

@RunWith(AndroidJUnit4.class)
public class ArticlesNewsRepositoryTest {

    private ArticlesRepository repository;

    @Before
    public void init() throws Exception {
        DataModule.get.init(new DataConfigTest());
        repository = RepositoryProvider.provideArticlesRepository();
    }

    @Test
    public void testGetArticlesSomethingWrong() throws Exception {
        MockingInterceptor.isError = true;
        TestSubscriber<BaseItemsContent<Article>> testSubscriber = new TestSubscriber<>();
        repository.getArticles("any_valid_source", Filter.TOP).subscribe(testSubscriber);
        testSubscriber.assertError(Exception.class);
    }

    @Test
    public void testGetArticlesWrongSource() throws Exception {
        repository.getArticles(MockingInterceptor.WRONG_SOURCE, Filter.TOP)
                .subscribe(itemsContent -> {
                    Assert.assertNotNull(itemsContent);
                    Assert.assertNotNull(itemsContent.getItems());
                    Assert.assertNotNull(itemsContent.getErrorMessage());
                    Assert.assertEquals(0, itemsContent.getItems().size());
                    Assert.assertEquals("The news source you've entered doesn't exist. Check your spelling, or see /v1/sources for a list of valid sources.", itemsContent.getErrorMessage());
                });
    }

    @Test
    public void testGetArticlesUnavailableSortedBy() throws Exception {
        BaseItemsContent<Article> itemsContent = repository.getArticles("any_valid_source", Filter.POPULAR).toBlocking().first();
        Assert.assertNotNull(itemsContent);
        Assert.assertNotNull(itemsContent.getItems());
        Assert.assertNotNull(itemsContent.getErrorMessage());
        Assert.assertEquals(0, itemsContent.getItems().size());
        Assert.assertEquals("The news source you've selected (bloomberg) isn't available sorted by popular", itemsContent.getErrorMessage());
    }

    @Test
    public void testGetArticlesSuccess() throws Exception {
        BaseItemsContent<Article> itemsContent = repository.getArticles("any_valid_source", Filter.TOP).toBlocking().first();
        Assert.assertNotNull(itemsContent);
        Assert.assertNotNull(itemsContent.getItems());
        Assert.assertNotNull(itemsContent.getErrorMessage());
        Assert.assertEquals(10, itemsContent.getItems().size());
        Assert.assertEquals("", itemsContent.getErrorMessage());
    }

    @Test
    public void testAnyArticleItem() throws Exception {
        BaseItemsContent<Article> itemsContent = repository.getArticles("any_valid_source", Filter.TOP).toBlocking().first();
        Assert.assertNotNull(itemsContent);
        Assert.assertNotNull(itemsContent.getItems());
        Assert.assertNotNull(itemsContent.getErrorMessage());
        Assert.assertEquals(10, itemsContent.getItems().size());
        Assert.assertEquals("", itemsContent.getErrorMessage());
        Article article = itemsContent.getItems().get(5);

        Assert.assertEquals("Akin Oyedele", article.getAuthor());
        Assert.assertEquals("Tesla is set to report third-quarter earnings...", article.getDescription());
        Assert.assertEquals("Here come Tesla earnings ...", article.getTitle());
        Assert.assertEquals("2017-11-01T19:52:39Z", article.getPublishedAt());
        Assert.assertEquals("http://www.businessinsider.com/tesla-earnings-q3-2017-11", article.getUrl());
        Assert.assertEquals("http://static2.businessinsider.com/image/59baca9d9803c578288b58d9-1190-625/here-come-tesla-earnings-.jpg", article.getUrlToImage());
    }

    @After
    public void finish() throws Exception {
        DataModule.get.init(null);
        MockingInterceptor.isError = false;
    }

}
