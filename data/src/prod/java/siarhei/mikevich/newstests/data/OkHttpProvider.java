package siarhei.mikevich.newstests.data;

import okhttp3.OkHttpClient;
import siarhei.mikevich.newstests.data.api.interceptor.ApiKeyInterceptor;
import siarhei.mikevich.newstests.data.api.interceptor.HttpLoggerInterceptor;

/**
 * Created by Siarhei Mikevich on 11/1/17.
 */

public class OkHttpProvider {

    private static volatile OkHttpClient client;

    public static void recreate() {
        client = null;
        client = provideClient();
    }

    public static OkHttpClient provideClient() {
        OkHttpClient okHttpClient = client;
        if (okHttpClient == null) {
            synchronized (OkHttpProvider.class) {
                okHttpClient = client;
                if (okHttpClient == null) {
                    okHttpClient = client = buildClient();
                }
            }
        }
        return okHttpClient;
    }

    private static OkHttpClient buildClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(ApiKeyInterceptor.create())
                .addInterceptor(HttpLoggerInterceptor.create())
                .build();
    }

}
