package siarhei.mikevich.newstests.data.utils.transformer.responsetransformer;

import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Response;
import rx.Observable;
import siarhei.mikevich.newstests.data.model.response.ArticlesResponse;
import siarhei.mikevich.newstests.data.model.response.MainResponse;
import siarhei.mikevich.newstests.data.model.response.SourcesResponse;

/**
 * Created by mikes on 26.11.2017.
 */

public class ResponseTransformer<T extends MainResponse> implements Observable.Transformer<Response<T>, T> {

    private Class<T> clazz;

    public ResponseTransformer(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public Observable<T> call(Observable<Response<T>> responseObservable) {
        return responseObservable.flatMap(this::responseProcessing);
    }

    private Observable<T> responseProcessing(Response<T> response) {
        T detailedResponse = response.body();
        if (response.isSuccessful() && detailedResponse != null) {
            return Observable.just(detailedResponse);
        } else {
            detailedResponse = generateBadResponse(response);
            return detailedResponse != null ? Observable.just(detailedResponse) : Observable.error(new Exception());
        }
    }

    private T generateBadResponse(Response<T> response) {
        try {
            String errorResponseBody = response.errorBody().string();
            MainResponse mainResponse = new Gson().fromJson(errorResponseBody, MainResponse.class);
            if (mainResponse != null) {
                if (clazz.isAssignableFrom(ArticlesResponse.class)) {
                    return clazz.cast(new ArticlesResponse(mainResponse));
                } else if (clazz.isAssignableFrom(SourcesResponse.class)) {
                    return clazz.cast(new SourcesResponse(mainResponse));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
