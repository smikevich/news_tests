package siarhei.mikevich.newstests.data.api.interceptor;

import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by Siarhei Mikevich on 10/7/17.
 */

public class HttpLoggerInterceptor implements Interceptor {

    private final Interceptor httpLoggerInterceptor;

    private HttpLoggerInterceptor() {
        httpLoggerInterceptor = new HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    public static Interceptor create() {
        return new HttpLoggerInterceptor();
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        return httpLoggerInterceptor.intercept(chain);
    }

}
