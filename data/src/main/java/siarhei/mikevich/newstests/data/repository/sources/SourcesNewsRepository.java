package siarhei.mikevich.newstests.data.repository.sources;

import rx.Observable;
import siarhei.mikevich.newstests.data.api.ApiFactory;
import siarhei.mikevich.newstests.data.model.response.SourcesResponse;
import siarhei.mikevich.newstests.data.utils.transformer.DataTransformer;
import siarhei.mikevich.newstests.data.utils.transformer.responsetransformer.ResponseTransformer;
import siarhei.mikevich.newstests.data.utils.transformer.ErrorTransformer;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Source;
import siarhei.mikevich.newstests.domain.repository.SourcesRepository;

/**
 * Created by Siarhei Mikevich on 11/21/17.
 */

public class SourcesNewsRepository implements SourcesRepository {

    @Override
    public Observable<BaseItemsContent<Source>> getSources(String category) {
        return ApiFactory.getNewsService()
                .getSources(category)
                .compose(new ResponseTransformer<>(SourcesResponse.class))
                .compose(new DataTransformer<>(Source.class))
                .compose(new ErrorTransformer<>());
    }

}
