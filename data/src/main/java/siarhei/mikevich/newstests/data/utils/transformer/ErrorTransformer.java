package siarhei.mikevich.newstests.data.utils.transformer;

import rx.Observable;

/**
 * Created by mikes on 06.11.17.
 */

public class ErrorTransformer<T> implements Observable.Transformer<T, T> {

    @Override
    public Observable<T> call(Observable<T> observable) {
        return observable
                .onErrorResumeNext(throwable -> {
                    throwable.printStackTrace();
                    return Observable.error(throwable);
                });
    }

}
