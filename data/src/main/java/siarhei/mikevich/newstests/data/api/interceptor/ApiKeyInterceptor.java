package siarhei.mikevich.newstests.data.api.interceptor;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import siarhei.mikevich.newstests.data.BuildConfig;

/**
 * Created by Siarhei Mikevich on 10/3/17.
 */

public class ApiKeyInterceptor implements Interceptor {

    private final String apiKey;

    private ApiKeyInterceptor() {
        apiKey = BuildConfig.API_KEY;
    }

    public static Interceptor create() {
        return new ApiKeyInterceptor();
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        if (TextUtils.isEmpty(apiKey)) {
            return chain.proceed(chain.request());
        }
        Request request = chain.request()
                .newBuilder()
                .addHeader("X-Api-Key", apiKey)
                .build();
        return chain.proceed(request);
    }

}
