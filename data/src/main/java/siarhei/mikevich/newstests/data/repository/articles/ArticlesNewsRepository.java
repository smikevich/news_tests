package siarhei.mikevich.newstests.data.repository.articles;

import rx.Observable;
import siarhei.mikevich.newstests.data.api.ApiFactory;
import siarhei.mikevich.newstests.data.api.utils.Filter;
import siarhei.mikevich.newstests.data.model.response.ArticlesResponse;
import siarhei.mikevich.newstests.data.utils.transformer.DataTransformer;
import siarhei.mikevich.newstests.data.utils.transformer.responsetransformer.ResponseTransformer;
import siarhei.mikevich.newstests.data.utils.transformer.ErrorTransformer;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.domain.repository.ArticlesRepository;

/**
 * Created by Siarhei Mikevich on 11/21/17.
 */

public class ArticlesNewsRepository implements ArticlesRepository {

    @Override
    public Observable<BaseItemsContent<Article>> getArticles(String source, @Filter String sortBy) {
        return ApiFactory.getNewsService()
                .getArticles(source, sortBy)
                .compose(new ResponseTransformer<>(ArticlesResponse.class))
                .compose(new DataTransformer<>(Article.class))
                .compose(new ErrorTransformer<>());
    }

}
