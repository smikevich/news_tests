package siarhei.mikevich.newstests.data.model.content;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mikes on 03.10.17.
 */

public class ArticleEntity {

    @SerializedName("author")
    private String author;

    @SerializedName("description")
    private String description;

    @SerializedName("title")
    private String title;

    @SerializedName("url")
    private String url;

    @SerializedName("urlToImage")
    private String urlToImage;

    @SerializedName("publishedAt")
    private String publishedAt;

    public String getAuthor() {
        return author;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getUrlToImage() {
        return urlToImage;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

}
