package siarhei.mikevich.newstests.data.model.content;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mikes on 03.10.17.
 */

public class SourceEntity {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("url")
    private String url;

    @SerializedName("category")
    private String category;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public String getCategory() {
        return category;
    }

}
