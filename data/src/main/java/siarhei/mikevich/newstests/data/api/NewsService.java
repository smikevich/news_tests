package siarhei.mikevich.newstests.data.api;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;
import siarhei.mikevich.newstests.data.api.utils.Filter;
import siarhei.mikevich.newstests.data.model.response.ArticlesResponse;
import siarhei.mikevich.newstests.data.model.response.SourcesResponse;

/**
 * Created by Siarhei Mikevich on 10/3/17.
 */

public interface NewsService {

    @GET("v1/articles")
    Observable<Response<ArticlesResponse>> getArticles(@Query("source") String source, @Query("sortBy") @Filter String sortBy);

    @GET("v1/sources")
    Observable<Response<SourcesResponse>> getSources(@Query("category") String category);

}
