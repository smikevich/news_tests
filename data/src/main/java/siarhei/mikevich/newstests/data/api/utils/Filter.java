package siarhei.mikevich.newstests.data.api.utils;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Siarhei Mikevich on 10/7/17.
 */

@StringDef({Filter.TOP, Filter.LATEST, Filter.POPULAR})
@Retention(RetentionPolicy.SOURCE)
public @interface Filter {
    String TOP = "top";
    String LATEST = "latest";
    String POPULAR = "popular";
}
