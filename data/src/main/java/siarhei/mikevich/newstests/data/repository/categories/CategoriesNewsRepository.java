package siarhei.mikevich.newstests.data.repository.categories;

import rx.Observable;
import siarhei.mikevich.newstests.data.model.response.CategoriesResponse;
import siarhei.mikevich.newstests.data.utils.transformer.DataTransformer;
import siarhei.mikevich.newstests.data.utils.transformer.responsetransformer.ResponseAssetsTransformer;
import siarhei.mikevich.newstests.data.utils.transformer.ErrorTransformer;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Category;
import siarhei.mikevich.newstests.domain.repository.CategoriesRepository;

/**
 * Created by Siarhei Mikevich on 11/21/17.
 */

public class CategoriesNewsRepository implements CategoriesRepository {

    @Override
    public Observable<BaseItemsContent<Category>> getCategories(String assetPath) {
        return Observable.just(assetPath)
                .compose(new ResponseAssetsTransformer<>(CategoriesResponse.class))
                .compose(new DataTransformer<>(Category.class))
                .compose(new ErrorTransformer<>());
    }

}
