package siarhei.mikevich.newstests.data.api.utils;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Siarhei Mikevich on 11/17/17.
 */

@StringDef({StatusCode.OK, StatusCode.ERROR})
@Retention(RetentionPolicy.SOURCE)
public @interface StatusCode {
    String OK = "ok";
    String ERROR = "error";
}
