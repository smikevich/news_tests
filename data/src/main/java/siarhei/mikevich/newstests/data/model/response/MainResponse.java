package siarhei.mikevich.newstests.data.model.response;

import com.google.gson.annotations.SerializedName;

import siarhei.mikevich.newstests.data.api.utils.StatusCode;

/**
 * Created by mikes on 03.10.17.
 */

public class MainResponse {

    @SerializedName("status")
    private @StatusCode String status;

    @SerializedName("code")
    private String code;

    @SerializedName("message")
    private String message;

    MainResponse(MainResponse response) {
        this.status = response.getStatus();
        this.code = response.getCode();
        this.message = response.getMessage();
    }

    public MainResponse(@StatusCode String status, String code, String message) {
        this.status = status;
        this.code = code;
        this.message = message;
    }

    public MainResponse(@StatusCode String status) {
        this.status = status;
    }

    public @StatusCode String getStatus() {
        return status;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
