package siarhei.mikevich.newstests.data.utils.transformer;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import siarhei.mikevich.newstests.data.api.utils.StatusCode;
import siarhei.mikevich.newstests.data.model.response.ArticlesResponse;
import siarhei.mikevich.newstests.data.model.response.CategoriesResponse;
import siarhei.mikevich.newstests.data.model.response.MainResponse;
import siarhei.mikevich.newstests.data.model.response.SourcesResponse;
import siarhei.mikevich.newstests.data.utils.mapper.EntityDataMapper;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;

/**
 * Created by mikes on 26.11.2017.
 */

public class DataTransformer<Response extends MainResponse, T> implements Observable.Transformer<Response, BaseItemsContent<T>> {

    private Class<T> clazz;

    public DataTransformer(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public Observable<BaseItemsContent<T>> call(Observable<Response> tObservable) {
        return tObservable
                .flatMap(response -> Observable.zip(getListObservable(response), getErrorMessageObservable(response), BaseItemsContent::new));
    }

    private Observable<String> getErrorMessageObservable(Response response) {
        return Observable.just(response.getStatus())
                .map(statusCode -> statusCode.equals(StatusCode.OK) ? "" : response.getMessage());
    }

    private Observable<List<T>> getListObservable(Response response) {
        return Observable.just(getList(response))
                .filter(list -> list != null && !list.isEmpty())
                .flatMap(Observable::from)
                .map(new EntityDataMapper<>(clazz))
                .toList();
    }

    private List<?> getList(Response response) {
        List<?> list = new ArrayList<>();
        if (response instanceof CategoriesResponse) {
            list = ((CategoriesResponse) response).getCategoryEntities();
        } else if (response instanceof SourcesResponse) {
            list = ((SourcesResponse) response).getSourceEntities();
        } else if (response instanceof ArticlesResponse) {
            list = ((ArticlesResponse) response).getArticleEntities();
        }
        return list;
    }

}
