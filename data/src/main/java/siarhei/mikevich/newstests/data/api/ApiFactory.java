package siarhei.mikevich.newstests.data.api;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import siarhei.mikevich.newstests.data.BuildConfig;
import siarhei.mikevich.newstests.data.OkHttpProvider;

/**
 * Created by Siarhei Mikevich on 10/3/17.
 */

public class ApiFactory {

    private static volatile NewsService service;

    private ApiFactory() {
    }

    public static void recreate() {
        OkHttpProvider.recreate();
        service = buildRetrofit().create(NewsService.class);
    }

    public static NewsService getNewsService() {
        NewsService newsService = service;
        if (newsService == null) {
            synchronized (ApiFactory.class) {
                newsService = service;
                if (newsService == null) {
                    newsService = service = buildRetrofit().create(NewsService.class);
                }
            }
        }
        return newsService;
    }

    public static void setNewsService(NewsService newsService) {
        service = newsService;
    }

    private static Retrofit buildRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_ENDPOINT)
                .client(OkHttpProvider.provideClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

}
