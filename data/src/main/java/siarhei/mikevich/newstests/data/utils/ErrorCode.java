package siarhei.mikevich.newstests.data.utils;

/**
 * Created by Siarhei Mikevich on 11/22/17.
 */

public interface ErrorCode {

    String ERROR_GETTING_RESPONSE_FROM_ASSETS = "error_getting_response_from_assets";

}
