package siarhei.mikevich.newstests.data.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import siarhei.mikevich.newstests.data.model.content.SourceEntity;

/**
 * Created by mikes on 03.10.17.
 */

public class SourcesResponse extends MainResponse {

    @SerializedName("sources")
    private List<SourceEntity> sourceEntities;

    public SourcesResponse(MainResponse response) {
        super(response);
    }

    public List<SourceEntity> getSourceEntities() {
        return sourceEntities;
    }

}
