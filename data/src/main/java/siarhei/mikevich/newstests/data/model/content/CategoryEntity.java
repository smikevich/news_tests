package siarhei.mikevich.newstests.data.model.content;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mikes on 05.10.17.
 */

public class CategoryEntity {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}
