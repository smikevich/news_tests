package siarhei.mikevich.newstests.data.api.interceptor;

import android.os.SystemClock;
import android.support.annotation.NonNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import siarhei.mikevich.newstests.data.DataModule;
import siarhei.mikevich.newstests.data.api.utils.Filter;

/**
 * Created by Siarhei Mikevich on 11/1/17.
 */

public class MockingInterceptor implements Interceptor {

    private final Map<Pattern, String> mapAssetResponsesPath;
    private MediaType applicationJsonMediaType = MediaType.parse("application,json");

    public static final String WRONG_CATEGORY = "wrong_category";
    public static final String WRONG_SOURCE = "wrong_source";
    public static boolean isError = false;

    public static MockingInterceptor create() {
        return new MockingInterceptor();
    }

    private MockingInterceptor() {
        String sourcesEncodedPath = "/v1/sources\\?";
        String articlesEncodedPath = "/v1/articles\\?";
        String categoryQueryParam = "category=";
        String sourceQuery = "source=";
        String sortByQuery = "sortBy=";
        mapAssetResponsesPath = new HashMap<>();
        mapAssetResponsesPath.put(Pattern.compile(sourcesEncodedPath + categoryQueryParam + WRONG_CATEGORY), "sources_empty.json");
        mapAssetResponsesPath.put(Pattern.compile(sourcesEncodedPath + categoryQueryParam + "(?!" + WRONG_CATEGORY + ").*$"), "sources.json");
        mapAssetResponsesPath.put(Pattern.compile(articlesEncodedPath + sourceQuery + WRONG_SOURCE + "&" + sortByQuery + "(" + Filter.POPULAR + "|" + Filter.TOP + "|" + Filter.LATEST + ")"), "articles_not_exists.json");
        mapAssetResponsesPath.put(Pattern.compile(articlesEncodedPath + sourceQuery + "(?!" + WRONG_SOURCE + ").*" + "&" + sortByQuery + Filter.POPULAR), "articles_unavailable_sorted_by.json");
        mapAssetResponsesPath.put(Pattern.compile(articlesEncodedPath + sourceQuery + "(?!" + WRONG_SOURCE + ").*" + "&" + sortByQuery + "(" + Filter.TOP + "|" + Filter.LATEST + ")"), "articles.json");
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        SystemClock.sleep(2000);
        Request request = chain.request();
        HttpUrl httpUrl = request.url();
        String url = httpUrl.encodedPath() + "?" + httpUrl.query();
        String responsePathAssets = getResponsePathAssetsByUrl(url);
        Response response = generateResponseFromAssets(request, responsePathAssets);
        return response == null ? chain.proceed(request) : response;
    }

    private String getResponsePathAssetsByUrl(String url) {
        String responsePathAssets = "";
        if (isError) {
            responsePathAssets = "";
        } else {
            for (Map.Entry<Pattern, String> entry : mapAssetResponsesPath.entrySet()) {
                if (entry.getKey().matcher(url).matches()) {
                    responsePathAssets = entry.getValue();
                }
            }
        }
        return responsePathAssets;
    }

    private Response generateResponseFromAssets(Request request, String assetPath) throws IOException {
        try {
            InputStream stream = DataModule.get.getDataConfig().getJsonInputStreamFromAssets(assetPath);
            Response response = generateResponse(request, 200, "OK", stream);
            stream.close();
            return response;
        } catch (IOException e) {
            return generateResponse(request, 500, e.getMessage(), null);
        }
    }

    private Response generateResponse(Request request, int code, String message, InputStream stream) throws IOException {
        ResponseBody responseBody;
        if (stream == null) {
            responseBody = ResponseBody.create(applicationJsonMediaType, "");
        } else {
            Buffer buffer = new Buffer().readFrom(stream);
            responseBody = ResponseBody.create(applicationJsonMediaType, buffer.size(), buffer);
        }
        return new Response.Builder()
                .request(request)
                .protocol(Protocol.HTTP_1_1)
                .code(code)
                .message(message)
                .body(responseBody)
                .build();
    }

}
