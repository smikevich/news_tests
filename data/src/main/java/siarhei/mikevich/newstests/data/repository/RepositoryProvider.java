package siarhei.mikevich.newstests.data.repository;

import siarhei.mikevich.newstests.data.repository.articles.ArticlesNewsRepository;
import siarhei.mikevich.newstests.data.repository.categories.CategoriesNewsRepository;
import siarhei.mikevich.newstests.data.repository.sources.SourcesNewsRepository;
import siarhei.mikevich.newstests.domain.repository.ArticlesRepository;
import siarhei.mikevich.newstests.domain.repository.CategoriesRepository;
import siarhei.mikevich.newstests.domain.repository.SourcesRepository;

/**
 * Created by mikes on 03.10.17.
 */

public class RepositoryProvider {

    private static CategoriesRepository categoriesRepository;
    private static SourcesRepository sourcesRepository;
    private static ArticlesRepository articlesRepository;

    public static CategoriesRepository provideCategoriesRepository() {
        if (categoriesRepository == null) {
            categoriesRepository = new CategoriesNewsRepository();
        }
        return categoriesRepository;
    }

    public static void setCategoriesRepository(CategoriesRepository repository) {
        categoriesRepository = repository;
    }

    public static SourcesRepository provideSourcesRepository() {
        if (sourcesRepository == null) {
            sourcesRepository = new SourcesNewsRepository();
        }
        return sourcesRepository;
    }

    public static void setSourcesRepository(SourcesRepository repository) {
        sourcesRepository = repository;
    }

    public static ArticlesRepository provideArticlesRepository() {
        if (articlesRepository == null) {
            articlesRepository = new ArticlesNewsRepository();
        }
        return articlesRepository;
    }

    public static void setArticlesRepository(ArticlesRepository repository) {
        articlesRepository = repository;
    }

    public static void init() {
        categoriesRepository = new CategoriesNewsRepository();
        sourcesRepository = new SourcesNewsRepository();
        articlesRepository = new ArticlesNewsRepository();
    }

}
