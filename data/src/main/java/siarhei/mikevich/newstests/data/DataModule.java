package siarhei.mikevich.newstests.data;

import siarhei.mikevich.newstests.data.api.ApiFactory;
import siarhei.mikevich.newstests.data.repository.RepositoryProvider;

/**
 * Created by Siarhei Mikevich on 11/22/17.
 */

public enum DataModule {

    get;

    private DataConfig dataConfig;

    DataModule() {

    }

    public void init(DataConfig config) {
        dataConfig = config;
        ApiFactory.recreate();
        RepositoryProvider.init();
    }

    public DataConfig getDataConfig() {
        return dataConfig;
    }

}
