package siarhei.mikevich.newstests.data.utils.transformer.responsetransformer;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import rx.Observable;
import siarhei.mikevich.newstests.data.DataModule;
import siarhei.mikevich.newstests.data.api.utils.StatusCode;
import siarhei.mikevich.newstests.data.model.response.ArticlesResponse;
import siarhei.mikevich.newstests.data.model.response.CategoriesResponse;
import siarhei.mikevich.newstests.data.model.response.MainResponse;
import siarhei.mikevich.newstests.data.model.response.SourcesResponse;
import siarhei.mikevich.newstests.data.utils.ErrorCode;

/**
 * Created by mikes on 26.11.2017.
 */

public class ResponseAssetsTransformer<T extends MainResponse> implements Observable.Transformer<String, T> {

    private Class<T> clazz;

    public ResponseAssetsTransformer(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public Observable<T> call(Observable<String> stringObservable) {
        return stringObservable.flatMap(this::getResponseFromAssets);
    }

    private Observable<T> getResponseFromAssets(String assetPath) {
        T assetResponse;
        try {
            assetResponse = generateSuccessResponse(assetPath);
        } catch(Exception e) {
            e.printStackTrace();
            assetResponse = generateBadResponse();
        }
        return Observable.just(assetResponse);
    }

    private T generateSuccessResponse(String assetPath) throws IOException {
        InputStream jsonInputStream = DataModule.get.getDataConfig().getJsonInputStreamFromAssets(assetPath);
        Scanner scanner = new Scanner(jsonInputStream).useDelimiter("\\A");
        String jsonString = scanner.hasNext() ? scanner.next() : "";
        return new Gson().fromJson(jsonString, clazz);
    }

    private T generateBadResponse() {
        String status = StatusCode.ERROR;
        String code = ErrorCode.ERROR_GETTING_RESPONSE_FROM_ASSETS;
        String message = DataModule.get.getDataConfig().getStringResourceByErrorCode(code);
        MainResponse mainResponse = new MainResponse(status, code, message);
        if (clazz.isAssignableFrom(ArticlesResponse.class)) {
            ArticlesResponse articlesResponse = new ArticlesResponse(mainResponse);
            return clazz.cast(articlesResponse);
        } else if (clazz.isAssignableFrom(SourcesResponse.class)) {
            SourcesResponse sourcesResponse = new SourcesResponse(mainResponse);
            return clazz.cast(sourcesResponse);
        } else if (clazz.isAssignableFrom(CategoriesResponse.class)) {
            CategoriesResponse categoriesResponse = new CategoriesResponse(mainResponse);
            return clazz.cast(categoriesResponse);
        } else {
            return null;
        }
    }

}
