package siarhei.mikevich.newstests.data.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import siarhei.mikevich.newstests.data.model.content.ArticleEntity;

/**
 * Created by mikes on 03.10.17.
 */

public class ArticlesResponse extends MainResponse {

    @SerializedName("source")
    private String source;

    @SerializedName("sortBy")
    private String sortBy;

    @SerializedName("articles")
    private List<ArticleEntity> articleEntities;

    public ArticlesResponse(MainResponse response) {
        super(response);
    }

    public String getSource() {
        return source;
    }

    public List<ArticleEntity> getArticleEntities() {
        return articleEntities;
    }

}
