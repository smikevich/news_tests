package siarhei.mikevich.newstests.data.utils.mapper;

import rx.functions.Func1;
import siarhei.mikevich.newstests.data.model.content.ArticleEntity;
import siarhei.mikevich.newstests.data.model.content.CategoryEntity;
import siarhei.mikevich.newstests.data.model.content.SourceEntity;
import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.domain.model.content.Category;
import siarhei.mikevich.newstests.domain.model.content.Source;

/**
 * Created by mikes on 26.11.2017.
 */

public class EntityDataMapper<T1, T2> implements Func1<T1, T2> {

    private Class<T2> clazz;

    public EntityDataMapper(Class<T2> clazz) {
        this.clazz = clazz;
    }

    @Override
    public T2 call(T1 t1) {
        return mapType(t1);
    }

    private T2 mapType(T1 entity) {
        if (entity instanceof CategoryEntity && clazz.isAssignableFrom(Category.class)) {
            return clazz.cast(mapCategory((CategoryEntity) entity));
        } else if (entity instanceof SourceEntity && clazz.isAssignableFrom(Source.class)) {
            return clazz.cast(mapSource((SourceEntity) entity));
        } else if (entity instanceof ArticleEntity && clazz.isAssignableFrom(Article.class)) {
            return clazz.cast(mapArticle((ArticleEntity) entity));
        }
        return null;
    }

    private Category mapCategory(CategoryEntity categoryEntity) {
        Category category = null;
        if (categoryEntity != null) {
            category = new Category();
            category.setId(categoryEntity.getId());
            category.setName(categoryEntity.getName());
        }
        return category;
    }

    private Source mapSource(SourceEntity sourceEntity) {
        Source source = null;
        if (sourceEntity != null) {
            source = new Source();
            source.setId(sourceEntity.getId());
            source.setName(sourceEntity.getName());
            source.setCategory(sourceEntity.getCategory());
            source.setDescription(sourceEntity.getDescription());
            source.setUrl(sourceEntity.getUrl());
        }
        return source;
    }

    private Article mapArticle(ArticleEntity articleEntity) {
        Article article = null;
        if (articleEntity != null) {
            article = new Article();
            article.setAuthor(articleEntity.getAuthor());
            article.setDescription(articleEntity.getDescription());
            article.setTitle(articleEntity.getTitle());
            article.setUrl(articleEntity.getUrl());
            article.setUrlToImage(articleEntity.getUrlToImage());
            article.setPublishedAt(articleEntity.getPublishedAt());
        }
        return article;
    }

}
