package siarhei.mikevich.newstests.data;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Siarhei Mikevich on 11/22/17.
 */

public interface DataConfig {

    String getStringResourceByErrorCode(String errorCode);

    InputStream getJsonInputStreamFromAssets(String assetPath) throws IOException;

}
