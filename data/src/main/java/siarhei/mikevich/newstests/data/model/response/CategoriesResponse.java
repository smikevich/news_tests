package siarhei.mikevich.newstests.data.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import siarhei.mikevich.newstests.data.model.content.CategoryEntity;

/**
 * Created by Siarhei Mikevich on 10/13/17.
 */

public class CategoriesResponse extends MainResponse {

    @SerializedName("categories")
    private List<CategoryEntity> categoryEntities;

    public CategoriesResponse(MainResponse response) {
        super(response);
    }

    public List<CategoryEntity> getCategoryEntities() {
        return categoryEntities;
    }

}
