package siarhei.mikevich.newstests.presentation.screen.categories;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

import siarhei.mikevich.newstests.domain.model.content.Category;
import siarhei.mikevich.newstests.domain.usecase.CategoriesUseCase;
import siarhei.mikevich.newstests.presentation.screen.base.activity.list.BaseListContract;

/**
 * Created by Siarhei Mikevich on 11/27/17.
 */

@RunWith(JUnit4.class)
public class CategoriesPresenterTest {

    private BaseListContract.View<Category> view;
    private CategoriesUseCase useCase;

    @Before
    public void init() throws Exception {
        view = new BaseListContract.View<Category>() {
            @Override
            public void showData(List<Category> list) {

            }

            @Override
            public void showErrorMessage(String errorMessage) {

            }

            @Override
            public void showErrorMessage(int errorMessageId) {

            }

            @Override
            public void setVisibilityContent(boolean isVisible) {

            }

            @Override
            public void navigateNext(Category item) {

            }

            @Override
            public void init() {

            }

            @Override
            public void showToastMessage(int messageId) {

            }

            @Override
            public void showLoadingView() {

            }

            @Override
            public void hideLoadingView() {

            }
        };
        useCase = new CategoriesUseCase(null, null);
    }

    @Test
    public void testNotNull() throws Exception {
        CategoriesPresenter presenter = new CategoriesPresenter(view, useCase);
        Assert.assertNotNull(presenter);
    }

}
