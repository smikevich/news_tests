package siarhei.mikevich.newstests.presentation.screen.details;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.domain.model.content.Source;
import siarhei.mikevich.newstests.presentation.R;

/**
 * Created by mikes on 27.11.2017.
 */

@RunWith(JUnit4.class)
public class DetailsPresenterTest {

    private DetailsContract.View view;
    private DetailsPresenter presenter;

    @Before
    public void setup() throws Exception {
        view = Mockito.mock(DetailsContract.View.class);
        presenter = new DetailsPresenter(view);
    }

    @Test
    public void testViewInit() throws Exception {
        Assert.assertTrue(view != null);
    }

    @Test
    public void testPresenterInit() throws Exception {
        Assert.assertTrue(presenter != null);
    }

    @Test
    public void testScreenCreated() throws Exception {
        presenter.onCreate();
        Mockito.verify(view).init();
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testValidLoadExtra() throws  Exception {
        Article article = new Article();
        presenter.loadArticleFromSerializable(article);
        Mockito.verify(view).showArticle(article);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testNullLoadExtra() throws  Exception {
        presenter.loadArticleFromSerializable(null);
        Mockito.verify(view).showToastMessage(R.string.error);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testNonCategoryLoadExtra() throws  Exception {
        presenter.loadArticleFromSerializable(new Source());
        Mockito.verify(view).showToastMessage(R.string.error);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testClickReadMoreWithNonNullArticle() throws Exception {
        Article article = new Article();
        presenter.loadArticleFromSerializable(article);
        Mockito.verify(view).showArticle(article);
        Mockito.verifyNoMoreInteractions(view);
        presenter.onClickReadMore();
        Mockito.verify(view).navigateBrowserByUrl(article.getUrl());
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testClickReadMoreWithNullArticle() throws Exception {
        presenter.loadArticleFromSerializable(null);
        Mockito.verify(view, Mockito.times(1)).showToastMessage(R.string.error);
        presenter.onClickReadMore();
        Mockito.verify(view, Mockito.times(2)).showToastMessage(R.string.error);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testOnOffsetChanged() throws Exception {
        presenter.onOffsetChanged(772, 0);
        Mockito.verify(view, Mockito.times(1)).showAboutButton();
        presenter.onOffsetChanged(772, 160);
        Mockito.verify(view, Mockito.times(1)).hideAboutButton();
        presenter.onOffsetChanged(772, 150);
        Mockito.verify(view, Mockito.times(2)).showAboutButton();
        presenter.onOffsetChanged(772, 200);
        Mockito.verify(view, Mockito.times(2)).hideAboutButton();
        presenter.onOffsetChanged(772, 190);
        Mockito.verifyNoMoreInteractions(view);
        presenter.onOffsetChanged(772, 195);
        Mockito.verifyNoMoreInteractions(view);
        presenter.onOffsetChanged(772, 140);
        Mockito.verify(view, Mockito.times(3)).showAboutButton();
        presenter.onOffsetChanged(772, 135);
        Mockito.verifyNoMoreInteractions(view);
        presenter.onOffsetChanged(772, 140);
        Mockito.verifyNoMoreInteractions(view);
    }

    @After
    public void finish() throws Exception {
        view = null;
        presenter = null;
    }

}
