package siarhei.mikevich.newstests.presentation.help.repository.articles;

import rx.Observable;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Article;

/**
 * Created by Siarhei Mikevich on 11/27/17.
 */

public class ArticlesNewsRepositoryMockTest extends ArticlesNewsRepositoryTest {

    private boolean isError = true;
    private BaseItemsContent<Article> itemsContent;

    public ArticlesNewsRepositoryMockTest() {

    }

    public ArticlesNewsRepositoryMockTest(boolean isError, BaseItemsContent<Article> itemsContent) {
        this.isError = isError;
        this.itemsContent = itemsContent;
    }

    @Override
    public Observable<BaseItemsContent<Article>> getArticles(String source, String sortBy) {
        if (isError) {
            return Observable.error(new Exception());
        }
        return Observable.just(itemsContent);
    }

}
