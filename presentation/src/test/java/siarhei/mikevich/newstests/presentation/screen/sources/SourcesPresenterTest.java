package siarhei.mikevich.newstests.presentation.screen.sources;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import siarhei.mikevich.newstests.data.repository.RepositoryProvider;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Category;
import siarhei.mikevich.newstests.domain.model.content.Source;
import siarhei.mikevich.newstests.domain.usecase.SourcesUseCase;
import siarhei.mikevich.newstests.presentation.R;
import siarhei.mikevich.newstests.presentation.help.repository.sources.SourcesNewsRepositoryMockTest;
import siarhei.mikevich.newstests.presentation.help.rules.RxTestRule;
import siarhei.mikevich.newstests.presentation.utils.transformer.AsyncTransformer;

/**
 * Created by Siarhei Mikevich on 11/27/17.
 */

@RunWith(JUnit4.class)
public class SourcesPresenterTest {

    private SourcesContract.View view;
    private SourcesPresenter presenter;

    @Rule
    public RxTestRule rule = new RxTestRule();

    @Before
    public void setup() throws Exception {
        view = Mockito.mock(SourcesContract.View.class);
    }

    @Test
    public void testViewInit() throws Exception {
        Assert.assertNotNull(view);
    }

    @Test
    public void testScreenCreated() throws Exception {
        RepositoryProvider.setSourcesRepository(new SourcesNewsRepositoryMockTest());
        presenter = new SourcesPresenter(view, new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()));

        presenter.onCreate();
        Mockito.verify(view).init();
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testClicked() throws Exception {
        RepositoryProvider.setSourcesRepository(new SourcesNewsRepositoryMockTest());
        presenter = new SourcesPresenter(view, new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()));

        Source source = new Source();
        presenter.onClickItem(source);
        Mockito.verify(view).navigateNext(source);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsSomethingWrong() throws Exception {
        RepositoryProvider.setSourcesRepository(new SourcesNewsRepositoryMockTest());
        presenter = new SourcesPresenter(view, new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Category());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showToastMessage(R.string.error);
        Mockito.verify(view).showErrorMessage(R.string.error_list_empty);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsStatusSuccessWithEmptyList() throws Exception {
        BaseItemsContent<Source> itemsContent = new BaseItemsContent<>(null, null);
        RepositoryProvider.setSourcesRepository(new SourcesNewsRepositoryMockTest(false, itemsContent));
        presenter = new SourcesPresenter(view, new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Category());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showErrorMessage(R.string.error_list_empty);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsStatusSuccessWithEmptyList2() throws Exception {
        BaseItemsContent<Source> itemsContent = new BaseItemsContent<>(new ArrayList<>(), null);
        RepositoryProvider.setSourcesRepository(new SourcesNewsRepositoryMockTest(false, itemsContent));
        presenter = new SourcesPresenter(view, new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Category());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showErrorMessage(R.string.error_list_empty);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsStatusSuccessWithEmptyList3() throws Exception {
        BaseItemsContent<Source> itemsContent = new BaseItemsContent<>(null, "");
        RepositoryProvider.setSourcesRepository(new SourcesNewsRepositoryMockTest(false, itemsContent));
        presenter = new SourcesPresenter(view, new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Category());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showErrorMessage(R.string.error_list_empty);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsStatusSuccessWithEmptyList4() throws Exception {
        BaseItemsContent<Source> itemsContent = new BaseItemsContent<>(new ArrayList<>(), "");
        RepositoryProvider.setSourcesRepository(new SourcesNewsRepositoryMockTest(false, itemsContent));
        presenter = new SourcesPresenter(view, new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Category());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showErrorMessage(R.string.error_list_empty);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsStatusSuccessWithFilledList() throws Exception {
        BaseItemsContent<Source> itemsContent = new BaseItemsContent<>(getSources(5), "");
        RepositoryProvider.setSourcesRepository(new SourcesNewsRepositoryMockTest(false, itemsContent));
        presenter = new SourcesPresenter(view, new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Category());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(itemsContent.getItems());
        Mockito.verify(view).setVisibilityContent(true);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsStatusSuccessWithFilledList2() throws Exception {
        String errorMessage = "some error message";
        BaseItemsContent<Source> itemsContent = new BaseItemsContent<>(getSources(5), errorMessage);
        RepositoryProvider.setSourcesRepository(new SourcesNewsRepositoryMockTest(false, itemsContent));
        presenter = new SourcesPresenter(view, new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Category());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(itemsContent.getItems());
        Mockito.verify(view).setVisibilityContent(true);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsWithErrorMessage() throws Exception {
        String errorMessage = "some error message";
        BaseItemsContent<Source> itemsContent = new BaseItemsContent<>(getSources(0), errorMessage);
        RepositoryProvider.setSourcesRepository(new SourcesNewsRepositoryMockTest(false, itemsContent));
        presenter = new SourcesPresenter(view, new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Category());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showToastMessage(R.string.error);
        Mockito.verify(view).showErrorMessage(itemsContent.getErrorMessage());
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsWithErrorMessage2() throws Exception {
        String errorMessage = "some error message";
        BaseItemsContent<Source> itemsContent = new BaseItemsContent<>(new ArrayList<>(), errorMessage);
        RepositoryProvider.setSourcesRepository(new SourcesNewsRepositoryMockTest(false, itemsContent));
        presenter = new SourcesPresenter(view, new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Category());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showToastMessage(R.string.error);
        Mockito.verify(view).showErrorMessage(itemsContent.getErrorMessage());
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsWithErrorMessage3() throws Exception {
        String errorMessage = "some error message";
        BaseItemsContent<Source> itemsContent = new BaseItemsContent<>(null, errorMessage);
        RepositoryProvider.setSourcesRepository(new SourcesNewsRepositoryMockTest(false, itemsContent));
        presenter = new SourcesPresenter(view, new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Category());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showToastMessage(R.string.error);
        Mockito.verify(view).showErrorMessage(itemsContent.getErrorMessage());
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testValidLoadExtra() throws  Exception {
        RepositoryProvider.setSourcesRepository(new SourcesNewsRepositoryMockTest());
        presenter = new SourcesPresenter(view, new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Category());
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testNullLoadExtra() throws  Exception {
        RepositoryProvider.setSourcesRepository(new SourcesNewsRepositoryMockTest());
        presenter = new SourcesPresenter(view, new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(null);
        Mockito.verify(view).showToastMessage(R.string.error);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testNonCategoryLoadExtra() throws  Exception {
        RepositoryProvider.setSourcesRepository(new SourcesNewsRepositoryMockTest());
        presenter = new SourcesPresenter(view, new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Source());
        Mockito.verify(view).showToastMessage(R.string.error);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testClickSourceAbout() throws  Exception {
        RepositoryProvider.setSourcesRepository(new SourcesNewsRepositoryMockTest());
        presenter = new SourcesPresenter(view, new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()));

        Source source = new Source();
        source.setUrl("some_url");
        presenter.onClickSourceAbout(source);
        Mockito.verify(view).navigateBrowserByUrl(source.getUrl());
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testScenario() throws Exception {
        Assert.assertNotNull(view);

        RepositoryProvider.setSourcesRepository(new SourcesNewsRepositoryMockTest());
        presenter = new SourcesPresenter(view, new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()));

        presenter.onCreate();
        Mockito.verify(view, Mockito.times(1)).init();
        Mockito.verifyNoMoreInteractions(view);

        Source source = new Source();
        presenter.onClickItem(source);
        Mockito.verify(view, Mockito.times(1)).navigateNext(source);
        Mockito.verifyNoMoreInteractions(view);

        RepositoryProvider.setSourcesRepository(new SourcesNewsRepositoryMockTest());
        presenter = new SourcesPresenter(view, new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Category());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view, Mockito.times(1)).showLoadingView();
        Mockito.verify(view, Mockito.times(1)).hideLoadingView();
        Mockito.verify(view, Mockito.times(1)).showData(new ArrayList<>());
        Mockito.verify(view, Mockito.times(1)).setVisibilityContent(false);
        Mockito.verify(view, Mockito.times(1)).showToastMessage(R.string.error);
        Mockito.verify(view, Mockito.times(1)).showErrorMessage(R.string.error_list_empty);
        Mockito.verifyNoMoreInteractions(view);

        BaseItemsContent<Source> itemsContent = new BaseItemsContent<>(null, "");
        RepositoryProvider.setSourcesRepository(new SourcesNewsRepositoryMockTest(false, itemsContent));
        presenter = new SourcesPresenter(view, new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Category());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view, Mockito.times(2)).showLoadingView();
        Mockito.verify(view, Mockito.times(2)).hideLoadingView();
        Mockito.verify(view, Mockito.times(2)).showData(new ArrayList<>());
        Mockito.verify(view, Mockito.times(2)).setVisibilityContent(false);
        Mockito.verify(view, Mockito.times(2)).showErrorMessage(R.string.error_list_empty);
        Mockito.verifyNoMoreInteractions(view);

        itemsContent = new BaseItemsContent<>(getSources(5), "");
        RepositoryProvider.setSourcesRepository(new SourcesNewsRepositoryMockTest(false, itemsContent));
        presenter = new SourcesPresenter(view, new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Category());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view, Mockito.times(3)).showLoadingView();
        Mockito.verify(view, Mockito.times(3)).hideLoadingView();
        Mockito.verify(view, Mockito.times(1)).showData(itemsContent.getItems());
        Mockito.verify(view, Mockito.times(1)).setVisibilityContent(true);
        Mockito.verifyNoMoreInteractions(view);

        String errorMessage = "some error message";
        itemsContent = new BaseItemsContent<>(null, errorMessage);
        RepositoryProvider.setSourcesRepository(new SourcesNewsRepositoryMockTest(false, itemsContent));
        presenter = new SourcesPresenter(view, new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Category());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view, Mockito.times(4)).showLoadingView();
        Mockito.verify(view, Mockito.times(4)).hideLoadingView();
        Mockito.verify(view, Mockito.times(3)).showData(new ArrayList<>());
        Mockito.verify(view, Mockito.times(3)).setVisibilityContent(false);
        Mockito.verify(view, Mockito.times(2)).showToastMessage(R.string.error);
        Mockito.verify(view, Mockito.times(1)).showErrorMessage(itemsContent.getErrorMessage());
        Mockito.verifyNoMoreInteractions(view);

        presenter.loadExtraData(new Category());
        Mockito.verifyNoMoreInteractions(view);

        presenter.loadExtraData(null);
        Mockito.verify(view, Mockito.times(3)).showToastMessage(R.string.error);
        Mockito.verifyNoMoreInteractions(view);

        presenter.loadExtraData(new Source());
        Mockito.verify(view, Mockito.times(4)).showToastMessage(R.string.error);
        Mockito.verifyNoMoreInteractions(view);

        source = new Source();
        source.setUrl("some_url");
        presenter.onClickSourceAbout(source);
        Mockito.verify(view, Mockito.times(1)).navigateBrowserByUrl(source.getUrl());
        Mockito.verifyNoMoreInteractions(view);
    }

    @After
    public void finish() throws Exception {
        view = null;
    }

    private List<Source> getSources(int size) {
        List<Source> sources = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            sources.add(new Source());
        }
        return sources;
    }

}
