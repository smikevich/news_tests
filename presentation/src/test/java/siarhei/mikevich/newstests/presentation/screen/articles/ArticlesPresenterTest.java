package siarhei.mikevich.newstests.presentation.screen.articles;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import siarhei.mikevich.newstests.data.repository.RepositoryProvider;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.domain.model.content.Category;
import siarhei.mikevich.newstests.domain.model.content.Source;
import siarhei.mikevich.newstests.domain.usecase.ArticlesUseCase;
import siarhei.mikevich.newstests.presentation.R;
import siarhei.mikevich.newstests.presentation.help.repository.articles.ArticlesNewsRepositoryMockTest;
import siarhei.mikevich.newstests.presentation.help.rules.RxTestRule;
import siarhei.mikevich.newstests.presentation.screen.base.activity.list.BaseListContract;
import siarhei.mikevich.newstests.presentation.utils.transformer.AsyncTransformer;

/**
 * Created by Siarhei Mikevich on 11/27/17.
 */

@RunWith(JUnit4.class)
public class ArticlesPresenterTest {

    private BaseListContract.View<Article> view;
    private ArticlesPresenter presenter;

    @Rule
    public RxTestRule rule = new RxTestRule();

    @Before
    public void setup() throws Exception {
        view = Mockito.mock(BaseListContract.View.class);
    }

    @Test
    public void testViewInit() throws Exception {
        Assert.assertTrue(view != null);
    }

    @Test
    public void testScreenCreated() throws Exception {
        RepositoryProvider.setArticlesRepository(new ArticlesNewsRepositoryMockTest());
        presenter = new ArticlesPresenter(view, new ArticlesUseCase(RepositoryProvider.provideArticlesRepository(), new AsyncTransformer<>()));

        presenter.onCreate();
        Mockito.verify(view).init();
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testClicked() throws Exception {
        RepositoryProvider.setArticlesRepository(new ArticlesNewsRepositoryMockTest());
        presenter = new ArticlesPresenter(view, new ArticlesUseCase(RepositoryProvider.provideArticlesRepository(), new AsyncTransformer<>()));

        Article article = new Article();
        presenter.onClickItem(article);
        Mockito.verify(view).navigateNext(article);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsSomethingWrong() throws Exception {
        RepositoryProvider.setArticlesRepository(new ArticlesNewsRepositoryMockTest());
        presenter = new ArticlesPresenter(view, new ArticlesUseCase(RepositoryProvider.provideArticlesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Source());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showToastMessage(R.string.error);
        Mockito.verify(view).showErrorMessage(R.string.error_list_empty);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsStatusSuccessWithEmptyList() throws Exception {
        BaseItemsContent<Article> itemsContent = new BaseItemsContent<>(null, null);
        RepositoryProvider.setArticlesRepository(new ArticlesNewsRepositoryMockTest(false, itemsContent));
        presenter = new ArticlesPresenter(view, new ArticlesUseCase(RepositoryProvider.provideArticlesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Source());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showErrorMessage(R.string.error_list_empty);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsStatusSuccessWithEmptyList2() throws Exception {
        BaseItemsContent<Article> itemsContent = new BaseItemsContent<>(new ArrayList<>(), null);
        RepositoryProvider.setArticlesRepository(new ArticlesNewsRepositoryMockTest(false, itemsContent));
        presenter = new ArticlesPresenter(view, new ArticlesUseCase(RepositoryProvider.provideArticlesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Source());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showErrorMessage(R.string.error_list_empty);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsStatusSuccessWithEmptyList3() throws Exception {
        BaseItemsContent<Article> itemsContent = new BaseItemsContent<>(null, "");
        RepositoryProvider.setArticlesRepository(new ArticlesNewsRepositoryMockTest(false, itemsContent));
        presenter = new ArticlesPresenter(view, new ArticlesUseCase(RepositoryProvider.provideArticlesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Source());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showErrorMessage(R.string.error_list_empty);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsStatusSuccessWithEmptyList4() throws Exception {
        BaseItemsContent<Article> itemsContent = new BaseItemsContent<>(new ArrayList<>(), "");
        RepositoryProvider.setArticlesRepository(new ArticlesNewsRepositoryMockTest(false, itemsContent));
        presenter = new ArticlesPresenter(view, new ArticlesUseCase(RepositoryProvider.provideArticlesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Source());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showErrorMessage(R.string.error_list_empty);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsStatusSuccessWithFilledList() throws Exception {
        BaseItemsContent<Article> itemsContent = new BaseItemsContent<>(getArticles(5), "");
        RepositoryProvider.setArticlesRepository(new ArticlesNewsRepositoryMockTest(false, itemsContent));
        presenter = new ArticlesPresenter(view, new ArticlesUseCase(RepositoryProvider.provideArticlesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Source());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(itemsContent.getItems());
        Mockito.verify(view).setVisibilityContent(true);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsStatusSuccessWithFilledList2() throws Exception {
        String errorMessage = "some error message";
        BaseItemsContent<Article> itemsContent = new BaseItemsContent<>(getArticles(5), errorMessage);
        RepositoryProvider.setArticlesRepository(new ArticlesNewsRepositoryMockTest(false, itemsContent));
        presenter = new ArticlesPresenter(view, new ArticlesUseCase(RepositoryProvider.provideArticlesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Source());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(itemsContent.getItems());
        Mockito.verify(view).setVisibilityContent(true);
        Mockito.verifyNoMoreInteractions(view);
    }


    @Test
    public void testLoadItemsWithErrorMessage() throws Exception {
        String errorMessage = "some error message";
        BaseItemsContent<Article> itemsContent = new BaseItemsContent<>(getArticles(0), errorMessage);
        RepositoryProvider.setArticlesRepository(new ArticlesNewsRepositoryMockTest(false, itemsContent));
        presenter = new ArticlesPresenter(view, new ArticlesUseCase(RepositoryProvider.provideArticlesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Source());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showToastMessage(R.string.error);
        Mockito.verify(view).showErrorMessage(itemsContent.getErrorMessage());
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsWithErrorMessage2() throws Exception {
        String errorMessage = "some error message";
        BaseItemsContent<Article> itemsContent = new BaseItemsContent<>(new ArrayList<>(), errorMessage);
        RepositoryProvider.setArticlesRepository(new ArticlesNewsRepositoryMockTest(false, itemsContent));
        presenter = new ArticlesPresenter(view, new ArticlesUseCase(RepositoryProvider.provideArticlesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Source());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showToastMessage(R.string.error);
        Mockito.verify(view).showErrorMessage(itemsContent.getErrorMessage());
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsWithErrorMessage3() throws Exception {
        String errorMessage = "some error message";
        BaseItemsContent<Article> itemsContent = new BaseItemsContent<>(null, errorMessage);
        RepositoryProvider.setArticlesRepository(new ArticlesNewsRepositoryMockTest(false, itemsContent));
        presenter = new ArticlesPresenter(view, new ArticlesUseCase(RepositoryProvider.provideArticlesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Source());
        Mockito.verifyNoMoreInteractions(view);
        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showToastMessage(R.string.error);
        Mockito.verify(view).showErrorMessage(itemsContent.getErrorMessage());
        Mockito.verifyNoMoreInteractions(view);
    }


    @Test
    public void testValidLoadExtra() throws  Exception {
        BaseItemsContent<Article> itemsContent = new BaseItemsContent<>(null, null);
        RepositoryProvider.setArticlesRepository(new ArticlesNewsRepositoryMockTest(false, itemsContent));
        presenter = new ArticlesPresenter(view, new ArticlesUseCase(RepositoryProvider.provideArticlesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Source());
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testNullLoadExtra() throws  Exception {
        BaseItemsContent<Article> itemsContent = new BaseItemsContent<>(null, null);
        RepositoryProvider.setArticlesRepository(new ArticlesNewsRepositoryMockTest(false, itemsContent));
        presenter = new ArticlesPresenter(view, new ArticlesUseCase(RepositoryProvider.provideArticlesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(null);
        Mockito.verify(view).showToastMessage(R.string.error);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testNonCategoryLoadExtra() throws  Exception {
        BaseItemsContent<Article> itemsContent = new BaseItemsContent<>(null, null);
        RepositoryProvider.setArticlesRepository(new ArticlesNewsRepositoryMockTest(false, itemsContent));
        presenter = new ArticlesPresenter(view, new ArticlesUseCase(RepositoryProvider.provideArticlesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Category());
        Mockito.verify(view).showToastMessage(R.string.error);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testFilterArticles() throws Exception {
        BaseItemsContent<Article> itemsContent = new BaseItemsContent<>(null, null);
        RepositoryProvider.setArticlesRepository(new ArticlesNewsRepositoryMockTest(false, itemsContent));
        presenter = new ArticlesPresenter(view, new ArticlesUseCase(RepositoryProvider.provideArticlesRepository(), new AsyncTransformer<>()));

        presenter.loadExtraData(new Source());
        Mockito.verifyNoMoreInteractions(view);
        presenter.filterArticles("");
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(itemsContent.getItems());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showErrorMessage(R.string.error_list_empty);
        Mockito.verifyNoMoreInteractions(view);
    }

    @After
    public void finish() throws Exception {
        view = null;
    }

    private List<Article> getArticles(int size) {
        List<Article> articles = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            articles.add(new Article());
        }
        return articles;
    }

}
