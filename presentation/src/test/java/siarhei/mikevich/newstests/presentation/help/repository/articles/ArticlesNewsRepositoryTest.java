package siarhei.mikevich.newstests.presentation.help.repository.articles;

import rx.Observable;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.domain.repository.ArticlesRepository;

/**
 * Created by Siarhei Mikevich on 11/27/17.
 */

public class ArticlesNewsRepositoryTest implements ArticlesRepository {

    @Override
    public Observable<BaseItemsContent<Article>> getArticles(String source, String sortBy) {
        return Observable.empty();
    }

}
