package siarhei.mikevich.newstests.presentation.help.view;

import junit.framework.Assert;

import java.util.List;

import siarhei.mikevich.newstests.domain.model.content.Category;
import siarhei.mikevich.newstests.presentation.screen.base.activity.list.BaseListContract;

/**
 * Created by Siarhei Mikevich on 11/27/17.
 */

public class CategoriesTestView implements BaseListContract.View<Category> {

    private boolean init;

    public CategoriesTestView(boolean init) {
        this.init = init;
    }

    @Override
    public void showLoadingView() {

    }

    @Override
    public void hideLoadingView() {

    }

    @Override
    public void init() {
        Assert.assertEquals(true, init);
    }

    @Override
    public void showToastMessage(int messageId) {

    }

    @Override
    public void showData(List<Category> list) {

    }

    @Override
    public void showErrorMessage(String errorMessage) {

    }

    @Override
    public void showErrorMessage(int errorMessageId) {

    }

    @Override
    public void setVisibilityContent(boolean isVisible) {

    }

    @Override
    public void navigateNext(Category item) {

    }

}
