package siarhei.mikevich.newstests.presentation.screen.categories;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import siarhei.mikevich.newstests.domain.usecase.CategoriesUseCase;
import siarhei.mikevich.newstests.presentation.help.view.CategoriesTestView;

/**
 * Created by Siarhei Mikevich on 11/27/17.
 */

@RunWith(JUnit4.class)
public class CategoriesPresenterTest2 {

    private CategoriesTestView view;
    private CategoriesPresenter presenter;

    @Before
    public void init() throws Exception {
        view = new CategoriesTestView(true);
        presenter = new CategoriesPresenter(view, new CategoriesUseCase(null, null));
    }

    @Test
    public void testNotNullView() throws Exception {
        Assert.assertNotNull(view);
    }

    @Test
    public void testNotNullPresenter() throws Exception {
        Assert.assertNotNull(presenter);
    }

}
