package siarhei.mikevich.newstests.presentation.help.repository.sources;

import rx.Observable;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Source;
import siarhei.mikevich.newstests.domain.repository.SourcesRepository;

/**
 * Created by Siarhei Mikevich on 11/27/17.
 */

public class SourcesNewsRepositoryTest implements SourcesRepository {

    @Override
    public Observable<BaseItemsContent<Source>> getSources(String category) {
        return Observable.empty();
    }

}
