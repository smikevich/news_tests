package siarhei.mikevich.newstests.presentation.help.repository.categories;

import rx.Observable;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Category;

/**
 * Created by Siarhei Mikevich on 11/27/17.
 */

public class CategoriesNewsRepositoryMockTest extends CategoriesNewsRepositoryTest {

    private boolean isError = true;
    private BaseItemsContent<Category> itemsContent;

    public CategoriesNewsRepositoryMockTest() {

    }

    public CategoriesNewsRepositoryMockTest(boolean isError, BaseItemsContent<Category> itemsContent) {
        this.isError = isError;
        this.itemsContent = itemsContent;
    }

    @Override
    public Observable<BaseItemsContent<Category>> getCategories(String assetPath) {
        if (isError) {
            return Observable.error(new Exception());
        }
        return Observable.just(itemsContent);
    }

}
