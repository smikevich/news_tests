package siarhei.mikevich.newstests.presentation.screen.categories;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import siarhei.mikevich.newstests.data.repository.RepositoryProvider;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Category;
import siarhei.mikevich.newstests.domain.usecase.CategoriesUseCase;
import siarhei.mikevich.newstests.presentation.R;
import siarhei.mikevich.newstests.presentation.help.repository.categories.CategoriesNewsRepositoryMockTest;
import siarhei.mikevich.newstests.presentation.help.rules.RxTestRule;
import siarhei.mikevich.newstests.presentation.screen.base.activity.list.BaseListContract;
import siarhei.mikevich.newstests.presentation.utils.transformer.AsyncTransformer;

/**
 * Created by Siarhei Mikevich on 11/27/17.
 */

@RunWith(JUnit4.class)
public class CategoriesPresenterTest3 {

    private BaseListContract.View<Category> view;
    private CategoriesPresenter presenter;

    @Rule
    public RxTestRule rule = new RxTestRule();

    @Before
    public void init() throws Exception {
        view = Mockito.mock(BaseListContract.View.class);
    }

    @Test
    public void testViewInit() throws Exception {
        Assert.assertNotNull(view);
    }

    @Test
    public void testScreenCreated() throws Exception {
        RepositoryProvider.setCategoriesRepository(new CategoriesNewsRepositoryMockTest());
        presenter = new CategoriesPresenter(view, new CategoriesUseCase(RepositoryProvider.provideCategoriesRepository(), new AsyncTransformer<>()));

        presenter.onCreate();
        Mockito.verify(view).init();
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testClicked() throws Exception {
        RepositoryProvider.setCategoriesRepository(new CategoriesNewsRepositoryMockTest());
        presenter = new CategoriesPresenter(view, new CategoriesUseCase(RepositoryProvider.provideCategoriesRepository(), new AsyncTransformer<>()));
        Category category = new Category();
        presenter.onClickItem(category);
        Mockito.verify(view).navigateNext(category);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsSomethingWrong() throws Exception {
        RepositoryProvider.setCategoriesRepository(new CategoriesNewsRepositoryMockTest());
        presenter = new CategoriesPresenter(view, new CategoriesUseCase(RepositoryProvider.provideCategoriesRepository(), new AsyncTransformer<>()));

        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showToastMessage(R.string.error);
        Mockito.verify(view).showErrorMessage(R.string.error_list_empty);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsStatusSuccessWithEmptyList() throws Exception {
        BaseItemsContent<Category> itemsContent = new BaseItemsContent<>(null, null);
        RepositoryProvider.setCategoriesRepository(new CategoriesNewsRepositoryMockTest(false, itemsContent));
        presenter = new CategoriesPresenter(view, new CategoriesUseCase(RepositoryProvider.provideCategoriesRepository(), new AsyncTransformer<>()));

        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showErrorMessage(R.string.error_list_empty);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsStatusSuccessWithEmptyList2() throws Exception {
        BaseItemsContent<Category> itemsContent = new BaseItemsContent<>(new ArrayList<>(), null);
        RepositoryProvider.setCategoriesRepository(new CategoriesNewsRepositoryMockTest(false, itemsContent));
        presenter = new CategoriesPresenter(view, new CategoriesUseCase(RepositoryProvider.provideCategoriesRepository(), new AsyncTransformer<>()));

        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showErrorMessage(R.string.error_list_empty);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsStatusSuccessWithEmptyList3() throws Exception {
        BaseItemsContent<Category> itemsContent = new BaseItemsContent<>(null, "");
        RepositoryProvider.setCategoriesRepository(new CategoriesNewsRepositoryMockTest(false, itemsContent));
        presenter = new CategoriesPresenter(view, new CategoriesUseCase(RepositoryProvider.provideCategoriesRepository(), new AsyncTransformer<>()));

        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showErrorMessage(R.string.error_list_empty);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsStatusSuccessWithEmptyList4() throws Exception {
        BaseItemsContent<Category> itemsContent = new BaseItemsContent<>(new ArrayList<>(), "");
        RepositoryProvider.setCategoriesRepository(new CategoriesNewsRepositoryMockTest(false, itemsContent));
        presenter = new CategoriesPresenter(view, new CategoriesUseCase(RepositoryProvider.provideCategoriesRepository(), new AsyncTransformer<>()));

        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showErrorMessage(R.string.error_list_empty);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsStatusSuccessWithFilledList() throws Exception {
        BaseItemsContent<Category> itemsContent = new BaseItemsContent<>(getCategories(5), "");
        RepositoryProvider.setCategoriesRepository(new CategoriesNewsRepositoryMockTest(false, itemsContent));
        presenter = new CategoriesPresenter(view, new CategoriesUseCase(RepositoryProvider.provideCategoriesRepository(), new AsyncTransformer<>()));

        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(itemsContent.getItems());
        Mockito.verify(view).setVisibilityContent(true);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsStatusSuccessWithFilledList2() throws Exception {
        String errorMessage = "some error message";
        BaseItemsContent<Category> itemsContent = new BaseItemsContent<>(getCategories(5), errorMessage);
        RepositoryProvider.setCategoriesRepository(new CategoriesNewsRepositoryMockTest(false, itemsContent));
        presenter = new CategoriesPresenter(view, new CategoriesUseCase(RepositoryProvider.provideCategoriesRepository(), new AsyncTransformer<>()));

        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(itemsContent.getItems());
        Mockito.verify(view).setVisibilityContent(true);
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsWithErrorMessage() throws Exception {
        String errorMessage = "some error message";
        BaseItemsContent<Category> itemsContent = new BaseItemsContent<>(getCategories(0), errorMessage);
        RepositoryProvider.setCategoriesRepository(new CategoriesNewsRepositoryMockTest(false, itemsContent));
        presenter = new CategoriesPresenter(view, new CategoriesUseCase(RepositoryProvider.provideCategoriesRepository(), new AsyncTransformer<>()));

        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showToastMessage(R.string.error);
        Mockito.verify(view).showErrorMessage(itemsContent.getErrorMessage());
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsWithErrorMessage2() throws Exception {
        String errorMessage = "some error message";
        BaseItemsContent<Category> itemsContent = new BaseItemsContent<>(new ArrayList<>(), errorMessage);
        RepositoryProvider.setCategoriesRepository(new CategoriesNewsRepositoryMockTest(false, itemsContent));
        presenter = new CategoriesPresenter(view, new CategoriesUseCase(RepositoryProvider.provideCategoriesRepository(), new AsyncTransformer<>()));

        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showToastMessage(R.string.error);
        Mockito.verify(view).showErrorMessage(itemsContent.getErrorMessage());
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testLoadItemsWithErrorMessage3() throws Exception {
        String errorMessage = "some error message";
        BaseItemsContent<Category> itemsContent = new BaseItemsContent<>(null, errorMessage);
        RepositoryProvider.setCategoriesRepository(new CategoriesNewsRepositoryMockTest(false, itemsContent));
        presenter = new CategoriesPresenter(view, new CategoriesUseCase(RepositoryProvider.provideCategoriesRepository(), new AsyncTransformer<>()));

        presenter.loadItems();
        Mockito.verify(view).showLoadingView();
        Mockito.verify(view).hideLoadingView();
        Mockito.verify(view).showData(new ArrayList<>());
        Mockito.verify(view).setVisibilityContent(false);
        Mockito.verify(view).showToastMessage(R.string.error);
        Mockito.verify(view).showErrorMessage(itemsContent.getErrorMessage());
        Mockito.verifyNoMoreInteractions(view);
    }

    @Test
    public void testScenario() throws Exception {
        Assert.assertNotNull(view);

        RepositoryProvider.setCategoriesRepository(new CategoriesNewsRepositoryMockTest());
        presenter = new CategoriesPresenter(view, new CategoriesUseCase(RepositoryProvider.provideCategoriesRepository(), new AsyncTransformer<>()));

        presenter.onCreate();
        Mockito.verify(view, Mockito.times(1)).init();
        Mockito.verifyNoMoreInteractions(view);

        Category category = new Category();
        presenter.onClickItem(category);
        Mockito.verify(view, Mockito.times(1)).navigateNext(category);
        Mockito.verifyNoMoreInteractions(view);

        presenter.loadItems();
        Mockito.verify(view, Mockito.times(1)).showLoadingView();
        Mockito.verify(view, Mockito.times(1)).hideLoadingView();
        Mockito.verify(view, Mockito.times(1)).showData(new ArrayList<>());
        Mockito.verify(view, Mockito.times(1)).setVisibilityContent(false);
        Mockito.verify(view, Mockito.times(1)).showToastMessage(R.string.error);
        Mockito.verify(view, Mockito.times(1)).showErrorMessage(R.string.error_list_empty);
        Mockito.verifyNoMoreInteractions(view);

        BaseItemsContent<Category> itemsContent = new BaseItemsContent<>(new ArrayList<>(), null);
        RepositoryProvider.setCategoriesRepository(new CategoriesNewsRepositoryMockTest(false, itemsContent));
        presenter = new CategoriesPresenter(view, new CategoriesUseCase(RepositoryProvider.provideCategoriesRepository(), new AsyncTransformer<>()));

        presenter.loadItems();
        Mockito.verify(view, Mockito.times(2)).showLoadingView();
        Mockito.verify(view, Mockito.times(2)).hideLoadingView();
        Mockito.verify(view, Mockito.times(2)).showData(itemsContent.getItems());
        Mockito.verify(view, Mockito.times(2)).setVisibilityContent(false);
        Mockito.verify(view, Mockito.times(2)).showErrorMessage(R.string.error_list_empty);
        Mockito.verifyNoMoreInteractions(view);

        itemsContent = new BaseItemsContent<>(getCategories(10), null);
        RepositoryProvider.setCategoriesRepository(new CategoriesNewsRepositoryMockTest(false, itemsContent));
        presenter = new CategoriesPresenter(view, new CategoriesUseCase(RepositoryProvider.provideCategoriesRepository(), new AsyncTransformer<>()));

        presenter.loadItems();
        Mockito.verify(view, Mockito.times(3)).showLoadingView();
        Mockito.verify(view, Mockito.times(3)).hideLoadingView();
        Mockito.verify(view, Mockito.times(1)).showData(itemsContent.getItems());
        Mockito.verify(view, Mockito.times(1)).setVisibilityContent(true);
        Mockito.verifyNoMoreInteractions(view);

        String errorMessage = "some error message";
        itemsContent = new BaseItemsContent<>(null, errorMessage);
        RepositoryProvider.setCategoriesRepository(new CategoriesNewsRepositoryMockTest(false, itemsContent));
        presenter = new CategoriesPresenter(view, new CategoriesUseCase(RepositoryProvider.provideCategoriesRepository(), new AsyncTransformer<>()));

        presenter.loadItems();
        Mockito.verify(view, Mockito.times(4)).showLoadingView();
        Mockito.verify(view, Mockito.times(4)).hideLoadingView();
        Mockito.verify(view, Mockito.times(3)).showData(new ArrayList<>());
        Mockito.verify(view, Mockito.times(3)).setVisibilityContent(false);
        Mockito.verify(view, Mockito.times(2)).showToastMessage(R.string.error);
        Mockito.verify(view, Mockito.times(1)).showErrorMessage(errorMessage);
        Mockito.verifyNoMoreInteractions(view);
    }

    @After
    public void finish() throws Exception {
        view = null;
    }

    private List<Category> getCategories(int size) {
        List<Category> categories = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            categories.add(new Category());
        }
        return categories;
    }

}
