package siarhei.mikevich.newstests.presentation.help.repository.sources;

import rx.Observable;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Source;

/**
 * Created by Siarhei Mikevich on 11/27/17.
 */

public class SourcesNewsRepositoryMockTest extends SourcesNewsRepositoryTest {

    private boolean isError = true;
    private BaseItemsContent<Source> itemsContent;

    public SourcesNewsRepositoryMockTest() {

    }

    public SourcesNewsRepositoryMockTest(boolean isError, BaseItemsContent<Source> itemsContent) {
        this.isError = isError;
        this.itemsContent = itemsContent;
    }

    @Override
    public Observable<BaseItemsContent<Source>> getSources(String category) {
        if (isError) {
            return Observable.error(new Exception());
        }
        return Observable.just(itemsContent);
    }

}
