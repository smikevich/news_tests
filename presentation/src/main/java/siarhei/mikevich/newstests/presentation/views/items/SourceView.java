package siarhei.mikevich.newstests.presentation.views.items;

import android.content.Context;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.widget.TextView;

import siarhei.mikevich.newstests.domain.model.content.Source;
import siarhei.mikevich.newstests.presentation.R;
import siarhei.mikevich.newstests.presentation.views.icon.IconView;

/**
 * Created by mikes on 06.10.17.
 */

public class SourceView extends LinearLayoutCompat {

    private IconView sourceIconView;
    private TextView sourceNameTextView;
    private TextView sourceDescriptionTextView;

    public SourceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public SourceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SourceView(Context context) {
        super(context);
        init();
    }

    public void setup(Source source) {
        sourceIconView.setup(source.getName());
        sourceNameTextView.setText(source.getName());
        sourceDescriptionTextView.setText(source.getDescription());
    }

    private void init() {
        inflate(getContext(), R.layout.view_source, this);
        bindViews();
    }

    private void bindViews() {
        sourceIconView = findViewById(R.id.source_icon_view);
        sourceNameTextView = findViewById(R.id.source_name_text_view);
        sourceDescriptionTextView = findViewById(R.id.source_description_text_view);
    }

}
