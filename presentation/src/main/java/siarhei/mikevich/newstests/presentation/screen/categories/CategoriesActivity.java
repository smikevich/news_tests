package siarhei.mikevich.newstests.presentation.screen.categories;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import siarhei.mikevich.newstests.data.repository.RepositoryProvider;
import siarhei.mikevich.newstests.domain.model.content.Category;
import siarhei.mikevich.newstests.domain.usecase.CategoriesUseCase;
import siarhei.mikevich.newstests.presentation.R;
import siarhei.mikevich.newstests.presentation.screen.base.activity.list.BaseListActivity;
import siarhei.mikevich.newstests.presentation.screen.base.activity.list.BaseListContract;
import siarhei.mikevich.newstests.presentation.screen.base.adapter.OnItemClickListener;
import siarhei.mikevich.newstests.presentation.screen.sources.SourcesActivity;
import siarhei.mikevich.newstests.presentation.utils.transformer.AsyncTransformer;

/**
 * Created by mikes on 13.10.17.
 */

public class CategoriesActivity
        extends BaseListActivity<Category, CategoriesPresenter, CategoriesAdapter>
        implements BaseListContract.View<Category>, OnItemClickListener<Category> {

    @Override
    protected int getActivityTitleId() {
        return R.string.title_categories;
    }

    @Override
    protected CategoriesPresenter getPresenter() {
        CategoriesUseCase categoriesUseCase = new CategoriesUseCase(RepositoryProvider.provideCategoriesRepository(), new AsyncTransformer<>());
        return new CategoriesPresenter(this, categoriesUseCase);
    }

    @Override
    protected CategoriesAdapter getAdapter() {
        return new CategoriesAdapter(this);
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return new GridLayoutManager(this, getResources().getInteger(R.integer.categories_columns_count));
    }

    @Override
    protected Class<? extends AppCompatActivity> getNextActivity() {
        return SourcesActivity.class;
    }

}
