package siarhei.mikevich.newstests.presentation.screen.articles;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.io.Serializable;

import siarhei.mikevich.newstests.data.api.utils.Filter;
import siarhei.mikevich.newstests.data.repository.RepositoryProvider;
import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.domain.usecase.ArticlesUseCase;
import siarhei.mikevich.newstests.presentation.R;
import siarhei.mikevich.newstests.presentation.screen.base.activity.list.BaseListActivity;
import siarhei.mikevich.newstests.presentation.screen.base.activity.list.BaseListContract;
import siarhei.mikevich.newstests.presentation.screen.base.adapter.OnItemClickListener;
import siarhei.mikevich.newstests.presentation.screen.details.DetailsActivity;
import siarhei.mikevich.newstests.presentation.utils.ExtraKeys;
import siarhei.mikevich.newstests.presentation.utils.transformer.AsyncTransformer;

/**
 * Created by Siarhei Mikevich on 10/13/17.
 */

public class ArticlesActivity
        extends BaseListActivity<Article, ArticlesPresenter, ArticlesAdapter>
        implements BaseListContract.View<Article>, OnItemClickListener<Article> {

    @Override
    protected int getActivityTitleId() {
        return R.string.title_articles;
    }

    @Override
    protected ArticlesPresenter getPresenter() {
        ArticlesUseCase articlesUseCase = new ArticlesUseCase(RepositoryProvider.provideArticlesRepository(), new AsyncTransformer<>());
        return new ArticlesPresenter(this, articlesUseCase);
    }

    @Override
    protected ArticlesAdapter getAdapter() {
        return new ArticlesAdapter(this);
    }

    @Override
    protected LinearLayoutManager getLayoutManager() {
        return new LinearLayoutManager(this);
    }

    @Override
    protected Class<? extends AppCompatActivity> getNextActivity() {
        return DetailsActivity.class;
    }

    @Override
    public void init() {
        presenter.loadExtraData(getSerializableData());
        super.init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.filter_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.filter_menu_top:
                filterArticles(Filter.TOP);
                return true;
            case R.id.filter_menu_latest:
                filterArticles(Filter.LATEST);
                return true;
            case R.id.filter_menu_popular:
                filterArticles(Filter.POPULAR);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private Serializable getSerializableData() {
        return getIntent().getSerializableExtra(ExtraKeys.ITEM_KEY);
    }

    private void filterArticles(@Filter String filter) {
        presenter.filterArticles(filter);
    }

}
