package siarhei.mikevich.newstests.presentation.screen.articles;

import java.io.Serializable;

import siarhei.mikevich.newstests.data.api.utils.Filter;
import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.presentation.screen.base.activity.list.BaseListContract;

/**
 * Created by Siarhei Mikevich on 10/13/17.
 */

public interface ArticlesContract {

    interface Presenter extends BaseListContract.Presenter<Article> {

        void loadExtraData(Serializable serializable);

        void filterArticles(@Filter String filter);

    }

}
