package siarhei.mikevich.newstests.presentation.screen.base.activity.list;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.utils.unit.TextUtils;
import siarhei.mikevich.newstests.presentation.R;
import siarhei.mikevich.newstests.presentation.utils.transformer.LoadingTransformer;

/**
 * Created by Siarhei Mikevich on 10/12/17.
 */

public abstract class BaseListPresenter<T, V extends BaseListContract.View<T>> implements BaseListContract.Presenter<T> {

    protected final V view;

    public BaseListPresenter(@NonNull final V view) {
        this.view = view;
    }

    protected abstract Observable<BaseItemsContent<T>> getObservable();

    @Override
    public void onCreate() {
        view.init();
    }

    @Override
    public void loadItems() {
        getObservable()
                .compose(new LoadingTransformer<>(view))
                .subscribe(this::handleResponse, throwable -> {
                    throwable.printStackTrace();
                    setupFailureScreen(R.string.error_list_empty, true);
                });
    }

    @Override
    public void onClickItem(T item) {
        view.navigateNext(item);
    }

    private void handleResponse(BaseItemsContent<T> itemsContent) {
        List<T> items = itemsContent.getItems();
        if (items.size() == 0) {
            String errorMessage = itemsContent.getErrorMessage();
            if (TextUtils.isEmpty(errorMessage)) {
                setupFailureScreen(R.string.error_list_empty, false);
            } else {
                setupFailureScreen(errorMessage);
            }
        } else {
            setupSuccessScreen(items);
        }
    }

    private void setupSuccessScreen(List<T> list) {
        view.showData(list);
        view.setVisibilityContent(true);
    }

    private void setupFailureScreen(String errorMessage) {
        setupFailureScreen(true);
        view.showErrorMessage(errorMessage);
    }

    private void setupFailureScreen(@StringRes int errorMessageId, boolean isShouldToastShow) {
        setupFailureScreen(isShouldToastShow);
        view.showErrorMessage(errorMessageId);
    }

    private void setupFailureScreen(boolean isShouldToastShow) {
        view.showData(new ArrayList<>());
        view.setVisibilityContent(false);
        if (isShouldToastShow) {
            view.showToastMessage(R.string.error);
        }
    }

}
