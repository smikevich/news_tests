package siarhei.mikevich.newstests.presentation.screen.details;

import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;

import java.io.Serializable;

import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.presentation.R;
import siarhei.mikevich.newstests.presentation.screen.base.activity.BaseActivity;
import siarhei.mikevich.newstests.presentation.utils.ExtraKeys;
import siarhei.mikevich.newstests.presentation.utils.browser.BrowserUtils;
import siarhei.mikevich.newstests.presentation.views.icon.ImageViewFresco;
import siarhei.mikevich.newstests.presentation.views.items.DetailsView;

/**
 * Created by Siarhei Mikevich on 10/7/17.
 */

public class DetailsActivity
        extends BaseActivity<DetailsContract.Presenter>
        implements DetailsContract.View, AppBarLayout.OnOffsetChangedListener {

    private AppBarLayout appBarLayout;
    private ImageViewFresco detailsImageView;
    private DetailsView detailsView;
    private FloatingActionButton aboutFloatingActionButton;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_details;
    }

    @Override
    protected int getActivityTitleId() {
        return R.string.title_details;
    }

    @Override
    protected DetailsContract.Presenter getPresenter() {
        return new DetailsPresenter(this);
    }

    @Override
    public void init() {
        bindViews();
        setupViews();
        loadArticle();
    }

    @Override
    public void showToastMessage(int messageId) {
        super.showToastMessage(messageId);
    }

    @Override
    public void showArticle(Article article) {
        detailsImageView.setImageURI(article.getUrlToImage());
        detailsView.setup(article);
    }

    @Override
    public void navigateBrowserByUrl(String url) {
        BrowserUtils.navigateBrowser(this, url, e -> showToastMessage(R.string.error));
    }

    @Override
    public void showAboutButton() {
        ViewCompat.animate(aboutFloatingActionButton).scaleY(1).scaleX(1).start();
    }

    @Override
    public void hideAboutButton() {
        ViewCompat.animate(aboutFloatingActionButton).scaleY(0).scaleX(0).start();
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        presenter.onOffsetChanged(appBarLayout.getTotalScrollRange(), verticalOffset);
    }

    private void bindViews() {
        appBarLayout = findViewById(R.id.app_bar_layout);
        detailsImageView = findViewById(R.id.details_image_view);
        detailsView = findViewById(R.id.details_view);
        aboutFloatingActionButton = findViewById(R.id.about_floating_action_button);
    }

    private void setupViews() {
        appBarLayout.addOnOffsetChangedListener(this);
        aboutFloatingActionButton.setOnClickListener(v -> onClickReadMore());
    }

    private void loadArticle() {
        Intent intent = getIntent();
        Serializable serializable = intent.getSerializableExtra(ExtraKeys.ITEM_KEY);
        presenter.loadArticleFromSerializable(serializable);
    }

    private void onClickReadMore() {
        presenter.onClickReadMore();
    }

}
