package siarhei.mikevich.newstests.presentation.screen.base.adapter;

import android.support.annotation.IdRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Siarhei Mikevich on 10/11/17.
 */

public abstract class BaseViewHolder<V extends View, T> extends RecyclerView.ViewHolder {

    protected V view;

    @IdRes
    protected abstract int getViewId();

    protected abstract void setupView(V view, T item);

    public BaseViewHolder(View itemView) {
        super(itemView);
        bindView(itemView);
    }

    public void setupView(T item) {
        setupView(view, item);
    }

    private void bindView(View itemView) {
        view = itemView.findViewById(getViewId());
    }

}
