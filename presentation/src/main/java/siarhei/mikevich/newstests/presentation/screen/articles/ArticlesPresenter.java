package siarhei.mikevich.newstests.presentation.screen.articles;

import android.support.annotation.NonNull;

import java.io.Serializable;

import rx.Observable;
import siarhei.mikevich.newstests.data.api.utils.Filter;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.domain.model.content.Source;
import siarhei.mikevich.newstests.domain.usecase.ArticlesUseCase;
import siarhei.mikevich.newstests.presentation.R;
import siarhei.mikevich.newstests.presentation.screen.base.activity.list.BaseListContract;
import siarhei.mikevich.newstests.presentation.screen.base.activity.list.BaseListPresenter;

/**
 * Created by Siarhei Mikevich on 10/13/17.
 */

class ArticlesPresenter extends BaseListPresenter<Article, BaseListContract.View<Article>> implements ArticlesContract.Presenter {

    private ArticlesUseCase articlesUseCase;

    private Source source;
    private @Filter String filter = Filter.TOP;

    ArticlesPresenter(@NonNull BaseListContract.View<Article> view, @NonNull ArticlesUseCase articlesUseCase) {
        super(view);
        this.articlesUseCase = articlesUseCase;
    }

    @Override
    protected Observable<BaseItemsContent<Article>> getObservable() {
        return articlesUseCase.execUseCase(new ArticlesUseCase.Params(source.getId(), filter));
    }

    @Override
    public void loadExtraData(Serializable serializable) {
        if (serializable != null && serializable instanceof Source) {
            source = (Source) serializable;
        } else {
            view.showToastMessage(R.string.error);
        }
    }

    @Override
    public void filterArticles(@Filter String filter) {
        this.filter = filter;
        loadItems();
    }

}
