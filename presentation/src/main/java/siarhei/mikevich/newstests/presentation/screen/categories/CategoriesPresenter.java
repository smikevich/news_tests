package siarhei.mikevich.newstests.presentation.screen.categories;

import android.support.annotation.NonNull;

import rx.Observable;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Category;
import siarhei.mikevich.newstests.domain.usecase.CategoriesUseCase;
import siarhei.mikevich.newstests.presentation.screen.base.activity.list.BaseListContract;
import siarhei.mikevich.newstests.presentation.screen.base.activity.list.BaseListPresenter;

/**
 * Created by mikes on 13.10.17.
 */

class CategoriesPresenter
        extends BaseListPresenter<Category, BaseListContract.View<Category>> implements BaseListContract.Presenter<Category> {

    private final CategoriesUseCase categoriesUseCase;

    CategoriesPresenter(@NonNull BaseListContract.View<Category> view, @NonNull CategoriesUseCase categoriesUseCase) {
        super(view);
        this.categoriesUseCase = categoriesUseCase;
    }

    @Override
    protected Observable<BaseItemsContent<Category>> getObservable() {
        return categoriesUseCase.execUseCase(new CategoriesUseCase.Params("categories.json"));
    }

}
