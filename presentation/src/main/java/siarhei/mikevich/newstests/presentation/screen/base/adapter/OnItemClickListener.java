package siarhei.mikevich.newstests.presentation.screen.base.adapter;

/**
 * Created by Siarhei Mikevich on 10/11/17.
 */

public interface OnItemClickListener<T> {

    void onItemClicked(T item);

}
