package siarhei.mikevich.newstests.presentation.screen.categories;

import android.view.View;

import siarhei.mikevich.newstests.domain.model.content.Category;
import siarhei.mikevich.newstests.presentation.R;
import siarhei.mikevich.newstests.presentation.screen.base.adapter.BaseAdapter;
import siarhei.mikevich.newstests.presentation.screen.base.adapter.BaseViewHolder;
import siarhei.mikevich.newstests.presentation.screen.base.adapter.OnItemClickListener;
import siarhei.mikevich.newstests.presentation.views.items.CategoryView;

/**
 * Created by Siarhei Mikevich on 10/11/17.
 */

class CategoriesAdapter extends BaseAdapter<CategoriesAdapter.ViewHolder, Category, OnItemClickListener<Category>> {

    CategoriesAdapter(OnItemClickListener<Category> listener) {
        super(listener);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item_category;
    }

    @Override
    protected ViewHolder getViewHolder(View itemView) {
        return new ViewHolder(itemView);
    }

    @Override
    protected void setupView(ViewHolder viewHolder, Category item) {
        viewHolder.setupView(item);
    }

    class ViewHolder extends BaseViewHolder<CategoryView, Category> {

        ViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected int getViewId() {
            return R.id.category_view;
        }

        @Override
        protected void setupView(CategoryView view, Category item) {
            view.setup(item);
        }

    }

}
