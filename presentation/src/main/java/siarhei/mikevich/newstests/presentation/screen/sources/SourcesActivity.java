package siarhei.mikevich.newstests.presentation.screen.sources;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.io.Serializable;

import siarhei.mikevich.newstests.data.repository.RepositoryProvider;
import siarhei.mikevich.newstests.domain.model.content.Source;
import siarhei.mikevich.newstests.domain.usecase.SourcesUseCase;
import siarhei.mikevich.newstests.presentation.R;
import siarhei.mikevich.newstests.presentation.screen.articles.ArticlesActivity;
import siarhei.mikevich.newstests.presentation.screen.base.activity.list.BaseListActivity;
import siarhei.mikevich.newstests.presentation.utils.ExtraKeys;
import siarhei.mikevich.newstests.presentation.utils.browser.BrowserUtils;
import siarhei.mikevich.newstests.presentation.utils.transformer.AsyncTransformer;

/**
 * Created by Siarhei Mikevich on 10/13/17.
 */

public class SourcesActivity
        extends BaseListActivity<Source, SourcesPresenter, SourcesAdapter> implements SourcesContract.View, OnSourceClickListener {

    @Override
    protected int getActivityTitleId() {
        return R.string.title_sources;
    }

    @Override
    protected SourcesPresenter getPresenter() {
        SourcesUseCase sourcesUseCase = new SourcesUseCase(RepositoryProvider.provideSourcesRepository(), new AsyncTransformer<>());
        return new SourcesPresenter(this, sourcesUseCase);
    }

    @Override
    protected SourcesAdapter getAdapter() {
        return new SourcesAdapter(this);
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(this);
    }

    @Override
    protected Class<? extends AppCompatActivity> getNextActivity() {
        return ArticlesActivity.class;
    }

    @Override
    public void init() {
        presenter.loadExtraData(getSerializableData());
        super.init();
    }

    @Override
    public void navigateBrowserByUrl(String url) {
        BrowserUtils.navigateBrowser(this, url, e -> showToastMessage(R.string.error));
    }

    @Override
    public void onClickSourceAbout(Source source) {
        presenter.onClickSourceAbout(source);
    }

    private Serializable getSerializableData() {
        return getIntent().getSerializableExtra(ExtraKeys.ITEM_KEY);
    }

}
