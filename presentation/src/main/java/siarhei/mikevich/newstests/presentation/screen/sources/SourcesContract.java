package siarhei.mikevich.newstests.presentation.screen.sources;

import java.io.Serializable;

import siarhei.mikevich.newstests.domain.model.content.Source;
import siarhei.mikevich.newstests.presentation.screen.base.activity.list.BaseListContract;

/**
 * Created by Siarhei Mikevich on 10/13/17.
 */

public interface SourcesContract {

    interface View extends BaseListContract.View<Source> {

        void navigateBrowserByUrl(String url);

    }

    interface Presenter extends BaseListContract.Presenter<Source> {

        void loadExtraData(Serializable serializable);

        void onClickSourceAbout(Source source);

    }

}
