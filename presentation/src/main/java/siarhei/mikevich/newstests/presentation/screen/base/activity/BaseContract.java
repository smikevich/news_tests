package siarhei.mikevich.newstests.presentation.screen.base.activity;

import android.support.annotation.StringRes;

/**
 * Created by mikes on 04.10.17.
 */

public interface BaseContract {

    interface View {

        void init();

        void showToastMessage(@StringRes int messageId);

    }

    interface Presenter {

        void onCreate();

    }

}
