package siarhei.mikevich.newstests.presentation.screen.base.activity.list;

import android.support.annotation.StringRes;

import java.util.List;

import siarhei.mikevich.newstests.presentation.screen.base.activity.BaseContract;
import siarhei.mikevich.newstests.presentation.views.loadingdialog.LoadingView;

/**
 * Created by Siarhei Mikevich on 10/12/17.
 */

public interface BaseListContract {

    interface View<T> extends BaseContract.View, LoadingView {

        void showData(List<T> list);

        void showErrorMessage(String errorMessage);

        void showErrorMessage(@StringRes int errorMessageId);

        void setVisibilityContent(boolean isVisible);

        void navigateNext(T item);

    }

    interface Presenter<T> extends BaseContract.Presenter {

        void loadItems();

        void onClickItem(T item);

    }

}
