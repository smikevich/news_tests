package siarhei.mikevich.newstests.presentation.screen.base.activity.list;

import android.content.Intent;
import android.support.annotation.StringRes;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

import siarhei.mikevich.newstests.presentation.R;
import siarhei.mikevich.newstests.presentation.screen.base.activity.BaseActivity;
import siarhei.mikevich.newstests.presentation.screen.base.adapter.BaseAdapter;
import siarhei.mikevich.newstests.presentation.screen.base.adapter.OnItemClickListener;
import siarhei.mikevich.newstests.presentation.utils.ExtraKeys;

/**
 * Created by mikes on 12.10.17.
 */

public abstract class BaseListActivity
        <
        T,
        P extends BaseListContract.Presenter<T>,
        A extends BaseAdapter<? extends RecyclerView.ViewHolder, T, ? extends OnItemClickListener<T>>
        >
        extends BaseActivity<P>
        implements BaseListContract.View<T>, SwipeRefreshLayout.OnRefreshListener, OnItemClickListener<T> {

    private RecyclerView recyclerView;
    private SwipeRefreshLayout refreshLayout;
    private TextView errorTextView;
    private A adapter;

    protected abstract A getAdapter();

    protected abstract RecyclerView.LayoutManager getLayoutManager();

    protected abstract Class<? extends AppCompatActivity> getNextActivity();

    @Override
    protected int getLayoutId() {
        return R.layout.activity_list;
    }

    @Override
    public void showLoadingView() {
        super.showLoadingView();
    }

    @Override
    public void hideLoadingView() {
        super.hideLoadingView();
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void init() {
        bindViews();
        setupViews();
        loadItems();
    }

    @Override
    public void showToastMessage(@StringRes int messageId) {
        super.showToastMessage(messageId);
    }

    @Override
    public void showData(List<T> list) {
        adapter.changeDataSet(list);
    }

    @Override
    public void showErrorMessage(String errorMessage) {
        showErrorMessageText(errorMessage);
    }

    @Override
    public void showErrorMessage(@StringRes int errorMessageId) {
        showErrorMessageText(getString(errorMessageId));
    }

    @Override
    public void setVisibilityContent(boolean isVisible) {
        recyclerView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        errorTextView.setVisibility(isVisible ? View.GONE : View.VISIBLE);
    }

    @Override
    public void navigateNext(T item) {
        Intent intent = new Intent(this, getNextActivity());
        intent.putExtra(ExtraKeys.ITEM_KEY, (Serializable) item);
        startActivity(intent);
    }

    @Override
    public void onItemClicked(T item) {
        presenter.onClickItem(item);
    }

    @Override
    public void onRefresh() {
        presenter.loadItems();
    }

    private void bindViews() {
        recyclerView = findViewById(R.id.recycler_view);
        refreshLayout = findViewById(R.id.refresh_layout);
        errorTextView = findViewById(R.id.error_text_view);
    }

    private void setupViews() {
        recyclerView.setLayoutManager(getLayoutManager());
        adapter = getAdapter();
        recyclerView.setAdapter(adapter);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setColorSchemeResources(R.color.colorAccent);
    }

    private void loadItems() {
        presenter.loadItems();
    }

    private void showErrorMessageText(String errorMessageText) {
        errorTextView.setText(errorMessageText);
    }

}
