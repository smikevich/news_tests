package siarhei.mikevich.newstests.presentation.screen.sources;

import android.support.annotation.NonNull;

import java.io.Serializable;

import rx.Observable;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Category;
import siarhei.mikevich.newstests.domain.model.content.Source;
import siarhei.mikevich.newstests.domain.usecase.SourcesUseCase;
import siarhei.mikevich.newstests.presentation.R;
import siarhei.mikevich.newstests.presentation.screen.base.activity.list.BaseListPresenter;

/**
 * Created by Siarhei Mikevich on 10/13/17.
 */

class SourcesPresenter
        extends BaseListPresenter<Source, SourcesContract.View> implements SourcesContract.Presenter {

    private SourcesUseCase sourcesUseCase;
    private Category category;

    SourcesPresenter(@NonNull SourcesContract.View view, @NonNull SourcesUseCase sourcesUseCase) {
        super(view);
        this.sourcesUseCase = sourcesUseCase;
    }

    @Override
    protected Observable<BaseItemsContent<Source>> getObservable() {
        return sourcesUseCase.execUseCase(new SourcesUseCase.Params(category.getId()));
    }

    @Override
    public void loadExtraData(Serializable serializable) {
        if (serializable != null && serializable instanceof Category) {
            category = (Category) serializable;
        } else {
            view.showToastMessage(R.string.error);
        }
    }

    @Override
    public void onClickSourceAbout(Source source) {
        view.navigateBrowserByUrl(source.getUrl());
    }

}
