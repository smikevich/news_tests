package siarhei.mikevich.newstests.presentation.utils.browser;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Siarhei Mikevich on 11/17/17.
 */

public class BrowserUtils {

    public static void navigateBrowser(AppCompatActivity activity, String url, OnBrowserListener listener) {
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            listener.onError(e);
        } catch (NullPointerException e) {
            e.printStackTrace();
            listener.onError(new Exception());
        }
    }

}
