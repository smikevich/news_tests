package siarhei.mikevich.newstests.presentation.screen.sources;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.FrameLayout;

import siarhei.mikevich.newstests.domain.model.content.Source;
import siarhei.mikevich.newstests.presentation.R;
import siarhei.mikevich.newstests.presentation.screen.base.adapter.BaseAdapter;
import siarhei.mikevich.newstests.presentation.screen.base.adapter.BaseViewHolder;
import siarhei.mikevich.newstests.presentation.views.items.SourceView;

/**
 * Created by Siarhei Mikevich on 10/11/17.
 */

class SourcesAdapter extends BaseAdapter<SourcesAdapter.ViewHolder, Source, OnSourceClickListener> {

    SourcesAdapter(OnSourceClickListener listener) {
        super(listener);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item_source;
    }

    @Override
    protected ViewHolder getViewHolder(View itemView) {
        return new ViewHolder(itemView);
    }

    @Override
    protected void setupView(ViewHolder viewHolder, Source item) {
        viewHolder.getAboutButton().setOnClickListener(v -> onClickSourceAbout(item));
        viewHolder.setupView(item);
    }

    private void onClickSourceAbout(Source item) {
        if (listener != null) {
            listener.onClickSourceAbout(item);
        }
    }

    class ViewHolder extends BaseViewHolder<SourceView, Source> {

        ViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected int getViewId() {
            return R.id.source_view;
        }

        @Override
        protected void setupView(SourceView view, Source item) {
            view.setup(item);
        }

        @NonNull
        FrameLayout getAboutButton() {
            return view.findViewById(R.id.source_about_button_ripple_circle).findViewById(R.id.button);
        }

    }

}
