package siarhei.mikevich.newstests.presentation.screen.details;

import java.io.Serializable;

import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.presentation.R;

/**
 * Created by Siarhei Mikevich on 10/7/17.
 */

public class DetailsPresenter implements DetailsContract.Presenter {

    private final DetailsContract.View view;
    private Article article;
    private boolean isVisibleFloatingActionButton = true;

    public DetailsPresenter(DetailsContract.View view) {
        this.view = view;
    }

    @Override
    public void onCreate() {
        view.init();
    }

    @Override
    public void loadArticleFromSerializable(Serializable serializable) {
        if (serializable != null && serializable instanceof Article) {
            Article article = (Article) serializable;
            this.article = article;
            view.showArticle(article);
        } else {
            showToastMessage(R.string.error);
        }
    }

    @Override
    public void onClickReadMore() {
        if (article != null) {
            view.navigateBrowserByUrl(article.getUrl());
        } else {
            showToastMessage(R.string.error);
        }
    }

    @Override
    public void onOffsetChanged(int totalScrollRange, int verticalOffset) {
        int percentage = (Math.abs(verticalOffset)) * 100 / totalScrollRange;
        if (percentage >= 20 && !isVisibleFloatingActionButton) {
            isVisibleFloatingActionButton = true;
            view.hideAboutButton();
        }
        else if(percentage < 20 && isVisibleFloatingActionButton){
            isVisibleFloatingActionButton = false;
            view.showAboutButton();
        }
    }

    private void showToastMessage(int messageId) {
        view.showToastMessage(messageId);
    }

}
