package siarhei.mikevich.newstests.presentation.views.icon;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;

import siarhei.mikevich.newstests.presentation.R;
import siarhei.mikevich.newstests.presentation.utils.display.DisplayUtils;

/**
 * Created by mikes on 17.10.17.
 */

public class ImageViewFresco extends SimpleDraweeView {

    private boolean isRoundImageCorners;
    private boolean isCustomSize;

    public ImageViewFresco(Context context) {
        super(context);
        init();
    }

    public ImageViewFresco(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttrs(attrs);
        init();
    }

    public ImageViewFresco(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initAttrs(attrs);
        init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (isCustomSize) {
            int width = getMeasuredWidth();
            int height = width / 2;
            super.onMeasure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
        }
    }

    private void initAttrs(AttributeSet attrs) {
        TypedArray a = getContext().getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.ImageViewFresco,
                0, 0);

        try {
            isRoundImageCorners = a.getBoolean(R.styleable.ImageViewFresco_image_loader_rounded_corners, false);
            isCustomSize = a.getBoolean(R.styleable.ImageViewFresco_image_loader_custom_size, false);
        } finally {
            a.recycle();
        }
    }

    private void init() {
        if (isRoundImageCorners) {
            int pixels = DisplayUtils.getPixelsByDp(4, getContext());
            RoundingParams roundingParams = RoundingParams.fromCornersRadii(pixels, pixels, 0, 0);
            setHierarchy(new GenericDraweeHierarchyBuilder(getResources())
                    .setRoundingParams(roundingParams)
                    .build());
        }
    }

}
