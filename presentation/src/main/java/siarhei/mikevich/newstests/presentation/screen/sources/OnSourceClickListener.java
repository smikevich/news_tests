package siarhei.mikevich.newstests.presentation.screen.sources;

import siarhei.mikevich.newstests.domain.model.content.Source;
import siarhei.mikevich.newstests.presentation.screen.base.adapter.OnItemClickListener;

/**
 * Created by Siarhei Mikevich on 10/11/17.
 */

public interface OnSourceClickListener extends OnItemClickListener<Source> {

    void onClickSourceAbout(Source source);

}
