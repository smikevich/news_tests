package siarhei.mikevich.newstests.presentation.utils.actions;

import android.support.design.widget.AppBarLayout;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.view.View;

import org.hamcrest.Matcher;

import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;

/**
 * Created by Siarhei Mikevich on 11/14/17.
 */

public class AppBarLayoutAction {

    public static ViewAction expanded(boolean isExpanded) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isAssignableFrom(AppBarLayout.class);
            }

            @Override
            public String getDescription() {
                return isExpanded ? "Expanding app bar layout" : "Collapsing app bar layout";
            }

            @Override
            public void perform(UiController uiController, View view) {
                AppBarLayout appBarLayout = (AppBarLayout) view;
                appBarLayout.setExpanded(isExpanded);
            }
        };
    }

}
