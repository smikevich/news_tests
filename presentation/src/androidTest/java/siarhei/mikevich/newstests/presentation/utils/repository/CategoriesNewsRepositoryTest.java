package siarhei.mikevich.newstests.presentation.utils.repository;

import rx.Observable;
import siarhei.mikevich.newstests.data.repository.RepositoryProvider;
import siarhei.mikevich.newstests.data.repository.categories.CategoriesNewsRepository;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Category;
import siarhei.mikevich.newstests.domain.repository.CategoriesRepository;

/**
 * Created by Siarhei Mikevich on 11/27/17.
 */

public class CategoriesNewsRepositoryTest implements CategoriesRepository {

    @Override
    public Observable<BaseItemsContent<Category>> getCategories(String assetPath) {
        RepositoryProvider.setCategoriesRepository(new CategoriesNewsRepository());
        return RepositoryProvider.provideCategoriesRepository()
                .getCategories(assetPath);
    }

}
