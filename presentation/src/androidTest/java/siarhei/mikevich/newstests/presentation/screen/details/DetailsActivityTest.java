package siarhei.mikevich.newstests.presentation.screen.details;

import android.content.Intent;
import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Random;

import rx.Observable;
import siarhei.mikevich.newstests.data.model.response.ArticlesResponse;
import siarhei.mikevich.newstests.data.utils.transformer.DataTransformer;
import siarhei.mikevich.newstests.data.utils.transformer.responsetransformer.ResponseAssetsTransformer;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Article;
import siarhei.mikevich.newstests.domain.utils.unit.DateUtils;
import siarhei.mikevich.newstests.domain.utils.unit.StringUtils;
import siarhei.mikevich.newstests.presentation.R;
import siarhei.mikevich.newstests.presentation.utils.ExtraKeys;
import siarhei.mikevich.newstests.presentation.utils.actions.AppBarLayoutAction;

/**
 * Created by Siarhei Mikevich on 11/14/17.
 */

// use mock flavor

@RunWith(AndroidJUnit4.class)
public class DetailsActivityTest {

    @Rule
    public final ActivityTestRule<DetailsActivity> activity = new ActivityTestRule<>(DetailsActivity.class, false, false);

    @Before
    public void init() throws Exception {
        Intents.init();
    }

    @Test
    public void testTitle() throws Exception {
        startActivity(getRandomFullArticle());
        Assert.assertEquals(activity.getActivity().getString(R.string.title_details), activity.getActivity().getTitle());
    }

    @Test
    public void testDetailsViewWithFullArticle() throws Exception {
        Article randomArticle = getRandomFullArticle();
        startActivity(randomArticle);
        checkVisibilityView(R.id.details_header_content, true);
        checkViewWithTextAssertion(R.id.details_author_text_view, randomArticle.getAuthor());
        checkVisibilityView(R.id.details_author_icon_view, true);
        checkViewWithTextAssertion(R.id.symbols_text_view, new StringUtils().getAbbreviation(randomArticle.getAuthor()));
        checkVisibilityView(R.id.symbols_text_view, true);
        checkViewWithTextAssertion(R.id.details_date_text_view, DateUtils.parseDateTime(randomArticle.getPublishedAt()));
        checkVisibilityView(R.id.details_date_text_view, true);
        checkViewWithTextAssertion(R.id.details_title_text_view, randomArticle.getTitle());
        checkVisibilityView(R.id.details_title_text_view, true);
        checkViewWithTextAssertion(R.id.details_description_text_view, randomArticle.getDescription());
        checkVisibilityView(R.id.details_description_text_view, true);
        checkViewWithTextAssertion(R.id.details_error_text_view, activity.getActivity().getString(R.string.error_no_content_details));
        checkVisibilityView(R.id.details_error_text_view, false);
    }

    @Test
    public void testDetailsViewWithFullArticle2() throws Exception {
        Article randomArticle = getArticle(true, false, false, false, false, true);
        startActivity(randomArticle);
        checkVisibilityView(R.id.details_header_content, true);
        checkViewWithTextAssertion(R.id.details_author_text_view, randomArticle.getAuthor());
        checkVisibilityView(R.id.details_author_icon_view, true);
        checkViewWithTextAssertion(R.id.symbols_text_view, new StringUtils().getAbbreviation(randomArticle.getAuthor()));
        checkVisibilityView(R.id.symbols_text_view, true);
        checkViewWithTextAssertion(R.id.details_date_text_view, DateUtils.parseDateTime(randomArticle.getPublishedAt()));
        checkVisibilityView(R.id.details_date_text_view, true);
        checkViewWithTextAssertion(R.id.details_title_text_view, "");
        checkVisibilityView(R.id.details_title_text_view, false);
        checkViewWithTextAssertion(R.id.details_description_text_view, "");
        checkVisibilityView(R.id.details_description_text_view, false);
        checkViewWithTextAssertion(R.id.details_error_text_view, activity.getActivity().getString(R.string.error_no_content_details));
        checkVisibilityView(R.id.details_error_text_view, true);
    }

    @Test
    public void testDetailsViewWithFullArticle3() throws Exception {
        Article randomArticle = getArticle(true, true, false, false, false, true);
        startActivity(randomArticle);
        checkVisibilityView(R.id.details_header_content, true);
        checkViewWithTextAssertion(R.id.details_author_text_view, randomArticle.getAuthor());
        checkVisibilityView(R.id.details_author_icon_view, true);
        checkViewWithTextAssertion(R.id.symbols_text_view, new StringUtils().getAbbreviation(randomArticle.getAuthor()));
        checkVisibilityView(R.id.symbols_text_view, true);
        checkViewWithTextAssertion(R.id.details_date_text_view, DateUtils.parseDateTime(randomArticle.getPublishedAt()));
        checkVisibilityView(R.id.details_date_text_view, true);
        checkViewWithTextAssertion(R.id.details_title_text_view, randomArticle.getTitle());
        checkVisibilityView(R.id.details_title_text_view, true);
        checkViewWithTextAssertion(R.id.details_description_text_view, "");
        checkVisibilityView(R.id.details_description_text_view, false);
        checkViewWithTextAssertion(R.id.details_error_text_view, activity.getActivity().getString(R.string.error_no_content_details));
        checkVisibilityView(R.id.details_error_text_view, false);
    }

    @Test
    public void testDetailsViewWithFullArticle4() throws Exception {
        Article randomArticle = getArticle(true, false, true, true, false, true);
        startActivity(randomArticle);
        checkVisibilityView(R.id.details_header_content, true);
        checkViewWithTextAssertion(R.id.details_author_text_view, randomArticle.getAuthor());
        checkVisibilityView(R.id.details_author_icon_view, true);
        checkViewWithTextAssertion(R.id.symbols_text_view, new StringUtils().getAbbreviation(randomArticle.getAuthor()));
        checkVisibilityView(R.id.symbols_text_view, true);
        checkViewWithTextAssertion(R.id.details_date_text_view, DateUtils.parseDateTime(randomArticle.getPublishedAt()));
        checkVisibilityView(R.id.details_date_text_view, true);
        checkViewWithTextAssertion(R.id.details_title_text_view, "");
        checkVisibilityView(R.id.details_title_text_view, false);
        checkViewWithTextAssertion(R.id.details_description_text_view, randomArticle.getDescription());
        checkVisibilityView(R.id.details_description_text_view, true);
        checkViewWithTextAssertion(R.id.details_error_text_view, activity.getActivity().getString(R.string.error_no_content_details));
        checkVisibilityView(R.id.details_error_text_view, false);
    }

    @Test
    public void testDetailsViewHeaderContentVisibility1() throws Exception {
        Article randomArticle = getArticle(true, false, false, false, false, true);
        startActivity(randomArticle);
        checkVisibilityView(R.id.details_header_content, true);
    }

    @Test
    public void testDetailsViewHeaderContentVisibility2() throws Exception {
        Article randomArticle = getArticle(true, false, false, false, false, false);
        startActivity(randomArticle);
        checkVisibilityView(R.id.details_header_content, true);
    }

    @Test
    public void testDetailsViewHeaderContentVisibility3() throws Exception {
        Article randomArticle = getArticle(false, false, false, false, false, true);
        startActivity(randomArticle);
        checkVisibilityView(R.id.details_header_content, true);
    }

    @Test
    public void testDetailsViewHeaderContentVisibility4() throws Exception {
        Article randomArticle = getArticle(false, false, false, false, false, false);
        startActivity(randomArticle);
        checkVisibilityView(R.id.details_header_content, false);
    }

    @Test
    public void testClickReadMoreWithValidUrl() throws Exception {
        Article randomArticle = getRandomFullArticle();
        startActivity(randomArticle);
        clickFloatingActionButton();
        Intents.intended(IntentMatchers.hasAction(Intent.ACTION_VIEW));
    }

    @Test
    public void testClickReadMoreWithWrongUrl() throws Exception {
        Article randomArticle = getArticle(true, true, true, false, true, true);
        startActivity(randomArticle);
        clickFloatingActionButton();
        Intents.intended(Matchers.not(IntentMatchers.hasAction(Intent.ACTION_VIEW)));
    }

    @Test
    public void testClickReadMoreWithWrongUrl2() throws Exception {
        Article article = new Article();
        article.setUrl("some_wrong_url");
        startActivity(article);
        clickFloatingActionButton();
        Intents.intended(Matchers.not(IntentMatchers.hasAction(Intent.ACTION_VIEW)));
    }

    @Test
    public void testAppBarLayoutExpanding() throws Exception {
        startActivity(getRandomFullArticle());
        expandAppBarLayout(true);
        checkCompletelyDisplayedFloatingActionButton(true);
    }

    @Test
    public void testAppBarLayoutCollapsing() throws Exception {
        startActivity(getRandomFullArticle());
        expandAppBarLayout(false);
        checkCompletelyDisplayedFloatingActionButton(false);
    }

    @Test
    public void testScenarioFloatingActionButton() throws Exception {
        startActivity(getRandomFullArticle());
        checkCompletelyDisplayedFloatingActionButton(true);
        expandAppBarLayout(false);
        checkCompletelyDisplayedFloatingActionButton(false);
        expandAppBarLayout(true);
        checkCompletelyDisplayedFloatingActionButton(true);
    }

    @After
    public void finish() throws Exception {
        Intents.release();
    }

    private void startActivity(Article article) {
        Intent intent = new Intent(InstrumentationRegistry.getTargetContext(), DetailsActivity.class);
        intent.putExtra(ExtraKeys.ITEM_KEY, article);
        activity.launchActivity(intent);
        SystemClock.sleep(3000);
    }

    private void checkViewWithTextAssertion(int viewId, String text) {
        Espresso.onView(ViewMatchers.withId(viewId))
                .check(ViewAssertions.matches(ViewMatchers.withText(text)));
    }

    private void checkVisibilityView(int viewId, boolean isVisible) {
        Espresso.onView(ViewMatchers.withId(viewId))
                .check(ViewAssertions.matches(ViewMatchers.withEffectiveVisibility(isVisible ? ViewMatchers.Visibility.VISIBLE : ViewMatchers.Visibility.GONE)));
    }

    private void clickFloatingActionButton() {
        Espresso.onView(ViewMatchers.withId(R.id.about_floating_action_button))
                .perform(ViewActions.click());
    }

    private void expandAppBarLayout(boolean isExpanded) {
        Espresso.onView(ViewMatchers.withId(R.id.app_bar_layout))
                .perform(AppBarLayoutAction.expanded(isExpanded));
        SystemClock.sleep(3000);
    }

    private void checkCompletelyDisplayedFloatingActionButton(boolean isCompletelyDisplayed) {
        Espresso.onView(ViewMatchers.withId(R.id.about_floating_action_button))
                .check(ViewAssertions.matches(isCompletelyDisplayed ? ViewMatchers.isCompletelyDisplayed() : Matchers.not(ViewMatchers.isCompletelyDisplayed())));
    }

    private Article getRandomFullArticle() {
        return Observable.just("articles.json")
                .compose(new ResponseAssetsTransformer<>(ArticlesResponse.class))
                .compose(new DataTransformer<>(Article.class))
                .map(BaseItemsContent::getItems)
                .map(articles -> articles.get(new Random().nextInt(articles.size())))
                .toBlocking()
                .first();
    }

    private Article getArticle(boolean isAuthor, boolean isTitle, boolean isDescription, boolean isUrl, boolean isUrlToImage, boolean isPublishedAt) {
        Article article = new Article();
        article.setAuthor(isAuthor ? "Alex Heath" : null);
        article.setTitle(isTitle ? "LIVE: Facebook crushes Q3 earnings as stock trades at all-time high" : null);
        article.setDescription(isDescription ? "Facebook crushed its..." : null);
        article.setUrl(isUrl ? "http://www.businessinsider.com/facebook-q3-earning-results-2017-11" : null);
        article.setUrlToImage(isUrlToImage ? "http://static6.businessinsider.com/image/59fa242658a0c12b0e8b4f8c-1190-625/live-facebook-crushes-q3-earnings-as-stock-trades-at-all-time-high.jpg" : null);
        article.setPublishedAt(isPublishedAt ? "2017-11-01T19:51:58Z" : null);
        return article;
    }

}
