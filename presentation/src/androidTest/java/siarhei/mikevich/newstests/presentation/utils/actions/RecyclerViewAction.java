package siarhei.mikevich.newstests.presentation.utils.actions;

import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.view.View;

import org.hamcrest.Matcher;

import siarhei.mikevich.newstests.presentation.R;

/**
 * Created by Siarhei Mikevich on 11/10/17.
 */

public class RecyclerViewAction {

    public static ViewAction clickAbout() {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return "Clicked about button in recycler view item";
            }

            @Override
            public void perform(UiController uiController, View view) {
                View v = view.findViewById(R.id.source_about_button_ripple_circle).findViewById(R.id.button);
                v.performClick();
            }
        };
    }

}
