package siarhei.mikevich.newstests.presentation.screen.articles;

import android.content.Intent;
import android.os.SystemClock;
import android.support.annotation.StringRes;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import siarhei.mikevich.newstests.data.api.interceptor.MockingInterceptor;
import siarhei.mikevich.newstests.domain.model.content.Source;
import siarhei.mikevich.newstests.presentation.R;
import siarhei.mikevich.newstests.presentation.screen.details.DetailsActivity;
import siarhei.mikevich.newstests.presentation.utils.ExtraKeys;
import siarhei.mikevich.newstests.presentation.utils.matchers.RecyclerViewMatcher;
import siarhei.mikevich.newstests.presentation.utils.matchers.ToastMatcher;

/**
 * Created by Siarhei Mikevich on 11/10/17.
 */

// use mock flavor

@RunWith(AndroidJUnit4.class)
public class ArticlesActivityTest {

    private Source validSource;
    private Source wrongSource;

    @Rule
    public ActivityTestRule<ArticlesActivity> activity = new ActivityTestRule<>(ArticlesActivity.class, false, false);

    @Before
    public void setup() throws Exception {
        validSource = new Source();
        validSource.setId("any_valid_source");
        wrongSource = new Source();
        wrongSource.setId(MockingInterceptor.WRONG_SOURCE);
        Intents.init();
    }

    @Test
    public void testTitle() throws Exception {
        startActivity(validSource);
        Assert.assertEquals(activity.getActivity().getString(R.string.title_articles), activity.getActivity().getTitle());
    }

    @Test
    public void testUIVisibilityWithValidSource() throws Exception {
        startActivity(validSource);
        checkVisibilityContent(true);
    }

    @Test
    public void testUIVisibilityWithWrongSource() throws Exception {
        startActivity(wrongSource);
        checkVisibilityContent(false);
        checkViewWithTextAssertion(R.id.error_text_view, "The news source you've entered doesn't exist. Check your spelling, or see /v1/sources for a list of valid sources.");
        // TODO: checkToastMessageShown();
    }

    @Test
    public void testUIVisibilityWithUnavailableSortedBy() throws Exception {
        startActivity(validSource);
        clickSort(R.string.menu_filter_popular);
        checkVisibilityContent(false);
        checkViewWithTextAssertion(R.id.error_text_view, "The news source you've selected (bloomberg) isn't available sorted by popular");
        // TODO: checkToastMessageShown();
    }

    @Test
    public void testUIVisibilityWithAvailableSortedBy() throws Exception {
        startActivity(validSource);
        clickSort(R.string.menu_filter_latest);
        checkVisibilityContent(true);
    }

    @Test
    public void testUIVisibilitySomethingWrong() throws Exception {
        MockingInterceptor.isError = true;
        startActivity(validSource);
        checkVisibilityContent(false);
        checkViewWithTextAssertion(R.id.error_text_view, R.string.error_list_empty);
        // TODO: checkToastMessageShown();
    }

    @Test
    public void testScroll() throws Exception {
        startActivity(validSource);
        scrollRecycler();
    }

    @Test
    public void testItemClick() throws Exception {
        startActivity(validSource);
        clickRecyclerItemAtPosition(0);
        Intents.intended(IntentMatchers.hasComponent(DetailsActivity.class.getName()));
    }

    @Test
    public void testElementsCountWithValidSource() throws Exception {
        startActivity(validSource);
        checkRecyclerItemsCount(10);
    }

    @Test
    public void testElementsCountWithWrongSource() throws Exception {
        startActivity(wrongSource);
        checkRecyclerItemsCount(0);
    }

    @Test
    public void testElementsCountWithUnavailableSortedBy() throws Exception {
        startActivity(wrongSource);
        clickSort(R.string.menu_filter_popular);
        checkRecyclerItemsCount(0);
    }

    @Test
    public void testElementsCountWithSomethingWrong() throws Exception {
        MockingInterceptor.isError = true;
        startActivity(validSource);
        checkRecyclerItemsCount(0);
    }

    @Test
    public void testPullDownToRefresh() throws Exception {
        startActivity(validSource);
        pullDownToRefresh();
    }

    @Test
    public void testClickSortedByPopular() throws Exception {
        startActivity(validSource);
        clickSort(R.string.menu_filter_popular);
    }

    @Test
    public void testScenario() throws Exception {
        startActivity(validSource);
        Assert.assertEquals(activity.getActivity().getString(R.string.title_articles), activity.getActivity().getTitle());

        checkVisibilityContent(true);
        checkRecyclerItemsCount(10);

        scrollRecycler();

        clickSort(R.string.menu_filter_popular);
        checkVisibilityContent(false);
        checkViewWithTextAssertion(R.id.error_text_view, "The news source you've selected (bloomberg) isn't available sorted by popular");
        checkRecyclerItemsCount(0);

        MockingInterceptor.isError = true;
        pullDownToRefresh();
        checkVisibilityContent(false);
        checkViewWithTextAssertion(R.id.error_text_view, R.string.error_list_empty);
        checkRecyclerItemsCount(0);

        MockingInterceptor.isError = false;
        pullDownToRefresh();
        checkVisibilityContent(false);
        checkViewWithTextAssertion(R.id.error_text_view, "The news source you've selected (bloomberg) isn't available sorted by popular");
        checkRecyclerItemsCount(0);

        clickSort(R.string.menu_filter_top);
        checkVisibilityContent(true);
        checkRecyclerItemsCount(10);

        scrollRecycler();

        SystemClock.sleep(1000);
        clickRecyclerItemAtPosition(1);
        SystemClock.sleep(1000);
    }

    @After
    public void finish() throws Exception {
        Intents.release();
        MockingInterceptor.isError = false;
    }

    private void startActivity(Source source) {
        Intent intent = new Intent(InstrumentationRegistry.getTargetContext(), ArticlesActivity.class);
        intent.putExtra(ExtraKeys.ITEM_KEY, source);
        activity.launchActivity(intent);
        SystemClock.sleep(3000);
    }

    private void checkVisibilityContent(boolean isVisible) {
        Espresso.onView(ViewMatchers.withId(R.id.recycler_view))
                .check(ViewAssertions.matches(ViewMatchers.withEffectiveVisibility(isVisible ? ViewMatchers.Visibility.VISIBLE : ViewMatchers.Visibility.GONE)));
        Espresso.onView(ViewMatchers.withId(R.id.error_text_view))
                .check(ViewAssertions.matches(ViewMatchers.withEffectiveVisibility(isVisible ? ViewMatchers.Visibility.GONE : ViewMatchers.Visibility.VISIBLE)));
    }

    private void checkViewWithTextAssertion(int viewId, @StringRes int stringResourceId) {
        Espresso.onView(ViewMatchers.withId(viewId))
                .check(ViewAssertions.matches(ViewMatchers.withText(stringResourceId)));
    }

    private void checkViewWithTextAssertion(int viewId, String text) {
        Espresso.onView(ViewMatchers.withId(viewId))
                .check(ViewAssertions.matches(ViewMatchers.withText(text)));
    }

    private void checkToastMessageShown() {
        Espresso.onView(ViewMatchers.withText(R.string.error))
                .inRoot(new ToastMatcher())
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
    }

    private void checkRecyclerItemsCount(int count) {
        Espresso.onView(ViewMatchers.withId(R.id.recycler_view))
                .check(ViewAssertions.matches(RecyclerViewMatcher.withSize(count)));
    }

    private void pullDownToRefresh() {
        Espresso.onView(ViewMatchers.withId(R.id.refresh_layout))
                .perform(ViewActions.swipeDown())
                .check(ViewAssertions.matches(Matchers.allOf(ViewMatchers.isDisplayed(), ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE))));
        SystemClock.sleep(3000);
    }

    private void scrollRecycler() {
        Espresso.onView(ViewMatchers.withId(R.id.recycler_view))
                .perform(RecyclerViewActions.scrollToPosition(10))
                .perform(RecyclerViewActions.scrollToPosition(0))
                .perform(RecyclerViewActions.scrollToPosition(2))
                .perform(RecyclerViewActions.scrollToPosition(6))
                .perform(RecyclerViewActions.scrollToPosition(1))
                .perform(RecyclerViewActions.scrollToPosition(9))
                .perform(RecyclerViewActions.scrollToPosition(0));
    }

    private void clickRecyclerItemAtPosition(int position) {
        Espresso.onView(ViewMatchers.withId(R.id.recycler_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(position, ViewActions.click()));
    }

    private void clickSort(@StringRes int menuItemTitle) {
        Espresso.onView(ViewMatchers.withId(R.id.filter_menu))
                .perform(ViewActions.click());
        SystemClock.sleep(500);
        Espresso.onData(Matchers.hasToString(Matchers.is(activity.getActivity().getString(menuItemTitle))))
                .perform(ViewActions.click());
        SystemClock.sleep(3000);
    }

}
