package siarhei.mikevich.newstests.presentation.utils.matchers;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

/**
 * Created by Siarhei Mikevich on 11/9/17.
 */

public class RecyclerViewMatcher extends TypeSafeMatcher<View> {

    private int size;

    private RecyclerViewMatcher(int size) {
        this.size = size;
    }

    public static RecyclerViewMatcher withSize(int size) {
        return new RecyclerViewMatcher(size);
    }

    @Override
    protected boolean matchesSafely(View item) {
        if (item instanceof RecyclerView) {
            RecyclerView recyclerView = (RecyclerView) item;
            int size = 0;
            if (recyclerView.getAdapter() != null) {
                size = recyclerView.getAdapter().getItemCount();
            }
            return size == this.size;
        } else {
            return false;
        }
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("recycler view is not contains " + this.size + " elements");
    }

}
