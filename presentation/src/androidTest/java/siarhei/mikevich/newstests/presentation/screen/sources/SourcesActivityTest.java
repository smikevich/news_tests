package siarhei.mikevich.newstests.presentation.screen.sources;

import android.content.Intent;
import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import siarhei.mikevich.newstests.data.api.interceptor.MockingInterceptor;
import siarhei.mikevich.newstests.domain.model.content.Category;
import siarhei.mikevich.newstests.presentation.R;
import siarhei.mikevich.newstests.presentation.screen.articles.ArticlesActivity;
import siarhei.mikevich.newstests.presentation.utils.ExtraKeys;
import siarhei.mikevich.newstests.presentation.utils.actions.RecyclerViewAction;
import siarhei.mikevich.newstests.presentation.utils.matchers.RecyclerViewMatcher;
import siarhei.mikevich.newstests.presentation.utils.matchers.ToastMatcher;

/**
 * Created by Siarhei Mikevich on 11/9/17.
 */

// use mock flavor

@RunWith(AndroidJUnit4.class)
public class SourcesActivityTest {

    private Category validCategory;
    private Category wrongCategory;

    @Rule
    public ActivityTestRule<SourcesActivity> activity = new ActivityTestRule<>(SourcesActivity.class, false, false);

    @Before
    public void setup() throws Exception {
        validCategory = new Category();
        validCategory.setId("any_valid_category");
        wrongCategory = new Category();
        wrongCategory.setId(MockingInterceptor.WRONG_CATEGORY);
        Intents.init();
    }

    @Test
    public void testTitle() throws Exception {
        startActivity(validCategory);
        Assert.assertEquals(activity.getActivity().getString(R.string.title_sources), activity.getActivity().getTitle());
    }

    @Test
    public void testUIVisibilityWithValidCategory() throws Exception {
        startActivity(validCategory);
        checkVisibilityContent(true);
    }

    @Test
    public void testUIVisibilityWithWrongCategory() throws Exception {
        startActivity(wrongCategory);
        checkVisibilityContent(false);
        checkViewWithTextAssertion(R.id.error_text_view, activity.getActivity().getString(R.string.error_list_empty));
    }

    @Test
    public void testUIVisibilitySomethingWrong() throws Exception {
        MockingInterceptor.isError = true;
        startActivity(validCategory);
        checkVisibilityContent(false);
        checkViewWithTextAssertion(R.id.error_text_view, activity.getActivity().getString(R.string.error_list_empty));
        // TODO: checkToastMessageShown();
    }

    @Test
    public void testScroll() throws Exception {
        startActivity(validCategory);
        scrollRecycler();
    }

    @Test
    public void testItemClick() throws Exception {
        startActivity(validCategory);
        clickRecyclerItemAtPosition(0, ViewActions.click());
        Intents.intended(IntentMatchers.hasComponent(ArticlesActivity.class.getName()));
    }

    @Test
    public void testElementsCountWithValidCategory() throws Exception {
        startActivity(validCategory);
        checkRecyclerItemsCount(11);
    }

    @Test
    public void testElementsCountWithWrongCategory() throws Exception {
        startActivity(wrongCategory);
        checkRecyclerItemsCount(0);
    }

    @Test
    public void testElementsCountWithSomethingWrong() throws Exception {
        MockingInterceptor.isError = true;
        startActivity(validCategory);
        checkRecyclerItemsCount(0);
    }

    @Test
    public void testPullDownToRefresh() throws Exception {
        startActivity(validCategory);
        pullDownToRefresh();
    }

    @Test
    public void testClickAbout() throws Exception {
        startActivity(validCategory);
        clickRecyclerItemAtPosition(0, RecyclerViewAction.clickAbout());
        Intents.intended(IntentMatchers.hasAction(Intent.ACTION_VIEW));
    }

    @Test
    public void testClickAboutWrongUrl() throws Exception {
        startActivity(validCategory);
        clickRecyclerItemAtPosition(10, RecyclerViewAction.clickAbout());
        Intents.intended(Matchers.not(IntentMatchers.hasAction(Intent.ACTION_VIEW)));
    }

    @Test
    public void testScenario() throws Exception {
        startActivity(validCategory);
        Assert.assertEquals(activity.getActivity().getString(R.string.title_sources), activity.getActivity().getTitle());
        checkVisibilityContent(true);
        scrollRecycler();
        clickRecyclerItemAtPosition(0, ViewActions.click());
        Intents.intended(IntentMatchers.hasComponent(ArticlesActivity.class.getName()));
        SystemClock.sleep(3000);
        Espresso.pressBack();
        checkRecyclerItemsCount(11);

        MockingInterceptor.isError = true;
        pullDownToRefresh();
        checkVisibilityContent(false);
        checkViewWithTextAssertion(R.id.error_text_view, activity.getActivity().getString(R.string.error_list_empty));
        checkRecyclerItemsCount(0);

        MockingInterceptor.isError = false;
        pullDownToRefresh();
        checkVisibilityContent(true);
        scrollRecycler();
        checkRecyclerItemsCount(11);
    }

    @After
    public void finish() throws Exception {
        MockingInterceptor.isError = false;
        Intents.release();
    }

    private void startActivity(Category category) {
        Intent intent = new Intent(InstrumentationRegistry.getTargetContext(), SourcesActivity.class);
        intent.putExtra(ExtraKeys.ITEM_KEY, category);
        activity.launchActivity(intent);
        SystemClock.sleep(3000);
    }

    private void checkVisibilityContent(boolean isVisible) {
        Espresso.onView(ViewMatchers.withId(R.id.recycler_view))
                .check(ViewAssertions.matches(ViewMatchers.withEffectiveVisibility(isVisible ? ViewMatchers.Visibility.VISIBLE : ViewMatchers.Visibility.GONE)));
        Espresso.onView(ViewMatchers.withId(R.id.error_text_view))
                .check(ViewAssertions.matches(ViewMatchers.withEffectiveVisibility(isVisible ? ViewMatchers.Visibility.GONE : ViewMatchers.Visibility.VISIBLE)));
    }

    private void checkViewWithTextAssertion(int viewId, String text) {
        Espresso.onView(ViewMatchers.withId(viewId))
                .check(ViewAssertions.matches(ViewMatchers.withText(text)));
    }

    private void checkToastMessageShown() {
        Espresso.onView(ViewMatchers.withText(R.string.error))
                .inRoot(new ToastMatcher())
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
    }

    private void checkRecyclerItemsCount(int count) {
        Espresso.onView(ViewMatchers.withId(R.id.recycler_view))
                .check(ViewAssertions.matches(RecyclerViewMatcher.withSize(count)));
    }

    private void scrollRecycler() {
        Espresso.onView(ViewMatchers.withId(R.id.recycler_view))
                .perform(RecyclerViewActions.scrollToPosition(10))
                .perform(RecyclerViewActions.scrollToPosition(0))
                .perform(RecyclerViewActions.scrollToPosition(2))
                .perform(RecyclerViewActions.scrollToPosition(6))
                .perform(RecyclerViewActions.scrollToPosition(1))
                .perform(RecyclerViewActions.scrollToPosition(9))
                .perform(RecyclerViewActions.scrollToPosition(0));
    }

    private void clickRecyclerItemAtPosition(int position, ViewAction viewAction) {
        Espresso.onView(ViewMatchers.withId(R.id.recycler_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(position, viewAction));
    }

    private void pullDownToRefresh() {
        Espresso.onView(ViewMatchers.withId(R.id.refresh_layout))
                .perform(ViewActions.swipeDown())
                .check(ViewAssertions.matches(Matchers.allOf(ViewMatchers.isDisplayed(), ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE))));
        SystemClock.sleep(3000);
    }

}
