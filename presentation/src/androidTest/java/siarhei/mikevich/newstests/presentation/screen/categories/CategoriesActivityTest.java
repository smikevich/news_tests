package siarhei.mikevich.newstests.presentation.screen.categories;

import android.content.Intent;
import android.os.SystemClock;
import android.support.annotation.StringRes;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import siarhei.mikevich.newstests.data.repository.RepositoryProvider;
import siarhei.mikevich.newstests.presentation.R;
import siarhei.mikevich.newstests.presentation.screen.sources.SourcesActivity;
import siarhei.mikevich.newstests.presentation.utils.matchers.RecyclerViewMatcher;
import siarhei.mikevich.newstests.presentation.utils.repository.CategoriesNewsRepositoryMockTest;

/**
 * Created by mikes on 06.11.17.
 */

// use any flavor

@RunWith(AndroidJUnit4.class)
public class CategoriesActivityTest {

    private final String validAssetResponse = "categories.json";
    private final String wrongAssetResponse = "some_wrong_asset_response.json";

    @Rule
    public final ActivityTestRule<CategoriesActivity> activity = new ActivityTestRule<>(CategoriesActivity.class, false, false);

    @Before
    public void setup() throws Exception {
        Intents.init();
    }

    @Test
    public void testTitle() throws Exception {
        startActivity(validAssetResponse);
        checkTitleAssertion(R.string.title_categories);
    }

    @Test
    public void testUIVisibilityWithValidAssetResponse() throws Exception {
        startActivity(validAssetResponse);
        checkUIVisibility(true);
    }

    @Test
    public void testUIVisibilityWithWrongAssetResponse() throws Exception {
        startActivity(wrongAssetResponse);
        checkUIVisibility(false);
    }

    @Test
    public void testErrorMessageWithWrongAssetResponse() throws Exception {
        startActivity(wrongAssetResponse);
        checkViewWithTextAssertion(R.id.error_text_view, R.string.error_getting_response_from_assets);
    }

    @Test
    public void testRecyclerItemsCountWithValidAssetResponse() throws Exception {
        startActivity(validAssetResponse);
        checkRecyclerItemsWithCount(10);
    }

    @Test
    public void testRecyclerItemsCountWithWrongAssetResponse() throws Exception {
        startActivity(wrongAssetResponse);
        checkRecyclerItemsWithCount(0);
    }

    @Test
    public void testScrolling() throws Exception {
        startActivity(validAssetResponse);
        scrollRecycler();
    }

    @Test
    public void testClickItem() throws Exception {
        startActivity(validAssetResponse);
        clickRecyclerAtPosition(0);
        Intents.intended(IntentMatchers.hasComponent(SourcesActivity.class.getName()));
    }

    @Test
    public void testPullDownToRefresh() throws Exception {
        startActivity(validAssetResponse);
        pullDownToRefresh();
    }

    @Test
    public void testScenario() throws Exception {
        startActivity(validAssetResponse);
        checkTitleAssertion(R.string.title_categories);
        checkUIVisibility(true);
        checkRecyclerItemsWithCount(10);
        scrollRecycler();
        pullDownToRefresh();
        clickRecyclerAtPosition(0);
        Intents.intended(IntentMatchers.hasComponent(SourcesActivity.class.getName()));

        SystemClock.sleep(3000);
        Espresso.pressBack();

        pullDownToRefresh();
        checkUIVisibility(true);
        checkRecyclerItemsWithCount(10);
        scrollRecycler();
        pullDownToRefresh();
        clickRecyclerAtPosition(0);
    }

    @After
    public void finish() throws Exception {
        Intents.release();
    }

    private void startActivity(String assetResponse) {
        RepositoryProvider.setCategoriesRepository(new CategoriesNewsRepositoryMockTest(assetResponse));
        Intent intent = new Intent(InstrumentationRegistry.getTargetContext(), CategoriesActivity.class);
        activity.launchActivity(intent);
        SystemClock.sleep(3000);
    }

    private void checkTitleAssertion(@StringRes int titleResourceId) {
        Assert.assertEquals(activity.getActivity().getString(titleResourceId), activity.getActivity().getTitle());
    }

    private void checkViewWithTextAssertion(int viewId, @StringRes int textResourceId) {
        Espresso.onView(ViewMatchers.withId(viewId))
                .check(ViewAssertions.matches(ViewMatchers.withText(textResourceId)));
    }

    private void checkUIVisibility(boolean isVisible) {
        Espresso.onView(ViewMatchers.withId(R.id.recycler_view))
                .check(ViewAssertions.matches(ViewMatchers.withEffectiveVisibility(isVisible ? ViewMatchers.Visibility.VISIBLE : ViewMatchers.Visibility.GONE)));
        Espresso.onView(ViewMatchers.withId(R.id.error_text_view))
                .check(ViewAssertions.matches(ViewMatchers.withEffectiveVisibility(isVisible ? ViewMatchers.Visibility.GONE : ViewMatchers.Visibility.VISIBLE)));
    }

    private void checkRecyclerItemsWithCount(int count) {
        Espresso.onView(ViewMatchers.withId(R.id.recycler_view))
                .check(ViewAssertions.matches(RecyclerViewMatcher.withSize(count)));
    }

    private void scrollRecycler() {
        Espresso.onView(ViewMatchers.withId(R.id.recycler_view))
                .perform(RecyclerViewActions.scrollToPosition(3))
                .perform(RecyclerViewActions.scrollToPosition(0))
                .perform(RecyclerViewActions.scrollToPosition(5))
                .perform(RecyclerViewActions.scrollToPosition(1))
                .perform(RecyclerViewActions.scrollToPosition(10))
                .perform(RecyclerViewActions.scrollToPosition(15))
                .perform(RecyclerViewActions.scrollToPosition(0));
    }

    private void pullDownToRefresh() {
        Espresso.onView(ViewMatchers.withId(R.id.refresh_layout))
                .perform(ViewActions.swipeDown())
                .check(ViewAssertions.matches(Matchers.allOf(ViewMatchers.isDisplayed(), ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE))));
        SystemClock.sleep(3000);
    }

    private void clickRecyclerAtPosition(int position) {
        Espresso.onView(ViewMatchers.withId(R.id.recycler_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(position, ViewActions.click()));
    }

}
