package siarhei.mikevich.newstests.presentation.utils.repository;

import rx.Observable;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Category;

/**
 * Created by Siarhei Mikevich on 11/27/17.
 */

public class CategoriesNewsRepositoryMockTest extends CategoriesNewsRepositoryTest {

    private final String assetPath;

    public CategoriesNewsRepositoryMockTest(String assetPath) {
        this.assetPath = assetPath;
    }

    @Override
    public Observable<BaseItemsContent<Category>> getCategories(String assetPath) {
        return super.getCategories(this.assetPath);
    }

}
