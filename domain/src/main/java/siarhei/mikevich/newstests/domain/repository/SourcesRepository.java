package siarhei.mikevich.newstests.domain.repository;

import rx.Observable;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Source;

/**
 * Created by Siarhei Mikevich on 11/21/17.
 */

public interface SourcesRepository {

    Observable<BaseItemsContent<Source>> getSources(String category);

}
