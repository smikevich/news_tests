package siarhei.mikevich.newstests.domain.utils.unit;

/**
 * Created by Siarhei Mikevich on 10/24/17.
 */

public class TextUtils {

    public static boolean isEmpty(String text) {
        return text == null || text.replaceAll("\\s+", "").length() == 0;
    }

}
