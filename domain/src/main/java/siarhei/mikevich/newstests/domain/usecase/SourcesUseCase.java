package siarhei.mikevich.newstests.domain.usecase;

import rx.Observable;
import siarhei.mikevich.newstests.domain.repository.SourcesRepository;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Source;

/**
 * Created by Siarhei Mikevich on 11/21/17.
 */

public class SourcesUseCase extends BaseUseCase<BaseItemsContent<Source>, SourcesUseCase.Params> {

    private final SourcesRepository repository;

    public SourcesUseCase(SourcesRepository repository, Observable.Transformer<BaseItemsContent<Source>, BaseItemsContent<Source>> asyncTransformer) {
        super(asyncTransformer);
        this.repository = repository;
    }

    @Override
    protected Observable<BaseItemsContent<Source>> getUseCaseObservable(Params params) {
        return repository.getSources(params.category);
    }

    public static final class Params {

        private String category;

        public Params(String category) {
            this.category = category;
        }

    }

}
