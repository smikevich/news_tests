package siarhei.mikevich.newstests.domain.repository;

import rx.Observable;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Category;

/**
 * Created by Siarhei Mikevich on 11/21/17.
 */

public interface CategoriesRepository {

    Observable<BaseItemsContent<Category>> getCategories(String assetPath);

}
