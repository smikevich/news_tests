package siarhei.mikevich.newstests.domain.model.content;

import java.io.Serializable;

/**
 * Created by mikes on 05.10.17.
 */

public class Category implements Serializable {

    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
