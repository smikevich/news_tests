package siarhei.mikevich.newstests.domain.usecase;

import rx.Observable;
import siarhei.mikevich.newstests.domain.repository.ArticlesRepository;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Article;

/**
 * Created by Siarhei Mikevich on 11/21/17.
 */

public class ArticlesUseCase extends BaseUseCase<BaseItemsContent<Article>, ArticlesUseCase.Params> {

    private final ArticlesRepository repository;

    public ArticlesUseCase(ArticlesRepository repository, Observable.Transformer<BaseItemsContent<Article>, BaseItemsContent<Article>> asyncTransformer) {
        super(asyncTransformer);
        this.repository = repository;
    }

    @Override
    protected Observable<BaseItemsContent<Article>> getUseCaseObservable(Params params) {
        return repository.getArticles(params.source, params.sortBy);
    }

    public static final class Params {

        private String source;
        private String sortBy;

        public Params(String source, String sortBy) {
            this.source = source;
            this.sortBy = sortBy;
        }

    }

}
