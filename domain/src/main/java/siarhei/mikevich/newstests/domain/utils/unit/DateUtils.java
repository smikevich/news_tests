package siarhei.mikevich.newstests.domain.utils.unit;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Siarhei Mikevich on 10/9/17.
 */

public class DateUtils {

    public static String parseDateTime(String dateTime) {
        String dateTimeParsed = "";
        if (TextUtils.isEmpty(dateTime)) {
            return dateTimeParsed;
        }

        try {
            Date date = new SimpleDateFormat(getDateTimePattern(dateTime.length()), Locale.US).parse(dateTime);
            dateTimeParsed = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT).format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateTimeParsed;
    }

    private static String getDateTimePattern(int dateTimeLength) {
        String dateTimePattern;
        if (dateTimeLength == 24) {
            dateTimePattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        } else if (dateTimeLength == 23) {
            dateTimePattern = "yyyy-MM-dd'T'HH:mm:ss.SS'Z'";
        } else if (dateTimeLength == 22) {
            dateTimePattern = "yyyy-MM-dd'T'HH:mm:ss.S'Z'";
        } else if (dateTimeLength == 20) {
            dateTimePattern = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        } else if (dateTimeLength == 19) {
            dateTimePattern = "yyyy-MM-dd'T'HH:mm:ss";
        } else {
            dateTimePattern = "yyyyMMddHHmmss";
        }
        return dateTimePattern;
    }

}
