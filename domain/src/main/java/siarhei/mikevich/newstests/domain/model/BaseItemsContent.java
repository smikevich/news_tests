package siarhei.mikevich.newstests.domain.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Siarhei Mikevich on 11/21/17.
 */

public class BaseItemsContent<T> {

    private List<T> items;
    private String errorMessage;

    public BaseItemsContent(List<T> items, String errorMessage) {
        this.items = items == null ? new ArrayList<T>() : items;
        this.errorMessage = errorMessage == null ? "" : errorMessage;
    }

    public List<T> getItems() {
        return items;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}
