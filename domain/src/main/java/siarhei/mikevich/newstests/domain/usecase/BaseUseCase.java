package siarhei.mikevich.newstests.domain.usecase;

import rx.Observable;

/**
 * Created by Siarhei Mikevich on 11/23/17.
 */

public abstract class BaseUseCase<T, P> {

    private final Observable.Transformer<T, T> asyncTransformer;

    BaseUseCase(Observable.Transformer<T, T> asyncTransformer) {
        this.asyncTransformer = asyncTransformer;
    }

    protected abstract Observable<T> getUseCaseObservable(P params);

    public Observable<T> execUseCase(P params) {
        return getUseCaseObservable(params)
                .compose(asyncTransformer);
    }

}
