package siarhei.mikevich.newstests.domain.utils.unit;

/**
 * Created by Siarhei Mikevich on 10/9/17.
 */

public class StringUtils {

    public static String getAbbreviation(String text) {
        StringBuilder symbolsText = new StringBuilder();
        if (!TextUtils.isEmpty(text)) {
            text = text
                    .replaceAll(" +", " ")
                    .replaceAll("^\\s", "")
                    .replaceAll("\\s$", "");
            String[] splitText = text.split(" ");
            char firstSymbol = Character.toUpperCase(splitText[0].charAt(0));
            char secondSymbol = splitText.length >= 2 ? Character.toUpperCase(splitText[1].charAt(0)) : ' ';
            symbolsText.append(firstSymbol).append(" ").append(secondSymbol);
        }
        return symbolsText.toString().trim();
    }

}
