package siarhei.mikevich.newstests.domain.usecase;

import rx.Observable;
import siarhei.mikevich.newstests.domain.repository.CategoriesRepository;
import siarhei.mikevich.newstests.domain.model.BaseItemsContent;
import siarhei.mikevich.newstests.domain.model.content.Category;

/**
 * Created by Siarhei Mikevich on 11/21/17.
 */

public class CategoriesUseCase extends BaseUseCase<BaseItemsContent<Category>, CategoriesUseCase.Params> {

    private final CategoriesRepository repository;

    public CategoriesUseCase(CategoriesRepository repository, Observable.Transformer<BaseItemsContent<Category>, BaseItemsContent<Category>> asyncTransformer) {
        super(asyncTransformer);
        this.repository = repository;
    }

    @Override
    protected Observable<BaseItemsContent<Category>> getUseCaseObservable(Params params) {
        return repository.getCategories(params.assetPath);
    }

    public static final class Params {

        private String assetPath;

        public Params(String assetPath) {
            this.assetPath = assetPath;
        }

    }

}
