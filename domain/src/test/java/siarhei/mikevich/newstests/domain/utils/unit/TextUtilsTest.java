package siarhei.mikevich.newstests.domain.utils.unit;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Created by Siarhei Mikevich on 11/27/17.
 */

@RunWith(JUnit4.class)
public class TextUtilsTest {

    @Test
    public void testIsEmptyTrue() throws Exception {
        Assert.assertEquals(true, TextUtils.isEmpty(""));
        Assert.assertEquals(true, TextUtils.isEmpty(null));
        Assert.assertEquals(true, TextUtils.isEmpty(" "));
        Assert.assertEquals(true, TextUtils.isEmpty("    "));
    }

    @Test
    public void testIsEmptyFalse() throws Exception {
        Assert.assertEquals(false, TextUtils.isEmpty("hello world"));
        Assert.assertEquals(false, TextUtils.isEmpty("      a"));
        Assert.assertEquals(false, TextUtils.isEmpty("      a "));
    }

}
