package siarhei.mikevich.newstests.domain.model;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;

import siarhei.mikevich.newstests.domain.model.content.Category;

/**
 * Created by Siarhei Mikevich on 12/12/17.
 */

@RunWith(JUnit4.class)
public class BaseItemsContentTest {

    @Test
    public void someTest() throws Exception {
        BaseItemsContent<Category> itemsContent = new BaseItemsContent<>(null, null);
        Assert.assertEquals(new ArrayList<>(), itemsContent.getItems());
        Assert.assertEquals(0, itemsContent.getItems().size());
        Assert.assertEquals("", itemsContent.getErrorMessage());
    }

    @Test
    public void someTest2() throws Exception {
        int itemsSize = 5;
        List<Category> categories = getCategories(itemsSize);
        String errorMessage = "some_error_message";
        BaseItemsContent<Category> itemsContent = new BaseItemsContent<>(categories, errorMessage);
        Assert.assertEquals(categories, itemsContent.getItems());
        Assert.assertEquals(itemsSize, itemsContent.getItems().size());
        Assert.assertEquals(errorMessage, itemsContent.getErrorMessage());
    }

    private List<Category> getCategories(int size) {
        List<Category> categories = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            categories.add(new Category());
        }
        return categories;
    }

}
