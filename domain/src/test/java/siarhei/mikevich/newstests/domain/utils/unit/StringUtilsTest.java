package siarhei.mikevich.newstests.domain.utils.unit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Created by Siarhei Mikevich on 11/27/17.
 */

@RunWith(JUnit4.class)
public class StringUtilsTest {

    private StringUtils stringUtils;

    @Before
    public void setUp() throws Exception {
        stringUtils = new StringUtils();
    }

    @Test
    public void testTwoSymbolsAbbreviation() throws Exception {
        Assert.assertEquals("A D", stringUtils.getAbbreviation("Android Developer Android Developer"));
        Assert.assertEquals("A D", stringUtils.getAbbreviation(" Android Developer Android Developer "));
        Assert.assertEquals("A D", stringUtils.getAbbreviation("   Android   Developer   Android   Developer   "));
        Assert.assertEquals("A D", stringUtils.getAbbreviation("Android Developer"));
        Assert.assertEquals("A D", stringUtils.getAbbreviation("Android developer"));
        Assert.assertEquals("A D", stringUtils.getAbbreviation("android Developer"));
        Assert.assertEquals("A D", stringUtils.getAbbreviation("android developer"));
        Assert.assertEquals("A D", stringUtils.getAbbreviation(" android developer"));
        Assert.assertEquals("A D", stringUtils.getAbbreviation(" android developer "));
        Assert.assertEquals("A D", stringUtils.getAbbreviation("         android   developer   "));
        Assert.assertEquals(". !", stringUtils.getAbbreviation(".!@#$% !@#$%"));
    }

    @Test
    public void testOneSymbolAbbreviation() throws Exception {
        Assert.assertEquals("A", stringUtils.getAbbreviation("android"));
        Assert.assertEquals("A", stringUtils.getAbbreviation("Android"));
        Assert.assertEquals("A", stringUtils.getAbbreviation("a"));
        Assert.assertEquals("A", stringUtils.getAbbreviation(" a"));
        Assert.assertEquals("A", stringUtils.getAbbreviation("   a"));
        Assert.assertEquals("A", stringUtils.getAbbreviation("a "));
        Assert.assertEquals("A", stringUtils.getAbbreviation("a   "));
        Assert.assertEquals("A", stringUtils.getAbbreviation(" a "));
        Assert.assertEquals("A", stringUtils.getAbbreviation("   a   "));
        Assert.assertEquals(".", stringUtils.getAbbreviation("."));
        Assert.assertEquals(".", stringUtils.getAbbreviation(".,!"));
        Assert.assertEquals("2", stringUtils.getAbbreviation("213"));
    }

    @Test
    public void testNoSymbolsAbbreviation() throws Exception {
        Assert.assertEquals("", stringUtils.getAbbreviation(" "));
        Assert.assertEquals("", stringUtils.getAbbreviation("  "));
        Assert.assertEquals("", stringUtils.getAbbreviation("   "));
        Assert.assertEquals("", stringUtils.getAbbreviation(""));
        Assert.assertEquals("", stringUtils.getAbbreviation(null));
    }

    @After
    public void finish() throws Exception {
        stringUtils = null;
    }

}
