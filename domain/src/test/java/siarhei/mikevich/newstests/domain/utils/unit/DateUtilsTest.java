package siarhei.mikevich.newstests.domain.utils.unit;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Created by Siarhei Mikevich on 11/27/17.
 */

@RunWith(JUnit4.class)
public class DateUtilsTest {

    private final String successExpectedResultEn = "October 24, 2017 1:55 PM";
    private final String successExpectedResultRu = "24 октября 2017 г. 13:55";
    private final String failureExpectedResult = "";
    private final String testDateTime1 = "2017-10-24T13:55:04.453Z";
    private final String testDateTime2 = "2017-10-24T13:55:04.45Z";
    private final String testDateTime3 = "2017-10-24T13:55:04.4Z";
    private final String testDateTime4 = "2017-10-24T13:55:04Z";
    private final String testDateTime5 = "2017-10-24T13:55:04";
    private final String testDateTime6 = "20171024135504";
    private final String testDateTime7 = "21432543421413121321";
    private final String testDateTime8 = "213213123";

    @Test
    public void testSuccessParseDateTime() throws Exception {
        Assert.assertTrue(successExpectedResultEn.equals(DateUtils.parseDateTime(testDateTime1)) || successExpectedResultRu.equals(DateUtils.parseDateTime(testDateTime1)));
        Assert.assertTrue(successExpectedResultEn.equals(DateUtils.parseDateTime(testDateTime2)) || successExpectedResultRu.equals(DateUtils.parseDateTime(testDateTime2)));
        Assert.assertTrue(successExpectedResultEn.equals(DateUtils.parseDateTime(testDateTime3)) || successExpectedResultRu.equals(DateUtils.parseDateTime(testDateTime3)));
        Assert.assertTrue(successExpectedResultEn.equals(DateUtils.parseDateTime(testDateTime4)) || successExpectedResultRu.equals(DateUtils.parseDateTime(testDateTime4)));
        Assert.assertTrue(successExpectedResultEn.equals(DateUtils.parseDateTime(testDateTime5)) || successExpectedResultRu.equals(DateUtils.parseDateTime(testDateTime5)));
        Assert.assertTrue(successExpectedResultEn.equals(DateUtils.parseDateTime(testDateTime6)) || successExpectedResultRu.equals(DateUtils.parseDateTime(testDateTime6)));
    }

    @Test
    public void testFailureParseDateTime() throws Exception {
        Assert.assertEquals(failureExpectedResult, DateUtils.parseDateTime(testDateTime7));
        Assert.assertEquals(failureExpectedResult, DateUtils.parseDateTime(testDateTime8));
        Assert.assertEquals(failureExpectedResult, DateUtils.parseDateTime(null));
    }

}
